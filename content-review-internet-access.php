<?php 
$meta = getMeta();

if (have_posts()) : while (have_posts()) : the_post(); 
?>

<h1><?php the_title(); ?></h1>

<?php 
the_content(); 

// Get permalink for later use in the form
$page_url = get_the_permalink();

// Interpret any incoming query string
// Location
if (empty($_GET['id'])) {
   $intId = '';
   
   _e( 'No internet access specified', 'signpost' );
} else {
   $intId = sanitize_text_field( $_GET['id'] );
   
   // Get draft record that corresponds with selected one

   // Initialise output
   $str_html = '';
   
   // Set up the array for the query
   $args = array(
      'post_type' => 'spt_internet_access',
      'post_status' => 'draft',
      'p' => $intId,
   );
   
   // Run query
   $myquery0 = new WP_Query($args); // Run the query
   if ($myquery0->have_posts()) :
   
      while ($myquery0->have_posts()) : $myquery0->the_post();
         $post_meta = get_post_custom();  // Retrieves all custom fields for post
   
         // Store details
         $int_access = array( // Create array to store required details
            'id' => get_the_ID(),
            'provider' => get_the_title(),
            // various meta fields here
            'location' => $post_meta['spt_int_location'][0],
            'address' => $post_meta['spt_int_address'][0],
            'postcode' => $post_meta['spt_int_postcode'][0],

            'freewifi' => $post_meta['spt_int_free_wifi'][0],
            'contact' => $post_meta['spt_int_contact_info'][0],
            'latlng' => $post_meta['spt_int_latlng'][0],
            'restrictions' => $post_meta['spt_int_restrictions'][0],
            'time_limits' => $post_meta['spt_int_time_limits'][0],
            'num_pcs' => $post_meta['spt_int_num_pcs'][0],
            'num_tablets' => $post_meta['spt_int_num_tablets'][0],
            'support' => $post_meta['spt_int_support'][0],
            'open_days' => $post_meta['spt_int_open_days'][0],
            'open_hours' => $post_meta['spt_int_open_hours'][0],
            'printing' => $post_meta['spt_int_printing'][0],
            'url' => $post_meta['spt_int_url'][0],
         );

///////////////
   
         // Set up string to highlight where details have not been specified
         $not_spec =  '<span class="not-spec">'.__( 'Not specified', 'signpost' ).'</span>';
      
         
         $str_html .= '<section class="data-item">';
         $str_html .= '<h3>'.$int_access['provider'].'</h3>';
         // Check for latlng value and put out mapping buttons and map div
         if (isset($int_access['latlng'])) {
            if ($int_access['latlng'] == ',') {
               $str_html .= '<p class="not-spec"><strong>'.__( 'Geocoding not successful - needs manual input', 'signpost' ).'</strong></p>';
            } else {
               // Assumption is that Geocode was successful - so show map
               $str_html .= output_review_map($int_access['latlng'], $int_access['provider'], 'mapcanvas' );
            }
         }
         // Put out rest of data
         $str_html .= '<dl class="other-detail">';
            //Location
         //if (!isset($int_access['location']) || empty($int_access['location'])) {
         if (isset($int_access['location']) && (strlen($int_access['location']) > 0) ) {
            $frag = $int_access['location'];
         } else {
            $frag = $not_spec;
         }
         $str_html .= '<dt class="location">'.__( 'Location notes:', 'signpost' ).'</dt> <dd>'.$frag.'</dd>';
      
         $str_html .= '<dt class="address">'.__( 'Address:', 'signpost' ).'</dt> <dd>'.$int_access['address'].' '.$int_access['postcode'].'</dd>';
         
         // Tidy up days
         $sdays = get_items_from_selection_array($int_access['open_days'], get_days_array());

      
         $str_html .= '<dt class="opening">'.__( 'Opening days:', 'signpost' ).'</dt> <dd>'.$sdays.'</dd>';
         $str_html .= '<dt class="opening">'.__( 'Opening hours:', 'signpost' ).'</dt> <dd>'.$int_access['open_hours'].'</dd>';
      
         //Contact details
         if (isset($int_access['contact']) && (strlen($int_access['contact']) > 0)) {
            $frag = $int_access['contact'];
         } else {
            $frag = $not_spec;
         }
         $str_html .= '<dt>'.__( 'Contact information:', 'signpost' ).'</dt> <dd>'.$frag.'</dd>';
      
         //Free wifi 
         $freewifi = get_items_from_selection_array($int_access['freewifi'], get_yes_no_array());
         $str_html .= '<dt>'.__( 'Free wifi:', 'signpost' ).'</dt> <dd>'.$freewifi.'</dd>';
/*
         if (isset($int_access['freewifi']) && (strlen($int_access['restrictions']) > 0)) {
            $frag = $int_access['restrictions'];
         } else {
            $frag = $not_spec;
         }
         $str_html .= '<dt>'.__( 'Restrictions:', 'signpost' ).'</dt> <dd>'.$frag.'</dd>';
*/      
         //Restrictions
         if (isset($int_access['restrictions']) && (strlen($int_access['restrictions']) > 0)) {
            $frag = $int_access['restrictions'];
         } else {
            $frag = $not_spec;
         }
         $str_html .= '<dt>'.__( 'Restrictions:', 'signpost' ).'</dt> <dd>'.$frag.'</dd>';
      
         //Time limits
         if (isset($int_access['time_limits']) && (strlen($int_access['time_limits']) > 0) ) {
            $frag = $int_access['time_limits'];
         } else {
            $frag = $not_spec;
         }
         $str_html .= '<dt>'.__( 'Time limits:', 'signpost' ).'</dt> <dd>'.$frag.'</dd>';
      
         //Num pcs
         if (isset($int_access['num_pcs']) && (strlen($int_access['num_pcs']) > 0) ) {
            $frag = $int_access['num_pcs'];
         } else {
            $frag = $not_spec;
         }
         $str_html .= '<dt>'.__( 'Number of internet PCs:', 'signpost' ).'</dt> <dd>'.$frag.'</dd>';
      
         //Num tablets
         if (isset($int_access['num_tablets']) && (strlen($int_access['num_tablets']) > 0) ) {
            $frag = $int_access['num_tablets'];
         } else {
            $frag = $not_spec;
         }
         $str_html .= '<dt>'.__( 'Number of internet tablets:', 'signpost' ).'</dt> <dd>'.$frag.'</dd>';


         $support = get_items_from_selection_array($int_access['support'], get_support_array());
         $str_html .= '<dt>'.__( 'Support Available:', 'signpost' ).'</dt> <dd>'.$support.'</dd>';
      
         //Printing
         if (isset($int_access['printing']) && (strlen($int_access['printing']) > 0) ) {
            $frag = $int_access['printing'];
         } else {
            $frag = $not_spec;
         }
         $str_html .= '<dt>'.__( 'Printing:', 'signpost' ).'</dt> <dd>'.$frag.'</dd>';
      
         //URL
         if (isset($int_access['url']) && (strlen($int_access['url']) > 0) ) {
            $frag = '<a href="'.$int_access['url'].'">'.$int_access['url'].'</a>';
         } else {
            $frag = $not_spec;
         }
         $str_html .= '<dt>'.__( 'Web address:', 'signpost' ).'</dt> <dd>'.$frag.'</dd>';
      
         $str_html .= '</dl>';
         $str_html .= output_review_form($page_url, $intId);
         
         $str_html .= '</section>';

//////////////
      endwhile;
   else :    
      // No recs found
      $str_html = '<p>'.__( 'No Internet Access Points found.', 'signpost' ).'</p>';
   
   endif; 
   wp_reset_query(); // Very important - drops the query and restores where you were
   

   echo '<div id="results">'.$str_html.'</div>';
}







endwhile; 

?>

<div class="clear"></div>

<?php 
else : 
?>

<h2><?php _e( 'Content Not Found', 'signpost' ); ?></h2>
<p><?php _e( 'Sorry, but you are looking for something that is not here', 'signpost' ); ?>.</p>

<?php 
endif; 
?>

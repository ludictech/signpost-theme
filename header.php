<head>
<meta charset="UTF-8" /> 
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<style type="text/css">@import url(<?php bloginfo ('stylesheet_url'); ?>);</style>
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<link rel="icon" href="/favicon.ico" type="image/x-icon" /> 
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
<?php
   include_once( "includes/check_domain.php" );
   $href = $details['favicon'];
   echo "<link rel=\"shortcut icon\" href=\"", $href, "\" type=\"image/x-icon\" />";
?>
<?php
   // Jquery
   wp_enqueue_script('jquery');  
   wp_enqueue_script(
      'tap', 
      get_bloginfo ('template_url').'/js/doubletaptogo.js',
      array( 'jquery' )
   );            
   wp_enqueue_script(
      'cookie', 
      get_bloginfo ('template_url').'/js/js.cookie.js',
      array( 'jquery' )
   );            
   wp_enqueue_script(
      'signpost-library', 
      get_bloginfo ('template_url').'/js/library.js',
      array( 'jquery' )
   );
   wp_enqueue_script('signpost-google-maps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAYfe7X0lZBx0a-bkBhpQ2ZXFIokadJOqI&callback=initMap', array());
?>

<!-- Add in customizer driven styles -->
<?php 
// Retrieve all the theme options into variables
$signpost_body_bg_colour = get_theme_mod( 'signpost2015_body_bg_colour' );
$signpost_body_txt_colour = get_theme_mod( 'signpost2015_body_txt_colour' );
$signpost_body_link_highlight_colour = get_theme_mod( 'signpost2015_body_link_highlight_colour' );
$signpost_navi_bg_colour = get_theme_mod( 'signpost2015_navi_bg_colour' );
$signpost_navi_txt_colour = get_theme_mod( 'signpost2015_navi_t_colour' );
$signpost_navi_highlight_colour = get_theme_mod( 'signpost2015_navi_h_colour' );
$signpost_footer_bg_colour = get_theme_mod( 'signpost2015_footer_bg_colour' );
$signpost_footer_txt_colour = get_theme_mod( 'signpost2015_footer_txt_colour' );
$signpost_footer_link_highlight_colour = get_theme_mod( 'signpost2015_footer_link_highlight_colour' );
$signpost_footer_stripe_colour = get_theme_mod( 'signpost2015_footer_stripe_colour' );
$signpost_form_block_bg_colour = get_theme_mod( 'signpost2015_form_block_bg_colour' );
?>


<style type="text/css">
/* General styles */

body {
   background-color:<?php echo $signpost_footer_bg_colour;?>;
   color:<?php echo $signpost_body_txt_colour; ?>;
}
h1, h2, h3, h4, h5, h6 {
   color:<?php echo $signpost_body_txt_colour; ?>;
}
a:link, a:visited {
   color:<?php echo $signpost_body_txt_colour; ?>;
}
a:hover, a:active, a:focus {
   color:<?php echo $signpost_body_link_highlight_colour; ?>;
}
input[type="text"],
select,
textarea {
   background-color:<?php echo $signpost_body_bg_colour; ?>;
   border: 1px solid <?php echo $signpost_body_txt_colour; ?>;
}
input:focus, textarea:focus, select:focus {
   background-color:#FFFDC0;
}
input[type="submit"],
button,
a.button {
   background:<?php echo $signpost_navi_bg_colour; ?>; 
   border: 1px solid <?php echo $signpost_navi_bg_colour; ?>; 
   color:<?php echo $signpost_navi_txt_colour; ?>;
}
input[type="submit"]:hover,
input[type="submit"]:active,
input[type="submit"]:focus,
button:hover,
button:active,
button:focus,
a.button:hover,
a.button:active,
a.button:focus {
   background-color:<?php echo $signpost_navi_txt_colour; ?>; 
   color:<?php echo $signpost_navi_bg_colour; ?>;
   border: 1px solid <?php echo $signpost_navi_bg_colour; ?>; 
}
/* specific areas */
#top {
   background:<?php echo $signpost_body_bg_colour; ?>;
   color:<?php echo $signpost_body_txt_colour; ?>;
}
#nav-strip {
   background:<?php echo $signpost_navi_bg_colour; ?>;
   color:<?php echo $signpost_navi_txt_colour; ?>;
}
#shownav:link, #shownav:visited, #hidenav:link, #hidenav:visited {
   background:<?php echo $signpost_navi_bg_colour; ?>;  /* Candidate */
   color:<?php echo $signpost_navi_txt_colour; ?>;  /* Candidate */
}
#shownav:hover, #shownav:active, #shownav:focus, #hidenav:hover, #hidenav:active, #hidenav:focus {
   background:<?php echo $signpost_navi_txt_colour; ?>;  /* Candidate */
   color:<?php echo $signpost_navi_bg_colour; ?>;  /* Candidate */
}
#nav li a:link, #nav li a:visited {
   background:<?php echo $signpost_navi_bg_colour; ?>; /* candidate */
   color:<?php echo $signpost_navi_txt_colour; ?>;  /* candidate */
}
#nav li.current-menu-item a:link,
#nav li.current-menu-item a:visited,
#nav li.current-menu-ancestor a:link,
#nav li.current-menu-ancestor a:visited,
#nav li.current-menu-item a:hover,
#nav li.current-menu-item a:focus,
#nav li a:hover,
#nav li a:focus,
#nav>li.hover>a:link,
#nav>li.hover>a:visited{
   background:<?php echo $signpost_navi_bg_colour; ?>;  /* candidate */
   color:<?php echo $signpost_navi_highlight_colour; ?>;  /* candidate */
}
#nav ul a:link,
#nav ul a:visited,	 
#nav li.current-menu-item ul a:link,
#nav li.current-menu-item ul a:visited, 
#nav li.current-menu-ancestor ul a:link,
#nav li.current-menu-ancestor ul a:visited	{ 
   background:<?php echo $signpost_navi_highlight_colour; ?>;  /* Candidate */
   color:<?php echo $signpost_navi_bg_colour; ?>;   /* Candidate */
}
#nav ul a:hover, 
#nav ul a:active, 
#nav ul a:focus,
#nav li.current-menu-item ul a:hover, 
#nav li.current-menu-item ul a:active, 
#nav li.current-menu-item ul a:focus, 
#nav li.current-menu-ancestor ul a:hover, 
#nav li.current-menu-ancestor ul a:active, 
#nav li.current-menu-ancestor ul a:focus,
#nav ul .hover>a		{ 
   background: <?php echo $signpost_navi_bg_colour; ?>;   /* Candidate */
   color:<?php echo $signpost_navi_txt_colour; ?>;   /* Candidate */
   /* font-weight:bold; */
}


#middle {
   background:<?php echo $signpost_body_bg_colour; ?>;
   color:<?php echo $signpost_body_txt_colour; ?>;
   border-top:7px solid <?php echo $signpost_footer_stripe_colour; ?>;
   border-bottom: 7px solid <?php echo $signpost_footer_stripe_colour; ?>;

}
.hp-form, #content .contact {
   background: <?php echo $signpost_form_block_bg_colour; ?>; /* Candidate */
   color:<?php echo $signpost_body_txt_colour; ?>; /* Candidate */
}
h2.form-header, h3.form-header {
   background: <?php echo $signpost_form_block_bg_colour; ?>; /* Candidate */
   color:<?php echo $signpost_body_txt_colour; ?>; /* Candidate */
   border-bottom:1px solid <?php echo $signpost_form_block_bg_colour; ?>;  /* Candidate */
}

#hp-access-hdr span, #hp-course-hdr span {
   background: <?php echo $signpost_form_block_bg_colour; ?>; /* Candidate */
   color:<?php echo $signpost_body_txt_colour; ?>; /* Candidate */
}
#bottom {
   background-color:<?php echo $signpost_footer_bg_colour; ?>;
   color:<?php echo $signpost_footer_txt_colour; ?>;
}
#bottom a:link, #bottom a:visited {
   color:<?php echo $signpost_footer_txt_colour; ?>;
}
#bottom a:hover, #bottom a:active, #bottom a:focus {
   color:<?php echo $signpost_footer_link_highlight_colour; ?>
}

@media screen and (max-width: 700px) {
/* Still to look at colours for mobile nav */

   #nav.active li a:link,
   #nav.active li a:visited,
   #nav li.current-menu-item ul a:link,
   #nav li.current-menu-item ul a:visited, 
   #nav li.current-menu-ancestor ul a:link,
   #nav li.current-menu-ancestor ul a:visited, 
   #nav.active li li a:link,
   #nav.active li li a:visited
    {
      border:1px solid <?php echo $signpost_navi_txt_colour; ?>; /*  Candidate */
      color:<?php echo $signpost_navi_txt_colour; ?>;  /* Candidate */
      background: <?php echo $signpost_navi_bg_colour; ?>;  /* Candidate */
   }
   #nav.active li.current-menu-item>a:link,
   #nav.active li.current-menu-item>a:visited {
      border:1px solid <?php echo $signpost_navi_txt_colour; ?>; /*  Candidate */
      background:<?php echo $signpost_navi_txt_colour; ?>;   /* Candidate */
      color:<?php echo $signpost_navi_bg_colour; ?>;   /* Candidate */
   }
   
   #nav.active li a:hover,
   #nav.active li a:active,
   #nav.active li a:focus,
   #nav.active li li a:hover,
   #nav.active li li a:active,
   #nav.active li li a:focus {
      color:<?php echo $signpost_navi_highlight_colour; ?>;  /* Candidate */
   }

}

@media print {
   html {font-size:90%; }
   #banner2, #nav-strip, #courses-filter-show, #courses-filter-hide,#int-access-filter-show, #int-access-filter-hide, .contact, h2.form-header,  h3.form-header, .map-buttons, .process, .soc-book, #footernav, #hp-forms, .home-links, .admin-links
   { display:none; }
   
   *, body, h1, h2, h3, h4, h5, h6 {
      color:#000;
      background-color:#fff !important;
   }
   #results .search-statement {
      border-top-color:#000;
   }
   #results .num-found, #results .data-item {
      border-bottom-color:#000;
   }

}   
</style>
<?php wp_head(); ?>
</head>
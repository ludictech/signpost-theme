<?php 
$meta = getMeta();

if (have_posts()) : while (have_posts()) : the_post(); 
?>

<h1><?php the_title(); ?></h1>

<?php 
the_content(); 

// Get permalink for later use in the form
$page_url = get_the_permalink();

// Interpret any incoming query string
// Location
if (empty($_GET['id'])) {
   $intId = '';
   
   _e( 'No course specified', 'signpost' );
} else {
   $intId = sanitize_text_field( $_GET['id'] );
   
   // Get draft record that corresponds with selected one

   // Initialise output
   $str_html = '';
   
   // Set up the array for the query
   $args = array(
      'post_type' => 'spt_course',
      'post_status' => 'draft',
      'p' => $intId,
   );
   
   
   // Run query
   $myquery0 = new WP_Query($args); // Run the query
   if ($myquery0->have_posts()) :
   
      while ($myquery0->have_posts()) : $myquery0->the_post();
         $post_meta = get_post_custom();  // Retrieves all custom fields for post
   
         // Extract taxonomy details
         $course_skills_level = implode(', ',wp_get_post_terms(get_the_ID(), 'course-skills', array("fields" => "names")));
         $course_theme = implode(', ',wp_get_post_terms(get_the_ID(), 'course-theme', array("fields" => "names")));

         // Store details
         $course = array( // Create array to store required details
            'id' => get_the_ID(),
            'activity_summary' => get_the_title(),
            'activity_detail' => get_the_content(),

            // get values from taxonomy
            'skills_level' => $course_skills_level,
            'theme' => $course_theme,

            // various meta fields here
            'act_type' => $post_meta['spt_course_act_type'][0],
            'skills' => $post_meta['spt_course_skills'][0],
            'booking' => $post_meta['spt_course_booking'][0],
            'cost' => $post_meta['spt_course_cost'][0],
            'url' => $post_meta['spt_course_url'][0],
            'location' => $post_meta['spt_course_location'][0],
            'free_wifi' => $post_meta['spt_course_free_wifi'][0],
            'address' => $post_meta['spt_course_address'][0],
            'postcode' => $post_meta['spt_course_postcode'][0],
            'latlng' => $post_meta['spt_course_latlng'][0],
            'provider' => $post_meta['spt_course_provider'][0],
            'dropin_days' => $post_meta['spt_course_dropin_days'][0],
            'exceptions' => $post_meta['spt_course_exceptions'][0],
            'start_date' => $post_meta['spt_course_start_date'][0],
            'end_date' => $post_meta['spt_course_end_date'][0],
            'start_time' => $post_meta['spt_course_start_time'][0],
            'end_time' => $post_meta['spt_course_end_time'][0],
            'duration' => $post_meta['spt_course_duration'][0],
         );
         
         //////
            
         // Set up string to highlight where details have not been specified
         $not_spec =  '<span class="not-spec">'.__( 'Not specified', 'signpost' ).'</span>';
      
         
         $str_html .= '<section class="data-item">';
         $str_html .= '<h3>'.$course['activity_summary'].'</h3>';
         
         if(!empty($course['activity_detail'])) {
            $str_html .= wpautop($course['activity_detail']);
         }
         // Check for latlng value and put out mapping buttons and map div
         if (isset($course['latlng']) && (strlen($course['latlng']) > 0)) {
            if ($course['latlng'] == ',') {
               $str_html .= '<p class="not-spec"><strong>'.__( 'Geocoding not successful - needs manual input', 'signpost' ).'</strong></p>';
            } else {
               // Assumption is that Geocode was successful - so show map
               $str_html .= output_review_map($course['latlng'], $course['provider'], 'mapcanvas' );
            }
         }
         // Put out rest of data
         $str_html .= '<dl class="other-detail">';
            //Location
         if (isset($course['location']) && (strlen($course['location']) > 0)) {
            $frag = $course['location'];
         } else {
            $frag = $not_spec;
         }
         $str_html .= '<dt class="location">'.__( 'Location:', 'signpost' ).'</dt> <dd>'.$frag.'</dd>';
      
         $str_html .= '<dt class="address">'.__( 'Address:', 'signpost' ).'</dt> <dd>'.$course['address'].' '.$course['postcode'].'</dd>';
         
         $free_wifi = get_items_from_selection_array($course['free_wifi'], get_yes_no_array());
         $str_html .= '<dt>'.__( 'Free wifi:', 'signpost' ).'</dt> <dd>'.$free_wifi.'</dd>';
         
         // Tidy up cost - interpret values
         if ($course['cost'] == '') {
           $str_cost = 'Unspecified';
         } elseif ($course['cost'] == '0') {
           $str_cost = 'Free';
         } else {
           $str_cost = esc_html($course['cost']);
         }

         $str_html .= '<dt>'.__( 'Cost:', 'signpost' ).'</dt> <dd>'.$str_cost.'</dd>'; 
      
         //Skills level
         $str_html .= '<dt>'.__( 'Skills level:', 'signpost' ).'</dt> <dd>'.$course['skills_level'].'</dd>';
         //Activity Type
         $act_type = get_items_from_selection_array($course['act_type'], get_course_act_type());
         $str_html .= '<dt>'.__( 'Activity type:', 'signpost' ).'</dt> <dd>'.$act_type.'</dd>';

         //Theme
         $str_html .= '<dt>'.__( 'Theme/title:', 'signpost' ).'</dt> <dd>'.$course['theme'].'</dd>';
         //Skills
         $skills = get_items_from_selection_array($course['skills'], get_course_skills());

         $str_html .= '<dt>'.__( 'Skills:', 'signpost' ).'</dt> <dd>'.$skills.'</dd>';
         
         //Booking
         if (isset($course['booking']) && (strlen($course['booking']) > 0)) {
            $frag = $course['booking'];
         } else {
            $frag = $not_spec;
         }
         $str_html .= '<dt>'.__( 'Contact information:', 'signpost' ).'</dt> <dd>'.$frag.'</dd>';
         //URL
         if (isset($course['url']) && (strlen($course['url']) > 0)) {
            $frag = '<a href="'.$course['url'].'">'.$course['url'].'</a>';
         } else {
            $frag = $not_spec;
         }
         $str_html .= '<dt>'.__( 'Web address:', 'signpost' ).'</dt> <dd>'.$frag.'</dd>';
      
         //Provider
         if (isset($course['provider']) && (strlen($course['provider']) > 0)) {
            $frag = $course['provider'];
         } else {
            $frag = $not_spec;
         }
         $str_html .= '<dt>'.__( 'Provider:', 'signpost' ).'</dt> <dd>'.$frag.'</dd>';
      
         // Tidy up days
         $sdays = get_items_from_selection_array($course['dropin_days'], get_days_array());
         if (isset($sdays) && (strlen($sdays) > 0)) {
            $frag = $sdays;
         } else {
            $frag = $not_spec;
         }
      
         $str_html .= '<dt class="opening">'.__( 'Activity days:', 'signpost' ).'</dt> <dd>'.$frag.'</dd>';
      
         //Exceptions
         if (isset($course['exceptions']) && (strlen($course['exceptions']) > 0)) {
            $frag = $course['exceptions'];
         } else {
            $frag = $not_spec;
         }
         $str_html .= '<dt>'.__( 'Activity days exceptions:', 'signpost' ).'</dt> <dd>'.$frag.'</dd>';
      
         //Start Date
         if (isset($course['start_date']) && (strlen($course['start_date']) > 0)) {
            $frag = yxmxdToFormat($course['start_date']);
         } else {
            $frag = $not_spec;
         }
         $str_html .= '<dt>'.__( 'Start date:', 'signpost' ).'</dt> <dd>'.$frag.'</dd>';
         //End Date
         if (isset($course['end_date']) && (strlen($course['end_date']) > 0)) {
            $frag = yxmxdToFormat($course['end_date']);
         } else {
            $frag = $not_spec;
         }
         $str_html .= '<dt>'.__( 'End date:', 'signpost' ).'</dt> <dd>'.$frag.'</dd>';
      
         //Start Time
         if (isset($course['start_time']) && (strlen($course['start_time']) > 0)) {
            $frag = $course['start_time'];
         } else {
            $frag = $not_spec;
         }
         $str_html .= '<dt>'.__( 'Start time:', 'signpost' ).'</dt> <dd>'.$frag.'</dd>';
         //End Time
         if (isset($course['end_time']) && (strlen($course['end_time']) > 0)) {
            $frag = $course['end_time'];
         } else {
            $frag = $not_spec;
         }
         $str_html .= '<dt>'.__( 'End time:', 'signpost' ).'</dt> <dd>'.$frag.'</dd>';
      
         //Duration
         if (isset($course['duration']) && (strlen($course['duration']) > 0)) {
            $frag = $course['duration'];
         } else {
            $frag = $not_spec;
         }
         $str_html .= '<dt>'.__( 'Duration:', 'signpost' ).'</dt> <dd>'.$frag.'</dd>';
      
      
         $str_html .= '</dl>';
      
         $str_html .= output_review_form($page_url, $intId);
         
         $str_html .= '</section>';    
         
      endwhile;
   else :    
      // No recs found
      $str_html = '<p>'.__( 'No courses/sessions found.', 'signpost' ).'</p>';
   
   endif; 
   wp_reset_query(); // Very important - drops the query and restores where you were
   


   echo '<div id="results">'.$str_html.'</div>';
}

endwhile; 

?>

<div class="clear"></div>

<?php 
else : 
?>

<h2><?php _e( 'Content Not Found', 'signpost' ); ?></h2>
<p><?php _e( 'Sorry, but you are looking for something that is not here', 'signpost' ); ?>.</p>

<?php 
endif; 
?>

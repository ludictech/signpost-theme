<?php 
$meta = getMeta();

if (have_posts()) : while (have_posts()) : the_post(); 
?>

<h1><?php the_title(); ?></h1>

<?php 
if (user_has_necessary_rights()) {

   echo '<h3 class="admin-links">'.__( 'Admin links', 'signpost' ).'</h3>';
   echo '<ul class="home-links">';
   echo '<li><a href="'.get_theme_mod( 'signpost2015_add_internet_access_url' ).'">'.__( 'Add Internet Access location', 'signpost' ).'</a></li>';
   echo '<li><a href="'.get_theme_mod( 'signpost2015_add_course_url' ).'">'.__( 'Add Course/Session', 'signpost' ).'</a></li>';
   echo '</ul>';
}

the_content(); 
 ?>
<div class="clear"></div>
<!--
<div id="hp-forms">
<section class="access">
<h2 id="hp-access-hdr"><span><?php _e( 'Internet Access', 'signpost' ); ?></span></h2>
<ul>
<li><a href="<?php echo get_theme_mod( 'signpost2015_view_internet_access_url' ); ?>"><?php _e( 'All internet locations', 'signpost' ); ?></a></li>
</ul>
<h3 class="form-header"><?php _e( 'Filter results', 'signpost' ); ?></h3>
<form action="<?php echo get_theme_mod( 'signpost2015_view_internet_access_url' ); ?>" method="get" name="int-access" id="int-access" class="hp-form contact">
<ul>
<li>
<label for="access-place"><?php _e( 'Place/Postcode', 'signpost' ); ?></label>
<input type="text" name="access-place" id="access-place">
</li>
<li>
<label for="access-distance"><?php _e( 'How close?', 'signpost' ); ?></label>
<select name="access-distance" id="access-distance">
	<option value="" selected="selected"><?php _e( 'Please choose...', 'signpost' ); ?></option>
   <?php 
   foreach(get_distances() as $item) {
	   echo '<option value="'.$item[0].'">'.$item[1].'</option>';
   }
   ?>
</select>
</li>
<li class="submit">
<input type="submit" name="access-submit-home" id="access-submit-home" value="<?php _e( 'Search', 'signpost' ); ?>">
</li>
</ul>

</form>
</section>
<section class="course">
<h2 id="hp-course-hdr"><span><?php _e( 'Courses/Sessions', 'signpost' ); ?></span></h2>
<ul>
<li><a href="<?php echo get_theme_mod( 'signpost2015_view_courses_url' ); ?>?activitytypes%5B%5D=di"><?php _e( 'Drop-in sessions', 'signpost' ); ?></a></li>
<li><a href="<?php echo get_theme_mod( 'signpost2015_view_courses_url' ); ?>?activitytypes%5B%5D=bs"><?php _e( 'Booked sessions', 'signpost' ); ?></a></li>
<li><a href="<?php echo get_theme_mod( 'signpost2015_view_courses_url' ); ?>?activitytypes%5B%5D=bc"><?php _e( 'Booked courses', 'signpost' ); ?></a></li>
</ul>
<h3 class="form-header"><?php _e( 'Filter results', 'signpost' ); ?></h3>
<form action="<?php echo get_theme_mod( 'signpost2015_view_courses_url' ); ?>" method="get" name="course" id="course" class="hp-form contact">
<ul>
<li>
<label for="course-place"><?php _e( 'Place/Postcode', 'signpost' ); ?></label>
<input type="text" name="course-place" id="course-place">
</li>
<li>
<label for="course-distance"><?php _e( 'How close?', 'signpost' ); ?></label>
<select name="course-distance" id="course-distance">
	<option value="" selected="selected"><?php _e( 'Please choose...', 'signpost' ); ?></option>
   <?php 
   foreach(get_distances() as $item) {
	   echo '<option value="'.$item[0].'">'.$item[1].'</option>';
   }
   ?>
</select>
</li>
<li class="submit">
<input type="submit" name="course-submit-home" id="course-submit-home" value="<?php _e( 'Search', 'signpost' ); ?>">
</li>
</ul>
</form>
</section>

</div><!-- End of #hp-forms -->


<!--
<?php 

echo getSocBookmarks(get_permalink($post->ID), get_the_title(), get_the_content());

endwhile; 

?>
-->

<div class="clear"></div>

<?php 
else : 
?>

<h2><?php _e( 'Content Not Found', 'signpost' ); ?></h2>
<p><?php _e( 'Sorry, but you are looking for something that is not here', 'signpost' ); ?>.</p>

<?php 
endif; 
?>
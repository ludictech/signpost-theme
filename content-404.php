<div id="content-inner">
  <h1><?php _e( 'Page not found', 'signpost' ); ?></h1>
  <p><?php _e( 'We\'re sorry, there is no page at this address.', 'signpost' ); ?></p>
  <p><?php _e( 'Please try using our search box in the footer below to find what you need.', 'signpost' ); ?></p>
</div>
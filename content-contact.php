<?php 
$meta = getMeta();

if (have_posts()) : while (have_posts()) : the_post(); 
?>

<h1><?php the_title(); ?></h1>

<?php 
the_content(); 
// Put out the errors here if there are any
$arrErrs = getFormErrors();
global $contactForm;

echo printFormErrors($contactForm,$arrErrs );

endwhile; 

?>

<div class="clear"></div>

<?php 
else : 
?>

<h2><?php _e( 'Content Not Found', 'signpost' ); ?></h2>
<p><?php _e( 'Sorry, but you are looking for something that is not here', 'signpost' ); ?>.</p>

<?php 
endif; 
?>

<?php 
$meta = getMeta();

if (have_posts()) : while (have_posts()) : the_post(); 
?>

<h1><?php the_title(); ?></h1>

<?php 
the_content(); 

// Interpret any incoming query string - note could be from a self submission if javascript is not enabled
// Location
$location = get_string_from_get('access-place');

// distance
$distance = get_option_from_get('access-distance', get_distances());

// days
$days = process_check_box_get_items('days', get_days_array(), 'access-submit');

?>

<?php /* orig
<button id="int-access-filter-show"><?php _e( 'Edit filter options', 'signpost' ); ?></button>
 */ ?>
<h2 id="int-access-form-header" class="form-header" tabindex="-1"><?php _e( 'Filter results', 'signpost' ); ?></h2>

<?php /* orig
<button id="int-access-filter-hide"><?php _e( 'Close filter options', 'signpost' ); ?></button>
 */ ?>
<button id="int-access-filter-full" class="int-access-filter-button show"><?php _e( 'Full filter options', 'signpost' ); ?></button>
<button id="int-access-filter-reduced" class="int-access-filter-button hide"><?php _e( 'Reduced filter options', 'signpost' ); ?></button>

<form action="<?php echo get_theme_mod( 'signpost2015_view_internet_access_url' ); ?>" method="get" name="int-access" id="int-access" class="contact filter">
<ul>
<li class="short clear">
<label for="access-place"><?php _e( 'Place/Postcode', 'signpost' ); ?></label>
<input type="text" name="access-place" id="access-place" value="<?php echo $location; ?>">
</li>
<li class="short">
<label for="access-distance"><?php _e( 'How close?', 'signpost' ); ?></label>
<select name="access-distance" id="access-distance">
	<option value=""<?php echo chk_dist_sel('', $distance);?>><?php _e( 'Please choose...', 'signpost' ); ?></option>
   <?php 
   foreach(get_distances() as $item) {
	   echo '<option value="'.$item[0].'"'.chk_dist_sel($item[0], $distance).'>'.$item[1].'</option>';
   }
   ?>
</select>
</li>
<li class="check clear days extended">
<fieldset>
<legend><?php _e( 'Days of week?', 'signpost' ); ?></legend>
<ul>
<?php 
foreach(get_days_array() as $day) {
   $str_html = '<li>';
   $str_html .= '<input type="checkbox" name="days[]" id="days'.$day[0].'" value="'.$day[0].'" '.chk_val_chk($day[0], $days).'>';
   $str_html .= '<label for="days'.$day[0].'">'.$day[1].'</label>';
   $str_html .= '</li>';
   echo $str_html;
}
?>
</ul>
</fieldset>
</li>
<li class="submit">
<input type="submit" name="access-submit" id="access-submit" value="<?php _e( 'Search', 'signpost' ); ?>">
</li>
</ul>

</form>
<?php
// Put out internet access selection

// Initialise output
$str_html = '';

// First set the options() and sort() arrays
$options = array(
   'location' => $location,
   'distance' => $distance,
   'days'     => $days,  // Initially these will all be checked
);
$sort = array('distance');

// Call function to format search statement
// i - internet access, c - course
$str_html .= format_search_statement('i', $options, $sort);

// Initialise array to receive recordset
$int_access_details = array(); 

// Call function
$int_access_details = get_internet_access_details($int_access_details, $options, $sort );

if (isset($int_access_details) and (count($int_access_details) > 0)) {
   //echo '<p>Got some records</p>';
   //$options = array();  // Eg show by day
   $omit = array();     // array of fields to not show
   
   
   $str_html .= format_internet_access_array($int_access_details, $options, $omit);
} else {
   // No recs found
   $str_html .= '<p>'.__( 'No Internet Access locations were found.', 'signpost' ).'</p>';
}


echo '<div id="results" tabindex="-1" role="region" aria-labelledby="srh"><h2 id="srh">'.__( 'Search Results', 'signpost' ).'</h2>'.$str_html.'</div>';

echo getSocBookmarks(get_permalink($post->ID), get_the_title(), get_the_content());

endwhile; 

?>

<div class="clear"></div>

<?php 
else : 
?>

<h2><?php _e( 'Content Not Found', 'signpost' ); ?></h2>
<p><?php _e( 'Sorry, but you are looking for something that is not here', 'signpost' ); ?>.</p>

<?php 
endif; 
?>

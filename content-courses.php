<?php 
$meta = getMeta();
$course_act_type_array = get_course_act_type();

if (have_posts()) : while (have_posts()) : the_post(); 
?>

<h1><?php the_title(); ?></h1>

<?php 
the_content(); 

// Interpret any incoming query string
// Location
$location = get_string_from_get('course-place');

// Cost
$costs = get_string_from_get('course-cost');

// distance
if (empty($_GET['course-distance']) or ($_GET['course-distance'] == '0')) {
   $distance = 'not set';
} else {
   $distance = $_GET['course-distance'];
}
$distance = get_option_from_get('course-distance', get_distances());

// days
$days = process_check_box_get_items('days', get_days_array(), 'course-submit');

// Skills levels
$skills_levels = process_check_box_get_items('skillslevels', get_course_skills_levels(), 'course-submit');
//echo '<p>skills_levels='.$skills_levels.'</p>';

// Activity types
$activity_types = process_check_box_get_items('activitytypes', get_course_act_type(), 'course-submit');
//echo '<p>activity_types='.$activity_types.'</p>';

// Themes
$themes = process_check_box_get_items('themes', get_course_themes(), 'course-submit');
//echo '<p>themes='.$themes.'</p>';

// Skills
$skills = process_check_box_get_items('skills', get_course_skills(), 'course-submit');
//echo '<p>skills='.$skills.'</p>';

// Costs
$costs = process_check_box_get_items('costs', get_course_costs(), 'course-submit');
//echo '<p>costs='.$costs.'</p>';


?>


<h2 id="courses-form-header" class="form-header" tabindex="-1"><?php _e( 'Filter results', 'signpost' ); ?></h2>

<?php /* new */ ?>
<button id="courses-filter-full" class="courses-filter-button show"><?php _e( 'Full filter options', 'signpost' ); ?></button>
<button id="courses-filter-reduced" class="courses-filter-button hide"><?php _e( 'Reduced filter options', 'signpost' ); ?></button>

<form action="<?php echo get_theme_mod( 'signpost2015_view_courses_url' ); ?>" method="get" name="courses" id="courses" class="contact">
<ul>
<li class="short clear">
<label for="course-place"><?php _e( 'Place/Postcode', 'signpost' ); ?></label>
<input type="text" name="course-place" id="course-place" value="<?php echo $location; ?>">
</li>
<li class="short">
<label for="course-distance"><?php _e( 'How close?', 'signpost' ); ?></label>
<select name="course-distance" id="course-distance">
	<option value=""<?php echo chk_dist_sel('', $distance);?>><?php _e( 'Please choose...', 'signpost' ); ?></option>
   <?php 
   foreach(get_distances() as $item) {
	   echo '<option value="'.$item[0].'"'.chk_dist_sel($item[0], $distance).'>'.$item[1].'</option>';
   }
   ?>
</select>
</li>

<!-- Cost -->
<li class="check clear days">
<fieldset>
<legend><?php _e( 'Course/session cost', 'signpost' ); ?>?</legend>
<ul>
<?php 
foreach(get_course_costs() as $item) {
   $str_html = '<li>';
   $str_html .= '<input type="checkbox" name="costs[]" id="costs'.$item[0].'" value="'.$item[0].'" '.chk_val_chk($item[0], $costs).'>';
   $str_html .= '<label for="costs'.$item[0].'">'.$item[1].'</label>';
   $str_html .= '</li>';
   echo $str_html;
}
?>
</ul>
</fieldset>
</li>


<!-- Days-->
<li class="check clear days extended">
<fieldset>
<legend><?php _e( 'Days of week?', 'signpost' ); ?></legend>
<ul>
<?php 
foreach(get_days_array() as $day) {
   $str_html = '<li>';
   $str_html .= '<input type="checkbox" name="days[]" id="days'.$day[0].'" value="'.$day[0].'" '.chk_val_chk($day[0], $days).'>';
   $str_html .= '<label for="days'.$day[0].'">'.$day[1].'</label>';
   $str_html .= '</li>';
   echo $str_html;
}
?>
</ul>
</fieldset>
</li>
<!-- Skills level -->
<li class="check clear days extended">
<fieldset>
<legend><?php _e( 'Course/session skills level', 'signpost' ); ?>?</legend>
<ul>
<?php 
foreach(get_course_skills_levels() as $item) {
   $str_html = '<li>';
   $str_html .= '<input type="checkbox" name="skillslevels[]" id="skillslevels'.$item[0].'" value="'.$item[0].'" '.chk_val_chk($item[0], $skills_levels).'>';
   $str_html .= '<label for="skillslevels'.$item[0].'">'.$item[1].'</label>';
   $str_html .= '</li>';
   echo $str_html;
}
?>
</ul>
</fieldset>
</li>

<!-- Activity type -->
<li class="check clear days extended">
<fieldset>
<legend><?php _e( 'Course/session activity type', 'signpost' ); ?>?</legend>
<ul>
<?php 
foreach(get_course_act_type() as $item) {
   $str_html = '<li>';
   $str_html .= '<input type="checkbox" name="activitytypes[]" id="activitytypes'.$item[0].'" value="'.$item[0].'" '.chk_val_chk($item[0], $activity_types).'>';
   $str_html .= '<label for="activitytypes'.$item[0].'">'.$item[1].'</label>';
   $str_html .= '</li>';
   echo $str_html;
}
?>
</ul>
</fieldset>
</li>

<!-- Themes -->
<li class="check clear days extended">
<fieldset>
<legend><?php _e( 'Course/session theme or title', 'signpost' ); ?>?</legend>
<ul>
<?php 
foreach(get_course_themes() as $item) {
   $str_html = '<li>';
   $str_html .= '<input type="checkbox" name="themes[]" id="themes'.$item[0].'" value="'.$item[0].'" '.chk_val_chk($item[0], $themes).'>';
   $str_html .= '<label for="themes'.$item[0].'">'.$item[1].'</label>';
   $str_html .= '</li>';
   echo $str_html;
}
?>
</ul>
</fieldset>
</li>
<!-- Skills -->
<li class="check clear days extended">
<fieldset>
<legend><?php _e( 'Course/session skills', 'signpost' ); ?>?</legend>
<ul>
<?php 
foreach(get_course_skills() as $item) {
   $str_html = '<li>';
   $str_html .= '<input type="checkbox" name="skills[]" id="skills'.$item[0].'" value="'.$item[0].'" '.chk_val_chk($item[0], $skills).'>';
   $str_html .= '<label for="skills'.$item[0].'">'.$item[1].'</label>';
   $str_html .= '</li>';
   echo $str_html;
}
?>
</ul>
</fieldset>
</li>

<li class="submit">
<input type="submit" name="course-submit" id="course-submit" value="<?php _e( 'Search', 'signpost' ); ?>">
</li>
</ul>

</form>
<?php
// Put out course selection

// Initialise output
$str_html = '';


// Call function to retrieve the course records

// First set the options() and sort() arrays
$options = array(
   'location' => $location,
   'distance' => $distance,
   'days'     => $days,  // Initially these will all be checked
   'skills_levels' => $skills_levels,
   'activity_types' => $activity_types,
   'themes' => $themes,
   'skills' => $skills,
   'costs' => $costs,
   // cost in here eventually
);
$sort = array('distance');

// Call function to format earch statement
// i - internet access, c - course
//var_dump($options);
$str_html .= format_search_statement('c', $options, $sort);

$course_details = array(); // Initialise array to receive full result set

// Populate array from results of function
$course_details = get_course_details($course_details, $options, $sort );

if (isset($course_details) and (count($course_details) > 0)) {
   //$options = array();  // Eg show by day
   $omit = array();     // array of fields to not show
   $str_html .= format_course_array($course_details, $options, $omit);
} else {
   // No recs found
   $str_html .= '<p>'.__( 'No courses or sessions found.', 'signpost' ).'</p>';
}


echo '<div id="results" tabindex="-1" role="region" aria-labelledby="srh"><h2 id="srh">'.__( 'Search Results', 'signpost' ).'</h2>'.$str_html.'</div>';



echo getSocBookmarks(get_permalink($post->ID), get_the_title(), get_the_content());

endwhile; 

?>

<div class="clear"></div>

<?php 
else : 
?>

<h2><?php _e( 'Content Not Found', 'signpost' ); ?></h2>
<p><?php _e( 'Sorry, but you are looking for something that is not here', 'signpost' ); ?>.</p>

<?php 
endif; 
?>

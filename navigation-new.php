<div class="inner">
<!-- <h2>Main Navigation</h2> -->
<div id="mainnav" role="navigation" aria-label="<?php _e( 'Main navigation', 'signpost' ); ?>" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">
<a class="button" id="shownav" href="#nav">Show navigation</a>
<a class="button" id="hidenav" href="#nav">Hide navigation</a>
<div id="logo"> // needs to be removed from banner.php if included here
<?php 

// Check if image added in customizer
if ( get_theme_mod( 'signpost2015_logo' ) ) { ?>
<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
<img src="<?php echo get_theme_mod( 'signpost2015_logo' ); ?>" id="logo_img" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" />
</a>
<?php } else { ?>
<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php echo esc_attr( get_bloginfo( 'name' ) ); ?></a>
<?php } ?>
</div>
<?php
$menuArgs = array( 
   'theme_location' => 'primary',
   'menu_id' => 'nav',
   'container' => false,
   'items_wrap' => '<ul tabindex="-1" id="%1$s" class="%2$s">%3$s</ul>',
   'depth' => 2
);

 wp_nav_menu($menuArgs); ?>
<div class="clear"></div>
</div>
</div>
<div id="content-inner">

<?php 
if (have_posts()) : 
   $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
    <?php /* If this is a category archive */ 
    if (is_category()) { ?>
   <h1>Archive for &#8216;<?php single_cat_title(); ?>&#8217; Category</h1>
    <?php /* If this is a tag archive */ } 
    elseif( is_tag() ) { ?>
   <h1>Items Tagged &#8216;<?php single_tag_title(); ?>&#8217;</h1>
   <?php /* If this is an author archive */ } 
   elseif (is_author()) { ?>
   <h1>Author Archive</h1>
    <?php /* If this is a paged archive */ } 
    elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
   <h1>Archives</h1>
    <?php } ?>
<?php 
else : 
?>   
   <h1><?php bloginfo('name');?> Blog Archive</h1>
<?php 
endif; 
?>   
<?php 
global $wp;
$current_url = add_query_arg( $wp->query_string, '', home_url( $wp->request ) );

getReadSpeakerButton($current_url); ?>

<?php 
if (have_posts()) :  while (have_posts()) : the_post();

if (get_post_type() == 'post') {
   $purpose = ' <small>(News Item)</small>';
   $pType = 'Post';
} elseif (get_post_type() == 'pphw_story') {
   $purpose =  ' <small>(Story)</small>';
   $pType = 'Post';
} elseif (get_post_type() == 'pphw_resource') {
   $purpose =  ' <small>(Resource)</small>';
   $pType = 'Post';
}
?>

<div class="news-block">
<h2><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title();   echo $purpose; ?></a></h2>
<p class="news-meta before">Posted on 
<time itemprop="datePublished" datetime="<?php the_time('c'); ?>">
<?php the_time('j F Y'); ?></time>
by <?php the_author(); ?><br><span class="comments">
<a href="<?php comments_link(); ?>" class="comments"><?php comments_number('No comments','1 comment','% comments'); ?></a>
</span></p>
<?php 
echo getDefaultImage(get_the_ID());
?>

<p class="news-meta after"><?php the_excerpt(); ?><a href="<?php the_permalink(); ?>" rel="bookmark"><span class="srdr"><?php the_title(); ?>: </span>Read more</a> &gt;</p>
<p class="news-meta category"><span class="categories">Category: <?php the_category(', '); ?></span>
<?php the_tags('<br><span class="tags">Tags: ', ', ', '</span>'); ?></p>
</div><!-- End of news-block -->

<?php 
endwhile; 

global $wp_query;
// Pagination needs to go in here
if ($wp_query->max_num_pages > 1) {
   echo '<div class="paginate-links">';
   echo '<h2 class="screen-reader-text">More News and Story Pages</h2>';
   $big = 999999999; // need an unlikely integer
   
   echo paginate_links( array(
   	'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
   	'format' => '?paged=%#%',
   	'current' => max( 1, get_query_var('paged') ),
   	'total' => $wp_query->max_num_pages,
      'end_size' => 3,
      'type' => 'list',
      'prev_text' => 'Newer',
      'next_text' => 'Older',
      'before_page_number' => '<span class="srdr">Page </span>',
   	'after_page_number' => ''
   
   ) );
   echo '<div class="clear"></div></div>';
} else {}


else : ?>

<h2>Not Found</h2>
<p>Sorry, but you are looking for something that isn't here.</p>

<?php endif; ?>
</div><!-- End of content-inner -->

<div class="inner">
<!-- <h2>Main Navigation</h2> -->
<div id="mainnav" role="navigation" aria-label="<?php _e( 'Main navigation', 'signpost' ); ?>" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">
<a class="button" id="shownav" href="#nav">Show navigation</a>
<a class="button" id="hidenav" href="#nav">Hide navigation</a>
<?php
$menuArgs = array( 
   'theme_location' => 'primary',
   'menu_id' => 'nav',
   'container' => false,
   'items_wrap' => '<ul tabindex="-1" id="%1$s" class="%2$s">%3$s</ul>',
   'depth' => 2
);

 wp_nav_menu($menuArgs); ?>
<div class="clear"></div>
</div>
</div>
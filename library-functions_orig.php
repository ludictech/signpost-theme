<?php
// Library functions
add_filter('widget_text', 'do_shortcode');


// Remove Various information from the header
// Do Not do this if you use it
remove_action('wp_head','wp_generator');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'rsd_link'); 

// Add support for thumbnails
if ( function_exists( 'add_theme_support' ) ) { 
  add_theme_support( 'post-thumbnails' ); 
}

/* Date functions */
function yxmxdToFormat($strDate, $pattern = ''){// Converts yyyy-mm-dd  to day month year
   //return date($pattern,mktime(0,0,0,substr($strDate,5,2),substr($strDate,8,2),substr($strDate,0,4))); 
   if (empty($pattern)) $pattern = get_option( 'date_format' );
   
   // This function now uses the internationalisation function to return the date in appropriate language
   return date_i18n( $pattern,mktime(0,0,0,substr($strDate,5,2),substr($strDate,8,2),substr($strDate,0,4)));
}


////////////////////////// File Functions //////////////////////////
function appendToFile($outFile,$newLine) {
   $ret = false;
   $handle = fopen($outFile, "a") or exit;
   fwrite($handle, $newLine); 
   fclose($handle); 
   $ret = true;
   return $ret;
}


///////////////////////// Common shortcodes /////////////////////////
function clear_sc($atts, $content = null) {
  return '<div class="clear"></div>';
}
add_shortcode("clear", "clear_sc");

function break_sc($atts, $content = null) {
  return '<br>';
}
add_shortcode("break", "break_sc");


add_filter('widget_text', 'do_shortcode');


///////////////////////// Email Functions ///////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

function WPEmail($remail, $rname, $semail, $sname, $cc, $bcc, $subject, $message, $attachments, $priority, $type) {
   // To
   // $to = $rname.' <'.$remail.'>';
   $to = $remail;
   // From
   $headers[] = 'From: '.$sname.' <'.$semail.'>';
   
   add_filter( 'wp_mail_content_type', 'set_html_content_type' );
   $ret = wp_mail( $to, $subject, $message, $headers, $attachments );
   remove_filter( 'wp_mail_content_type', 'set_html_content_type' );
   
   return $ret;
}

function set_html_content_type() {

	return 'text/html';
}


///////////////////////// Security Questions ////////////////////////
$secureQs = array(
   array('4 plus 2 = ?','6'),
   array('1 plus 3 = ?','4'),
   array('4 plus 3 = ?','7'),
   array('5 plus 2 = ?','7'),
   array('3 plus 2 = ?','5'),
   array('1 plus 2 = ?','3'),
   array('3 plus 3 = ?','6'),
   array('1 plus 1 = ?','2')
   );

function getSecArrayCount() {
   global $secureQs;
   return count($secureQs);
}

function getSecQ($i) {
   global $secureQs;
   return $secureQs[$i][0];
}
function getSecA($i) {
   global $secureQs;
   return $secureQs[$i][1];
}


//////////////////// String processing functions ///////////////////////////

/** Method to add hyperlink html tags to any urls, twitter ids or hashtags in the tweet */ 
function processLinks($text) {
	$text = utf8_decode( $text );
	$text = preg_replace('@(https?://([-\w\.]+)+(d+)?(/([\w/_\.]*(\?\S+)?)?)?)@', '<a target="sfs" href="$1">$1</a>',  $text );
	$text = preg_replace("#(^|[\n ])@([^ \"\t\n\r<]*)#ise", "'\\1<a target=\"sfs\" href=\"http://www.twitter.com/\\2\" >@\\2</a>'", $text);  
	$text = preg_replace("#(^|[\n ])\#([^ \"\t\n\r<]*)#ise", "'\\1<a target=\"sfs\" href=\"http://twitter.com/search?q=%23\\2\" >#\\2</a>'", $text);
	//$text = preg_replace("#(^|[\n ])\#([^ \"\t\n\r<]*)#ise", "'\\1<a target=\"sfs\" href=\"http://hashtags.org/search?query=\\2\" >#\\2</a>'", $text);
	return $text;
}

function formatUrlsInText($text) {
// Replaces urls in text with links to site mentioned.
   return preg_replace('@(http)?(s)?(://)?(([-\w]+\.)+([^\s]+)+[^,.\s])@', '<a href="http$2://$4">$1$2$3$4</a>', $text);
}
function formatEmailsInText($text) {
   return preg_replace('/([a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6})/', '<a href="mailto:$1">$1</a>', $text);
   
}

function formatEmailsAndUrlsInText($text) {
   return formatUrlsInText(formatEmailsInText($text));
}

///////////////////////// Social Bookmarks Stuff ////////////////////////
function getSocBookmarks($pageUrl, $pageTitle, $content, $reqBookmarks = null) {
/* Called from a page or a post this function writes out a series of social bookmarks.
   Accepts URL and title of page. An optional 3rd paragrph specifies the bookmarks to include and the order. If not passed then the default selection is used. */
   if ($reqBookmarks == null) {
      $reqBookmarks = array(
         'Facebook', 
         'Twitter', 
         'LinkedIn', 
         'Delicious', 
         'Digg', 
         'Posterous',
         'Reddit',
      );
   }
   
   // Add site name to front of title
   $pageTitle = get_bloginfo('name').': '.$pageTitle;
   
   $temp = strip_shortcodes($content);
   $content = strip_tags($temp);
   
   $imgTransp = '<img src="'.get_bloginfo ('template_url').'/images/transp.gif" height="30" width="30" alt="'.__('Share this page on XXXXX (opens new window)','signpost').'">';

   $strHtml = '<div class="soc-book"><p><strong>'.__('Share this page (Links open new windows/tabs)','signpost').'</strong></p><ul>';
   foreach($reqBookmarks as $bookmark) {
      switch ($bookmark) {
         case 'Facebook':
            $max = 70; // Longest allowed string for title
            $shortTitle = shortenText($pageTitle, 70,true);
            $strHtml .= '<li><a class="facebook" rel="nofollow" target="_blank"  href="http://www.facebook.com/sharer.php?u='.urlencode($pageUrl).'&amp;t='.urlencode($shortTitle).'"  >'.str_replace ( 'XXXXX', $bookmark, $imgTransp).'</a></li>';
            break; 

         case 'Twitter': 
            $shortTitle = shortenText($pageTitle, 60,true);
            $strHtml .= '<li><a class="twitter" rel="nofollow" target="_blank"  href="http://twitter.com/share?url='.urlencode($pageUrl).'&amp;text='.urlencode($shortTitle).'">'.str_replace ( 'XXXXX', $bookmark, $imgTransp).'</a></li>';
            break; 
         
         case 'LinkedIn': 
         
            $excerpt =  shortenText($content, 50,true);

            $strHtml .= '<li><a class="linkedin" rel="nofollow" target="_blank"  href="http://www.linkedin.com/shareArticle?mini=true&amp;url='.urlencode($pageUrl).'">'.str_replace ( 'XXXXX', $bookmark, $imgTransp).'</a></li>';

            break; 
         
         case 'Delicious': 
            $max = 70; // Logest allowed string for title
            $shortTitle = shortenText($pageTitle, 70,true);
            $strHtml .= '<li><a class="delicious" rel="nofollow" target="_blank"  href="http://delicious.com/post?url='.urlencode($pageUrl).'&amp;title='.urlencode($shortTitle).'"  >'.str_replace ( 'XXXXX', $bookmark, $imgTransp).'</a></li>';

            break; 
         
         case 'Digg': 
            $max = 70; // Logest allowed string for title
            $shortTitle = shortenText($pageTitle, 70,true);
            $strHtml .= '<li><a class="digg" rel="nofollow" target="_blank"  href="http://digg.com/submit?url='.urlencode($pageUrl).'&amp;title='.urlencode($shortTitle).'"  >'.str_replace ( 'XXXXX', $bookmark, $imgTransp).'</a></li>';

            break; 
         
         case 'Posterous':
            $max = 70; // Logest allowed string for title
            $shortTitle = shortenText($pageTitle, 70,true);
            $strHtml .= '<li><a class="posterous" rel="nofollow" target="_blank"  href="http://www.posterous.com/share?linkto='.urlencode($pageUrl).'&amp;title='.urlencode($shortTitle).'"  >'.str_replace ( 'XXXXX', $bookmark, $imgTransp).'</a></li>';
            
            break; 
         
         case 'Reddit':
            $max = 70; // Logest allowed string for title
            $shortTitle = shortenText($pageTitle, 70,true);
            $strHtml .= '<li><a class="reddit" rel="nofollow" target="_blank"  href="http://www.reddit.com/submit?url='.urlencode($pageUrl).'&amp;title='.urlencode($shortTitle).'"  >'.str_replace ( 'XXXXX', $bookmark, $imgTransp).'</a></li>';
            
            break; 
      }
   }
   $strHtml .= '</ul>';
   $strHtml .= '</div>';
   return $strHtml;
}


function shortenText($text, $chars, $add = false) { 
// Shortens $text to word boundary if longer than specified length - $chars.
// Optional parameter $add determines whether to show elipsis
   
   // First check to see if string is longer than allowed
   if (strlen($text) < $chars ) {
      // no need to shorten text so return orig
      return $text;
   }
   
   
   if ($add && ($chars > 3)) {  
   // If elipsis required and allowed length greater than 3
      $newChars = $chars - 3;  // take 3 off allowed length
      
      // Try and break the string at a suitable
      $newText = substr($text, 0, strrpos(substr($text, 0, $newChars), ' '));
      
      if (strlen($newText) == 0 ) {
         $newText = substr($text, 0, $newChars).'...';
      } else {
         $newText = $newText.'...';
      }
   } else {
      $newText = substr($text, 0, strrpos(substr($text, 0, $chars), ' '));
      if (strlen($newText) == 0 ) {
         $newText = substr($text, 0, $chars);
      }
   }
   return $newText; 

} 

///////////////////////// Check Array ///////////////////////////////
// Function to check an array of array for presence of a value
// Returns true or false, or the value of an index if required
function ccCheckArr($myArr, $val) {
   $ret = false;
   
   foreach($myArr as $item) {
      if ($item[0] == $val) return true;
   }
   
   return false;
}
function ccReturnArr($myArr, $val) {
   $ret = false;
   
   foreach($myArr as $item) {
      if ($item[0] == $val) {
         return $item[1];
      echo 'item[1] = '.$item[1];
         break;
      }
   }
   
   return false;
}


function array_orderby() {
/****************************************************************************************
   Function to help sort associative arrays

   Pass the array, followed by the column names and sort flags
   $sorted = array_orderby($data, 'volume', SORT_DESC, 'edition', SORT_ASC);


***************************************************************************************/
    $args = func_get_args();
    $data = array_shift($args);
    foreach ($args as $n => $field) {
        if (is_string($field)) {
            $tmp = array();
            foreach ($data as $key => $row)
                $tmp[$key] = $row[$field];
            $args[$n] = $tmp;
            }
    }
    $args[] = &$data;
    call_user_func_array('array_multisort', $args);
    return array_pop($args);
}

function possible_array_values_as_string($array, $what = 0, $delimiter = ',') {
/********************************************************************************************
   Function to output a delimited string with all possible values from a checkbox or select array.
   Values returned are the code values, unless specified in param 2
   There are three params:
   1) Array to process
   2) what - 0 default = code, 1 = literal. All other values are invalid
   3) delimiter - defaults to a comma
*********************************************************************************************/
   
   // Initialise return
   $ret = '';
   
   // Check values
   if (($what <> 0) and ($what <> 1)) return $ret;
   
   $tmp_arr = array();
   
   for ($i = 0; $i < count($array); $i++) {
      $tmp_arr[] = $array[$i][$what];
   }
   
   // Now create the string
   $ret = implode(',',$tmp_arr);
   
   return $ret;
}


//// Process XML feed

function getXmlFeed($xmlPath) {
   $success = true;
   $errMsg = '';
   
   // Check that xmlpath supplied
   if (empty($xmlPath)) {
      $success = false;
      $errMsg = '<p>Path to XML Feed not supplied (X)</p>';
      return array($success, $errMsg);
   }
   
   // Try to open the remote file
   $objResponse = wp_remote_get($xmlPath) ;
   if( is_wp_error( $objResponse ) ) {
      $success = false;
      $errMsg = '<p>We are unable to retrieve the requested information at the moment. (A)</p>';
      return array($success, $errMsg);
   } 
   
   // Get the XML out
   $strXml = $objResponse['body'];
   if (!strlen($strXml)) {
      $success = false;
      $errMsg = '<p>We are unable to retrieve the requested information at the moment. (B)</p>';
      return array($success, $errMsg);
   }
   
   // Load XML into object
   $xmlObj  = simplexml_load_string($strXml);
   if (!$xmlObj) {
      $success = false;
      $errMsg = '<p>We are unable to retrieve the requested information at the moment. (C)</p>';
      return array($success, $errMsg);
   }
   
   // Still here - return success and xml object
   return array($success, $xmlObj);
}

// Geocode an address: return array of latitude & longitude
function cc_gmap_geocode_old( $address ) {
    // Make Google Geocoding API URL
    $map_url = 'http://maps.google.com/maps/api/geocode/json?components=country:GB&address=';
    $map_url .= urlencode( $address ).'&sensor=false';
    
    // Send GET request
    $request = wp_remote_get( $map_url );

    // Get the JSON object
    $json = wp_remote_retrieve_body( $request );

    // Make sure the request was succesful or return false
    if( empty( $json ) )
        return false;
    
    // Decode the JSON object
    $json = json_decode( $json );

    // Get coordinates
    $lat = $json->results[0]->geometry->location->lat;    //latitude
    $long = $json->results[0]->geometry->location->lng;   //longitude
    
    // Return array of latitude & longitude
    return compact( 'lat', 'long' );
}
function cc_gmap_geocode( $address ) {
    // Make Google Geocoding API URL
    $map_url = 'https://maps.google.com/maps/api/geocode/json?components=country:GB&address=';
    $map_url .= urlencode( $address ).'&sensor=false';
    // &key=AIzaSyASDkZEBO7ZfSUGSOemEh2SB4dmMypuYtg
    
// get the json response
    $resp_json = file_get_contents($map_url);
     
    // decode the json
    $resp = json_decode($resp_json, true);
 
    // response status will be 'OK', if able to geocode given address 
    if($resp['status']=='OK'){
 
        // get the important data
        $lati = $resp['results'][0]['geometry']['location']['lat'];
        $longi = $resp['results'][0]['geometry']['location']['lng'];
        //$formatted_address = $resp['results'][0]['formatted_address'];
         
        // verify if data is complete
        if($lati && $longi){
         
            // put the data in the array
            $data_arr = array();            
             
            array_push(
                $data_arr, 
                    $lati, 
                    $longi 
                );
             
            return $data_arr;
             
        }else{
            return false;
        }
         
    }else{
        return false;
    }
}


/*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
/*::                                                                         :*/
/*::  This routine calculates the distance between two points (given the     :*/
/*::  latitude/longitude of those points). It is being used to calculate     :*/
/*::  the distance between two locations using GeoDataSource(TM) Products    :*/
/*::                                                                         :*/
/*::  Definitions:                                                           :*/
/*::    South latitudes are negative, east longitudes are positive           :*/
/*::                                                                         :*/
/*::  Passed to function:                                                    :*/
/*::    lat1, lon1 = Latitude and Longitude of point 1 (in decimal degrees)  :*/
/*::    lat2, lon2 = Latitude and Longitude of point 2 (in decimal degrees)  :*/
/*::    unit = the unit you desire for results                               :*/
/*::           where: 'M' is statute miles (default)                         :*/
/*::                  'K' is kilometers                                      :*/
/*::                  'N' is nautical miles                                  :*/
/*::  Worldwide cities and other features databases with latitude longitude  :*/
/*::  are available at http://www.geodatasource.com                          :*/
/*::                                                                         :*/
/*::  For enquiries, please contact sales@geodatasource.com                  :*/
/*::                                                                         :*/
/*::  Official Web site: http://www.geodatasource.com                        :*/
/*::                                                                         :*/
/*::         GeoDataSource.com (C) All Rights Reserved 2015		   		     :*/
/*::                                                                         :*/
/*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
function cc_geo_distance($lat1, $lon1, $lat2, $lon2, $unit = 'M') {

  $theta = $lon1 - $lon2;
  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
  $dist = acos($dist);
  $dist = rad2deg($dist);
  $miles = $dist * 60 * 1.1515;
  $unit = strtoupper($unit);

  if ($unit == "K") {
    return ($miles * 1.609344);
  } else if ($unit == "N") {
      return ($miles * 0.8684);
    } else {
        return $miles;
      }
}


function cc_driving_distance($lat1, $lon1, $lat2, $lon2, $mode = 'walking') {
   $url = 'http://maps.googleapis.com/maps/api/distancematrix/json?';
   $details = $url . 'origins='.$lat1.','.$lon1.'&destinations='.$lat2.','.$lon2.'&mode='.$mode.'&units=imperial&sensor=false'; 
   
   $json = file_get_contents($details);

   $details = json_decode($json, TRUE);
   
   $metres = $details['rows'][0]['elements'][0]['distance']['value'];
   
   $miles = round($metres/1609.34, 2);

   echo "<pre>"; print_r($details); echo "</pre>";
   echo "<pre>".$miles."</pre>";
   //echo "<pre>"; $details['rows']['elements']['distance']; echo "</pre>";
}



///////////////////////// Wordpress Stuff ///////////////////////////
function get_current_user_role_old () {
    global $current_user;
    get_currentuserinfo();
    $user_roles = $current_user->roles;
    $user_role = array_shift($user_roles);
    return $user_role;
};

function get_current_user_role () {
    $current_user = wp_get_current_user();
    //get_currentuserinfo();
    $user_roles = $current_user->roles;
    $user_role = array_shift($user_roles);
    return $user_role;
};

function increase_postmeta_form_limit() {
	return 120;
}
add_filter('postmeta_form_limit', 'increase_postmeta_form_limit'); 


function is_subpage() {
	global $post;                                 // load details about this page
        if ( is_page() && $post->post_parent ) {      // test to see if the page has a parent
               $parentID = $post->post_parent;        // the ID of the parent is this
               return $parentID;                      // return the ID
        } else {                                      // there is no parent so...
               return false;                          // ...the answer to the question is false
        };
};

add_theme_support( 'automatic-feed-links' );


?>
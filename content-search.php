<div id="content-inner">
<h1>Search Results for "<?php the_search_query() ?>"</h1>
<p>You searched for <strong>"<?php the_search_query() ?>"</strong> and the search returned <?php echo $wp_query->found_posts; ?> items. Here are the results:</p>

<?php 
if (have_posts()) :  while (have_posts()) : the_post(); 

if (get_post_type() == 'post') {
   $purpose = ' <small>(Blog Post)</small>';
   $pType = 'Post';
} else {
   $purpose = '';
   $pType = 'Page';
}     

?>
<div class="news-block">
<h2><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title();  echo $purpose; ?></a></h2>

<?php  
if ((get_post_type() == 'post') or (get_post_type() == 'pphw_story')) {  
?> 			
<p class="news-meta before">Posted on 
<time itemprop="datePublished" datetime="<?php the_time('c'); ?>">
<?php the_time('j F Y'); ?></time>
by <?php the_author(); ?><br><span class="comments">
<a href="<?php comments_link(); ?>" class="comments"><?php comments_number('No comments','1 comment','% comments'); ?></a>
</span></p>
<?php
} 
?>
<p class="news-meta after"><?php the_excerpt(); ?><a href="<?php the_permalink(); ?>" rel="bookmark"><span class="srdr"><?php the_title(); ?>: </span>Read more</a> &gt;</p>

<?php  
if ((get_post_type() == 'post') or (get_post_type() == 'pphw_story')) {  
?> 			
<p class="news-meta category"><span class="categories">Category: <?php the_category(', '); ?></span>
<?php the_tags('<br><span class="tags">Tags: ', ', ', '</span>'); ?></p>
<?php  
}
?>
</div><!-- End of news-block -->
<?php 
endwhile; 

global $wp_query;
// Pagination needs to go in here
if ($wp_query->max_num_pages > 1) {
   echo '<div class="paginate-links">';
   echo '<h2 class="screen-reader-text">More Search Results</h2>';
   $big = 999999999; // need an unlikely integer
   
   echo paginate_links( array(
   	'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
   	'format' => '?paged=%#%',
   	'current' => max( 1, get_query_var('paged') ),
   	'total' => $wp_query->max_num_pages,
      'end_size' => 3,
      'type' => 'list',
      'prev_text' => 'Previous',
      'next_text' => 'Next',
      'before_page_number' => '<span class="srdr">Page </span>',
   	'after_page_number' => ''
   
   ) );
   echo '<div class="clear"></div></div>';
} else {}

else : ?>

<h2>Not Found</h2>
<p>Sorry, but you are looking for something that isn't here.</p>

<?php 
endif; 
?>
</div><!-- End of content-inner -->

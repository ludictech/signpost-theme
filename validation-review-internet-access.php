<?php
$submitted = false;
$approved = false;

// Process any POST requests to validate the form
// Check to see if page is loading due to submit
if(isset($_POST['approve'])) {
   $submitted = true;
   $approved = true;
}
if(isset($_POST['editreq'])) {
   $submitted = true;
   $approved = false;
}

if($submitted) {
   
   // If we've got this far and there are no errors then the form was submitted successfully
   // Start the processing to update the new internet access record
      $postAdd = array();
      $postAdd['ID'] = $_POST['itemid'];
      if ( $approved ) {
         $postAdd['post_status'] = 'publish'; // straight to publish 
      } else {
         $postAdd['post_status'] = 'pending';
      }
      

      $pId = wp_update_post($postAdd);

      if ($pId > 0) {
         // Record successfully updated
         
      } 
      
      // Redirect based on what was pressed
      if ( $approved ) {
         header('Location: /');
         // Make sure that code below does not get executed when we redirect.
         exit;

      } else {
         header('Location: /wp-admin/post.php?post='.$_POST['itemid'].'&action=edit');
         // Make sure that code below does not get executed when we redirect.
         exit;
      
      }

} 

?>

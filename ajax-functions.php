<?php

add_action('wp_enqueue_scripts', 'cc_add_admin_js_css', 15);


function cc_add_admin_js_css() {

	//wp_enqueue_script('cc_admin_post_js', get_bloginfo ('template_url'). '/js/admin-post.js', array('jquery'), '', true);
   
   // Test function
	wp_localize_script( 'signpost-library', 'cc_ajax', array(
		'ajaxurl' => admin_url( 'admin-ajax.php' ),
		//Nonces
		'get_test' => wp_create_nonce( 'cc_get_test_nonce' ),
		//other params
		'param1' => 'param1-value',
		'param2' => 'param2-value',
		)
	);
   // Internet access function
	wp_localize_script( 'signpost-library', 'cc_int_access', array(
		'ajaxurl' => admin_url( 'admin-ajax.php' ),
		//Nonces
		'get_int_access' => wp_create_nonce( 'cc_get_internet_nonce' ),
		//other params
		//'param1' => 'param1-value',
		//'param2' => 'param2-value',
		)
	);
   // Course function
	wp_localize_script( 'signpost-library', 'cc_course', array(
		'ajaxurl' => admin_url( 'admin-ajax.php' ),
		//Nonces
		'get_course' => wp_create_nonce( 'cc_get_course_nonce' ),
		//other params
		//'param1' => 'param1-value',
		//'param2' => 'param2-value',
		)
	);

   // Javascript translate function
	wp_localize_script( 'signpost-library', 'cc_js_translate', array(
		'close_filter_options' => __( 'Close filter options', 'signpost' ),
		'edit_filter_options' => __( 'Edit filter options', 'signpost' ),
		'filters_hidden' => __( 'Filters hidden', 'signpost' ),
		'query_failed' => __( 'Query failed.', 'signpost' ),
		'query_failed_try_again' => __( 'Query failed. Please try again.', 'signpost' ),
		'search_results_returned' => __( 'Search results returned.', 'signpost' ),
		'map_shown' => __( 'Map shown.', 'signpost' ),
		'map_hidden' => __( 'Map hidden.', 'signpost' ),
		)
	);

}

add_action('wp_ajax_ccsg_get_int_access', 'ccsg_get_int_access');
add_action('wp_ajax_nopriv_ccsg_get_int_access', 'ccsg_get_int_access');

function ccsg_get_int_access() {
/*******************************************************************************************************
   Wrapper function to receive AJAX call to get list of internet access locations.
   Function will receive paramaters based on values posted in by the jquery routines - the values
   will be taken from the form on the View Internet Access Locations page.
   
   The internet access locations will be retrieved and formatted using the main functions:
   get_internet_access_details() and format_internet_access_array()
*******************************************************************************************************/
   //echo 'in function ccsg_get_int_access';
   
   check_ajax_referer( 'cc_get_internet_nonce', 'cc_security' );

   $location = sanitize_text_field($_POST['location']);
   $distance = sanitize_text_field($_POST['distance']);
   $days = sanitize_text_field($_POST['days']);
   
   //echo '<p>loc = '.$location.'  dist = '.$distance.'  days='.$days.'</p>';
   $options = array(
      'location' => $location,
      'distance' => $distance,
      'days'     => $days,  
   );
   $sort = array('distance');
   
   // initialise output string
   $str_html = '';
   
   // Call function to format search statement
   // i - internet access, c - course
   $str_html .= format_search_statement('i', $options, $sort);

   $int_access_details = array();
   
   // Now we've got the information, call the function to do the query
   $int_access_details = get_internet_access_details($int_access_details, $options, $sort );
   
   if (isset($int_access_details) and (count($int_access_details) > 0)) {
      //echo '<p>Got some records</p>';
      //$options = array();  // Eg show by day
      $omit = array();     // array of fields to not show
      $str_html .= format_internet_access_array($int_access_details, $options, $omit);
   } else {
      // No recs found
      $str_html = '<p>'.__( 'No Internet Access locations were found.', 'signpost' ).'</p>';
   }

   echo '<h2 id="srh">'.__( 'Search Results', 'signpost' ).'</h2>'.$str_html;

   
   exit();

}

add_action('wp_ajax_ccsg_get_course', 'ccsg_get_course');
add_action('wp_ajax_nopriv_ccsg_get_course', 'ccsg_get_course');

function ccsg_get_course() {
/*******************************************************************************************************
   Wrapper function to receive AJAX call to get list of courses.
   Function will receive paramaters based on values posted in by the jquery routines - the values
   will be taken from the form on the View Course Locations page.
   
   The courses will be retrieved and formatted using the main functions:
   get_course_details() and format_course_array()
*******************************************************************************************************/
   //echo 'in function ccsg_get_int_access';
   
   check_ajax_referer( 'cc_get_course_nonce', 'cc_security' );

   $location = sanitize_text_field($_POST['location']);
   $distance = sanitize_text_field($_POST['distance']);
   $days = sanitize_text_field($_POST['days']);
   $skills_levels = sanitize_text_field($_POST['skillslevels']);
   $activity_types = sanitize_text_field($_POST['activitytypes']);
   $themes = sanitize_text_field($_POST['themes']);
   $skills = sanitize_text_field($_POST['skills']);
   $costs = sanitize_text_field($_POST['costs']);
   
   //echo '<p>loc = '.$location.'  dist = '.$distance.'  days='.$days.'</p>';
   $options = array(
      'location' => $location,
      'distance' => $distance,
      'days'     => $days,  
      'skills_levels' => $skills_levels,
      'activity_types' => $activity_types,
      'themes' => $themes,
      'skills' => $skills,
      'costs' => $costs,
      
   );
   $sort = array('distance');
   
   // initialise output string
   $str_html = '';
   
   // Call function to format search statement
   // i - internet access, c - course
   $str_html .= format_search_statement('c', $options, $sort);

   $course_details = array();
   
   // Now we've got the information, call the function to do the query
   $course_details = get_course_details($course_details, $options, $sort );
   
   if (isset($course_details) and (count($course_details) > 0)) {
      //echo '<p>Got some records</p>';
      //$options = array();  // Eg show by day
      $omit = array();     // array of fields to not show
      $str_html .= format_course_array($course_details, $options, $omit);
   } else {
      // No recs found
      $str_html = '<p>'.__( 'No courses were found.', 'signpost' ).'</p>';
   }

   echo '<h2 id="srh">'.__( 'Search Results', 'signpost' ).'</h2>'.$str_html;

   
   exit();

}


add_action('wp_ajax_ccsg_test_ajax', 'ccsg_test_ajax');
add_action('wp_ajax_nopriv_ccsg_test_ajax', 'ccsg_test_ajax');


function ccsg_test_ajax() {
   //echo 'in function ccsg_test_ajax';
   //exit();

	$prefix = "_a7_";
	
	check_ajax_referer( 'cc_get_internet_nonce', 'cc_security' );
	
	$param1 = $_POST['param1'];
	
	$param2 = $_POST['param2'];

   // Set up the array for the query
   $args = array(
      'post_type' => 'spt_internet_access',
      'post_status' => 'publish',
      'posts_per_page' => '-1',
   );
	
   $int_access_details = array();
   
   $myquery0 = new WP_Query($args); // Run the query
   if ($myquery0->have_posts()) :
   
      while ($myquery0->have_posts()) : $myquery0->the_post();
      
         $int_access = array( // Create array to store required details
            'id' => get_the_ID(),
            'title' => get_the_title(),
         );
         $int_access_details[] = $int_access; // Give this posts details to the full result set
      endwhile;
   else :    
   
   endif; 
   wp_reset_query(); // Very important - drops the query and restores where you were

   $str_html = '';
   
   foreach($int_access_details as $int_access) {
      $str_html .= '<section class="data-item">';
      $str_html .= '<h3>'.$int_access['title'].'</h3>';
      $str_html .= '<p class="location">param1 = '.$param1.' param2 = '.$param2.'</p>';
      $str_html .= '</section>';
   }
   
	echo $str_html;
	exit();
}
<?php
$submitted = false;
$status = '';

// Process any POST requests to validate the form
// Check to see if page is loading due to submit
if(isset($_POST['delete'])) {
   $submitted = true;
   $status = 'trash';
}
if(isset($_POST['amend'])) {
   $submitted = true;
   $status = 'pending';
}
if(isset($_POST['hide'])) {
   $submitted = true;
   $status = 'draft';
}

if($submitted and user_has_necessary_rights()) {
   
   // If we've got this far and there are no errors then the form was submitted successfully
   // Start the processing to update the new internet access record
      $postAdd = array();
      $postAdd['ID'] = $_POST['itemid'];
      $postAdd['post_status'] = $status;

      $pId = wp_update_post($postAdd);

      if ($pId > 0) {
         // Record successfully updated so where to next
         if ($status == 'pending') header('Location: /wp-admin/post.php?post='.$_POST['itemid'].'&action=edit');
         
      } 
      // Drop through and show page
} 

?>

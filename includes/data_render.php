// https://wordpress.stackexchange.com/questions/115896/using-wp-remote-get-instead-of-file-get-contents
// https://stackoverflow.com/questions/43328595/access-a-local-json-file-in-wordpress-theme/43597562#43597562

<?php

    if ($_SERVER['REQUEST_METHOD'] === 'POST')
    {
        //Ok we got a POST, probably from a FORM, read from $_POST.
        var_dump($_POST); //Use this to see what info we got!
    }
    else
    {
       //You could assume you got a GET
       var_dump($_GET); //Use this to see what info we got!
    }

?>
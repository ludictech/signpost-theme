<?php
// access to local geojson file
// thanks to Adriano https://stackoverflow.com/a/43597562/5168907
require_once 'check_domain.php';

$directory = trailingslashit( get_template_directory_uri() );
$url = $directory . $details['our_geojson'];

// Make the request
$request = wp_remote_get( $url );
if ( is_wp_error( $request ) ) {
    echo "Error finding ", $url;
	return false; // Bail early - if there's no data then might as well stop!
}

// Retrieve the data
$json_body = wp_remote_retrieve_body( $request );
$our_geodata = json_decode( $json_body, true );  // if problems, try removing 'true'
?>
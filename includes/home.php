

<?php
get_header();
?>

<div class="page-content">
	<div class="wrapper">
		<div id="content">
			<?php
			include "check_domain.php";
			echo $site_full;
			$page_title = "Internet access, courses and sessions";
			
			echo "<h2>";
				echo _e( "$page_title", 'signpost' );
			echo "</h2>";
			?>
			
			<iframe src="https://www.google.com/maps/d/embed?mid=1kKYLYZMyOwTLACTDfcEJj4bsNK4-bX0Z" width="100%" height="480"></iframe>
			
			<!-- <div id="map" style='width: 100%; height: 400px;'></div> -->
			<!-- <div id="map2" style='width: 100%; height: 400px;'></div> -->
			
			<div><p></p></div>
			
			<div id="filterform">
			<?php include( 'filter_form.php' ); ?>
			</div>
<div><p></p></div>
			<div id="data" data-collapse>
			<?php include( 'create_list.php' ); ?>
			</div>
			<div>
				<h3><?php echo _e ( "Follow us on social media..." , "signpost" ); ?></h3>
			<div class="fb-page" data-href="<?php echo $facebook_id ?>" data-tabs="timeline, events" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="<?php echo $facebook_id ?>" class="fb-xfbml-parse-ignore"><a href="<?php echo $facebook_id ?>"><?php echo $site_full ?></a></blockquote></div>
			<div>
				<a class="twitter-timeline" href="https://twitter.com/<?php echo $twitter_id ?>" style="width: auto; max-height: 400;"><?php echo _e ( "Tweets by " , "signpost" ) , $twitter_id ?></a>
				<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
			</div>
			</div>
        </div>
	</div>
</div>
<?php get_footer(); ?>
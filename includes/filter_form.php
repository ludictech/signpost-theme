<?php
require_once("get_json.php");

global $locations;
$locations = array();
if( is_array( $results )) {
	foreach( $results as $entry ) {			// this line included in case array needs to be parsed down a level - remove if poss
		$place = $entry['area'];
		if( !in_array( $place, $locations, true ) ) {
			$locations[] = $place;
		}
	}
} else {
	echo "data is not an array";
}
?>
<form id="filterform" name="filter" action="create_list.php" title="" method="post">
<!--  <fieldset> // not sure if this is needed or good practice -->
	<h2>Filter list by location</h2>
    <select>
    	<option value="All areas" selected="selected">
    		<?php echo _e( "All areas", 'signpost' ); ?>
    	</option>
    	<?php
    		foreach ( $locations as $location ) {
    			echo "<option value=\"", $location, "\">", $location, "</option>\n\t";
    		}
    	?>
    </select>
<!--  </fieldset> // not sure if this is needed or good practice -->
</form>
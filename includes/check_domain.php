<?php
// print_r( apache_request_headers() );
$domain = $_SERVER['HTTP_HOST'];
$details = 'domain check did not work';

if (
	$domain == ( 'fbarton.alwaysdata.net' || 'digitalbrightonandhove.org.uk' || 'www.digitalbrightonandhove.org.uk' ) ) {
		$details = array(
			'site' => 'brighton',
			'site_full' => 'Digital Brighton and Hove',
			'facebook_id' => 'https://www.facebook.com/DigitalBrightonAndHove/',
			'twitter_id' => 'DigiBTN_Hove',
			'lang' => 'en_GB',
			'gmap' => '1kKYLYZMyOwTLACTDfcEJj4bsNK4-bX0Z',
			'our_geojson' => 'json/dbh_list.geojson',
			'favicon' => 'https://digitalbrightonandhove.org.uk/files/2019/03/dbh_logo_300x.png'
		);
		$our_json = 'json/dbh_list.json';
	}
else if (
	$domain == ( 'digitalgwynedd.wales' || 'www.digitalgwynedd.wales' ) ) {
		$details = array(
			'site' => 'gwynedd_en',
			'site_full' => 'Digital Gwynedd',
			'facebook_id' => 'https://www.facebook.com/GwyneddDdigidol/',
			'twitter_id' => 'GwyneddDdigidol',
			'lang' => 'en_GB',
			'gmap' => '1qP8KoLRjTsoiBAjbn_yYDRHJBNlZqJXJ',
			'our_json' => 'json/dgw_en_list.json',
			'favicon' => ''
		);
	}
else if (
	$domain == ( 'gwyneddddigidol.cymru' || 'www.gwyneddddigidol.cymru' ) ) {
		$details = array(
			'site' => 'gwynedd_cy',
			'site_full' => 'Gwynedd Ddigidol',
			'facebook_id' => 'https://www.facebook.com/GwyneddDdigidol/',
			'twitter_id' => 'GwyneddDdigidol',
			'lang' => 'cy_GB',
			'gmap' => '1qP8KoLRjTsoiBAjbn_yYDRHJBNlZqJXJ',
			'our_json' => 'json/dgw_cy_list.json',
			'favicon' => ''
		);
	}
else
	$details = 'no appropriate domain found';
// print_r( $details );
?>
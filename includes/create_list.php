<?php
require_once 'get_json.php';
?>

<?php
/*
// not needed if $locations below works
	$locations_list = array(
		"Abermaw / Barmouth",
		"Bangor",
		"Blaenau Ffestiniog",
		"Caernarfon",
		"Deiniolen / Llanberis",
		"Dolgellau",
		"Harlech",
		"Porthmadog",
		"Pwllheli",
		"Tywyn",
		"Y Bala / Bala"
	);
*/
/*
// only show data in the session body if its key matches this list
// might be better eventually to convert into a negative list i.e. don't show data whose key matches,
// once list of keys in the json has settled down
$show_fields = array(
	"open_days",
	"venue_postcode",
	"asset_type",
	// "asset_name_cy",
    // "asset_name",
    "asset_desc",
	"venue_address_cy",
	"venue_address",
	//		"location_area",
	"session_contact_cy",
	"session_contact",
	"info_url_cy",
	"info_url",
    "provider",
    "venue_info",
);

function print_info($item, $show_fields) {
	echo "<ul>\n";
		foreach ( $item as $key => $value ) {
			if ( in_array( $key, $show_fields ) ) {
				if ( is_array( $value ) ) {
					echo "\t<li class=\"$key\">";
					echo _e( "$key", 'signpost' ), ": ";
					foreach ( $value as $day ) {
						echo _e( "$day", 'signpost' ), " ";
					}
					echo "</li>\n";
				} else {
					echo "\t<li class=\"", $key, "\">", $value, "</li>\n";
				}
			}			
		}
	echo "</ul>\n\n";
}

/*
if ( is_ajax() ) {
		if ( isset( $_POST["action"] ) && !empty( $_POST["action"] ) ) {
			$action = $_POST["action"];
			// switch( $action ) {
			//	case "test": test_function(); break;
			// }
			$return = $_POST;
			echo $action;
			echo $return;
		}
}

function is_ajax() {
	return isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && strtolower( $_SERVER['HTTP_X_REQUESTED_WITH'] ) == 'xmlhttprequest';
}

// function test_function() {
//	$return = $_POST;
//	echo $return;
//}
*/
/*
foreach ( $locations as $location ) {
	echo "<h3 class=\"location\">", $location , "</h3>\n<div data-collapse>";
		foreach ( $results as $item ) {
			if ( is_array( $item ) ) {
				$id    = $item['post_id'];
				$name  = $item['asset_name'];
				$type  = $item['asset_type'];
				$place = $item['area'];
				if ( $place === $location ) {
					echo "<h4 class=\"session\" id=\"session-", $id, "\">\n\t";
						if ( $name ) {
							echo _e( "$name", 'signpost' );
						} else
							echo _e( "$type", 'signpost' );
							echo "\n</h4>\n";
							print_info( $item, $show_fields );
				}
			}
		}
	echo "</div>\n";
}
*/
?>
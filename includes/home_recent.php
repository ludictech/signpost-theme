<?php
get_header(); ?>

<?php
$page_title = "Internet access, courses and sessions";



// Define the URL
$dgw_geo = 'gwynddig_sessions.json';
$dbh_geo = 'dbh_assets_orig.geojson';

$url = $dgw_geo;

// Make the request
$request = wp_remote_get( $url );
if( is_wp_error( $request ) ) {
//	$request = file_get_contents( $url ); //  shouldn't need this fallback because $request shouldn't ever fail
//	if( false == $request ) {
		echo "Error";
		return false; // Bail early - if there's no data then might as well stop!
//	}
}
$dbh_geo_request = wp_remote_get( $dbh_geo );
if( is_wp_error( $dbh_geo_request ) ) {
		echo "Error";
		return false; // Bail early - if there's no data then might as well stop!
}

// Retrieve the data
$json_body = wp_remote_retrieve_body( $request );
$results = json_decode( $json_body, true );

$dbh_geo_body = wp_remote_retrieve_body( $dbh_geo_request );
$dbh_geo_data = json_decode( $dbh_geo_body, true );

/*	// not needed if $locations below works
	$locations_list = array(
		"Abermaw / Barmouth",
		"Bangor",
		"Blaenau Ffestiniog",
		"Caernarfon",
		"Deiniolen / Llanberis",
		"Dolgellau",
		"Harlech",
		"Porthmadog",
		"Pwllheli",
		"Tywyn",
		"Y Bala / Bala"
	);
*/

// only show data in the session body if its key matches this list
// might be better eventually to convert into a negative list i.e. don't show data whose key matches,
// once list of keys in the json has settled down
$show_fields = array(
	"open_days",
	"venue_postcode",
	"asset_type",
	"venue_address_cy",
	//		"location_area",
	"session_contact_cy",
	"info_url_cy",
	"course_type"
);

// $report = var_dump( $results );
// echo "$report";

// $q = $_REQUEST["q"];

$locations = array();
if( is_array( $results )) {
	foreach( $results as $entry ) {			// this line included in case array needs to be parsed down a level - remove if poss
		$place = $entry['location_area'];
		if( !in_array( $place, $locations, true ) ) {
			$locations[] = $place;
		}
	}
} else {
	echo "data is not an array";
}
?>

<div class="page-content">
	<div class="wrapper">
		<div id="content">
			<?php echo "<pre>"; print_r( var_dump( $dbh_geo_data ) ); echo "</pre>"; ?>
			
			<div id="map" style='width: 100%; height: 400px;'></div>
			<script>
				// https://docs.mapbox.com/help/tutorials/google-to-mapbox/
				mapboxgl.accessToken = 'pk.eyJ1IjoiZnJhbmNpc2JhcnRvbiIsImEiOiJjanJhcWhna2MwNXppNDRudngzZWV6bjBsIn0.BOVhsy0Ts-EYnYgErZ7v6Q';
				var map = new mapboxgl.Map({
					container: 'map',
					// style: 'mapbox://styles/francisbarton/cjrar8asr0esy2so6z4tyb9hl',
					style: 'mapbox://styles/mapbox/streets-v11',
					// style: 'mapbox.streets',
					zoom: 11.0,
					center: [-0.126, 50.835]
				});
				
				// add navigation controls
				var nav = new mapboxgl.NavigationControl();
					map.addControl(nav, 'top-left');
				// add fullscreen control
				map.addControl(new mapboxgl.FullscreenControl({map: document.querySelector('body')}));
				
				// add GeoJSON source https://docs.mapbox.com/mapbox-gl-js/api/#geojsonsource
				map.addSource('dbh_geo_body', {
					type: 'geojson',
					data: 'dbh_geo_data'
				});

				// add markers to map
				dbh_geo_file.features.forEach(function(marker) {

					// create a HTML element for each feature
					var el = document.createElement('div');
						el.className = 'marker';

					// make a marker for each feature and add to the map
					new mapboxgl.Marker(el)
    					.setLngLat(marker.geometry.coordinates)
    					.addTo(map);
				});
			</script>
			
			<!-- <div id="map2" style='width: 100%; height: 400px;'></div>
			<script>
				L.mapbox.accessToken = 'pk.eyJ1IjoiZnJhbmNpc2JhcnRvbiIsImEiOiJjanJhcWhna2MwNXppNDRudngzZWV6bjBsIn0.BOVhsy0Ts-EYnYgErZ7v6Q';

				var map = L.mapbox.map( 'map2', 'mapbox.streets' )
					.setView( [-0.116, 50.835], 11.0 );
				// .featureLayer.setGeoJSON( 'dbh_geo_body' );
				// var layer = L.mapbox.tileLayer('mapbox.streets');
				// var featureLayer = L.mapbox.featureLayer(dbh_geo_body)
    			//	.addTo(map);
    			// var featureLayer = L.mapbox.featureLayer()
    			//	.addTo(map);
				// featureLayer.loadURL('dbh_assets_orig.geojson');
			</script>
			-->
			
			<div id="filterform">
			<form id="filter" name="filter" action="data_render.php" title="" method="post">
			<!--  <fieldset> // not sure if this is needed or good practice -->
				<h2>Filter list by location</h2>
        		<select>
        			<option value="All areas" selected="selected">
        				<?php echo _e( "All areas", 'signpost' ); ?>
        			</option>
        			<?php
        			foreach ( $locations as $location ) {
        				echo "<option value=\"", $location,"\">", $location, "</option>\n\t";
        			}
        			?>
        		</select>
        	<!--  </fieldset> // not sure if this is needed or good practice -->
        	</form>
			</div>
			
			<?php echo "<pre>"; print_r( var_dump( $filter ) ); echo "</pre>"; ?>

			<?php echo "<h2>";
					echo _e( "$page_title", 'signpost' );
				echo "</h2>";
			?>
			<!-- <h2>mynediad rhyngrwyd, cyrsiau a sesiynau</h2> -->
			<div id="data">
				<?php include 'data_render.php'; ?>
			</div>
        </div>
	</div>
</div>
<?php get_footer(); ?>
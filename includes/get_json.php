<?php
$domain = $_SERVER['HTTP_HOST'];
$details = 'domain check did not work';

if ( in_array( $domain, array( "digitalgwynedd.wales", "www.digitalgwynedd.wales" ) ) ) {
    $details = array(
		'site' => 'gwynedd_en',
		'site_full' => 'Digital Gwynedd',
		'facebook_id' => 'https://www.facebook.com/GwyneddDdigidol/',
		'twitter_id' => 'GwyneddDdigidol',
		'lang' => 'en_GB',
		'gmap' => '1qP8KoLRjTsoiBAjbn_yYDRHJBNlZqJXJ',
		'our_json' => 'json/dgw_en_list.json',
		'favicon' => ''
	);
}
else if ( in_array( $domain, array( "digitalbrightonandhove.org.uk", "www.digitalbrightonandhove.org.uk") ) ) {
	$details = array(
		'site' => 'brighton',
		'site_full' => 'Digital Brighton and Hove',
		'facebook_id' => 'https://www.facebook.com/DigitalBrightonAndHove/',
		'twitter_id' => 'DigiBTN_Hove',
		'lang' => 'en_GB',
		'gmap' => '1kKYLYZMyOwTLACTDfcEJj4bsNK4-bX0Z',
		'our_geojson' => 'json/dbh_list.geojson',
		'favicon' => 'https://digitalbrightonandhove.org.uk/files/2019/03/dbh_logo_300x.png',
		'our_json' => 'json/dbh_list.json'
	);
}
else if ( in_array( $domain, array( "gwyneddddigidol.cymru", "www.gwyneddddigidol.cymru" ) ) ) {
	$details = array(
		'site' => 'gwynedd_cy',
		'site_full' => 'Gwynedd Ddigidol',
		'facebook_id' => 'https://www.facebook.com/GwyneddDdigidol/',
		'twitter_id' => 'GwyneddDdigidol',
		'lang' => 'cy_GB',
		'gmap' => '1qP8KoLRjTsoiBAjbn_yYDRHJBNlZqJXJ',
		'our_json' => 'json/dgw_cy_list.json',
		'favicon' => ''
	);
}
else $details = 'no appropriate domain found';

// access to local json file
// thanks to Adriano https://stackoverflow.com/a/43597562/5168907
$directory = trailingslashit( get_template_directory_uri() );
$url = $directory . $details['our_json'];
// $geo_url = $directory . $our_geojson;

// Make the request
$request = wp_remote_get( $url );
if ( is_wp_error( $request ) ) {
    echo "Error finding ", $url;
return false; // Bail early - if there's no data then might as well stop!
}

// Retrieve the data
$json_body = wp_remote_retrieve_body( $request );
// $geojson_body = wp_remote_retrieve_body( $geo_request );
$our_data = json_decode( $json_body, true );  // if problems, try removing 'true'
// $geo_results = json_decode( $geojson_body, true );  // if problems, try removing 'true'


// echo "<pre>"; print_r( var_dump( $our_data ) ); echo "</pre>"; // works - but removed for now
?>
<div id="bottom-inner">
<div id="footer" role="contentinfo">
<div id="footernav-cont">
<?php
$menuArgs = array( 
   'theme_location' => 'footer',
   'menu_id' => 'footernav',
   'container' => false,
   'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>'
);

 wp_nav_menu($menuArgs); ?>
<div class="clear"></div>
<div id="footer-credits">
<p>
&copy; <?php bloginfo( 'name' ); ?> <?php echo date('Y'); ?>. |
<?php _e( 'Site designed by', 'signpost' ); ?> <a href="https://qwertyop.co.uk/">Qwertyop Design</a> | 
<?php _e( 'Built by', 'signpost' ); ?> <a href="https://coolfields.co.uk/">Coolfields Consulting</a></p>
<p><?php _e( 'To report any problems with using this site, please ', 'signpost' ); ?> <a href="contact-us"><?php _e( 'Contact us', 'signpost' ); ?></a></p>
<!-- [ivory-search id="845" title="Search site"] -->
</div><!-- End of #footer-credits -->
</div><!-- End of #footernav-cont -->
<div id="footer-logos">
<?php dynamic_sidebar( 'footer-logos' ); ?>
</div><!-- End of #footer-logos -->
<?php
?>

</div><!-- end of #footer -->
</div><!-- end of #bottom-inner -->

<?php	wp_footer(); ?>
jQuery(document).ready( function($) {

// the list functions
function buildlocationlist( data ) {
  $.each( data, function( index, item ) {
    var place = item.area;
    if ( $.inArray( place, locations ) === -1) {
      locations.push( place );
    }
  });
}

function buildlist( locations, data ) {
  var start = "<div id=\"list\" data-collapse>";
  list.push( start );
  $.each( locations, function( index, place ) {
    var subhead = "<h3 id=\"area-";
    subhead += index + "\" class=\"p-locality\">";
    subhead += place + "</h3><div class=\"subhead\">";
    list.push( subhead );
    $.each( data, function( index, item ) {
      $.each( types, function( no, type ) {
        if ( item.area === place && item.typecode === type  ) {
          var title = "<h4 class=\"";
          title += type + "\" id=\"";
          title += index + "\">";
          title += item.title + "</h4>";
          list.push( title );
          var prov = "<div><p class=\"provider\"><a href=\"";
          prov += item.url + "\">";
          prov += item.provider + "</a></p>";
          list.push( prov );
          var addr = "<p class=\"address p-street-address\"><span class=\"address\">";
          addr += item.address + "</span>&nbsp;<span class=\"postcode p-postal-code\">";
          addr += item.postcode + "</span></p>";
          list.push( addr );
          // var cont = "<p class=\"contact\">";
          // cont += item.contact + "</p>";
          // list.push( cont );
          var dets = "<dl class=\"details\">";
          $.each( item, function( key, value ) {
            if ( $.inArray( key, fields ) !== -1) {
              dets += "<dt class=\"" + key + "\">" + key + ":</dt>";
              dets += "<dd>" + value + "</dd>";
            }
          });
          dets += '</dl></div>';
          list.push( dets );
        }
      } );
    } );
    var close_area = "</div>";
    list.push( close_area );
  } );
  var end = "</div>";
  list.push( end );
}
////////////////////////////


// setting up some variables
datadiv = $( '#data' );
var list = [];
var types = ["ia","di","bs","bc"];
var our_data = localJSON.data;
var locations = [];
var fields = [
  "contact",
	"days",
	"dates",
	"desc",
  "access"
];

// use the functions and variables above to create the structured list
$( '#list' ).empty();
// console.info( our_data );
buildlocationlist( our_data );
// console.info( locations );
buildlist( locations, our_data );
datadiv.append( list.join('') );

new jQueryCollapse($("#demo"), {
  query: 'div h2'
});

$( ".subhead" ).accordion( {
  collapsible: true,
  heightStyle: "content"
} );

// the map
var details = localJSON.details; // selects an array of info based on which domain (site) we are on (see includes/get_json.php)
console.info( details );

var mapcode = "<figure id=\"map_embed\"><iframe height=\"480px\" width=\"100%\" src=\"https://www.google.com/maps/d/embed?mid=";
  mapcode += details.gmap;
  mapcode += "\"></iframe>";
  mapcode += "<figcaption>You can zoom in and move around the map using a trackpad or mouse. The map can also be expanded to full-screen size. ";
  mapcode += "The icon at the top-left corner opens the map key.</figcaption></figure><div</div>";

// click the div to load the google map
$( '#click' ).click( function showMap() {
  $( '#gmap' ).empty();
  // $( '#click' ).remove();
  $( '#gmap' ).animate( {
    width: '100%',
    height: '480px',
    padding: 0
  }, 500 );
  $( '#gmap' ).append( mapcode );
});

// closes first line jquery
});

  /*
  // get location selection from form, on form change
  $( "#filterform" ).change( function() {
    choice = $( this, "option:selected" ).val();
      $.ajax(
        {
          type: "POST",
          dataType: "text",
          url: "create_list.php",
          data: choice,
          success: function( choice ) {
            console.log( choice );
            $("#data").load( choice );
          }
        }
      );
  });

*/
/*
  function show(filter) {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        document.getElementById("data").innerHTML = this.responseText;
      }
    };
    xmlhttp.open("GET", "create_list.php" + filter, true);
    xmlhttp.send();
  }
*/
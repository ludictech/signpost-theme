jQuery(document).ready(function($) {
   var $errInProgress = false;
   var $fieldId = '';

   // Code here will be executed on document ready. Use $ as normal.
   $('html').addClass( 'js' ).removeClass( 'non-js' );
   //$('.initially-hidden').hide();
   

   // Navigation related functions //////////////////////////////////////////////////////////////////////////////
   $( '#nav').find('li:has(ul)')
      .attr('aria-haspopup','true')
      .doubleTapToGo();
   
   // Replicate dropdown functionality
   $hoverClass = 'hover';
   var $timer;
   // Add appropriate class to list items that are being hovered over
   // and remove when move moves out
   $('#nav').find('li').hover(
      function() {
        $(this).data("timeout", setTimeout($.proxy(function() {
           $( this ).addClass($hoverClass);
        }, this), 200));
      },
      function() {
         var timeout = $(this).data("timeout");
         if(timeout) clearTimeout(timeout);
         $( this ).removeClass($hoverClass);
      }
   )
   // Handle tabbing with dropdown menus
   $('#nav').find('li').find('a').focus(
      function() {
         $( this ).parentsUntil('#nav').addClass($hoverClass);
      }
   ).blur(
      function() {
         $( this ).parentsUntil('#nav').removeClass($hoverClass);
      }
   );
   
   // Handle the navigation for lower screen widths
   $('#shownav').addClass('active');
   $('#shownav').on('click', function(e) {
         $('#nav').addClass('active').focus();
         $('#hidenav').addClass('active');
         $( this ).removeClass('active');
         e.preventDefault();
      });
   $('#hidenav').on('click', function(e) {
         $('#nav').removeClass('active');
         $('#shownav').addClass('active').focus();
         $( this ).removeClass('active');
         e.preventDefault();
      });


   // Check things that need sizes altered ////////////////////////////////////////////////////////////////
   function checkSizes() {
      $('.map').each(function() {
         $map_id = $( this ).attr('id');
         setMapDivHeight($map_id);
      });
   }

   // Initial run of checksizes
   checkSizes();
   
   // Run it when window resizes
   $(window).resize(checkSizes);
   
   // AJAX Stuff //////////////////////////////////////////////////////////////////////////////////////////
	
   // Internet access
   $('#access-submit').click( function(event) {
      event.preventDefault();
      
      console.log('In access-submit function.');
      
      // Harvest the values from the various input fields to use as params
      var location = $('#access-place').val();
      var distance = $('#access-distance').val();
      var days_arr = new Array();
      
      $("input[name='days[]']:checked").each(function(){
         days_arr.push($(this).val());
      });
      
      var days_str = days_arr.join(',');
      
      //console.log('location='+location+' distance='+distance+' days='+days_str);
      $.post(
      	cc_int_access.ajaxurl,
      	{
      		action: 'ccsg_get_int_access',
      		location: location,	
      		distance: distance,	
      		days: days_str,	
      		cc_security: cc_int_access.get_int_access
      	},
      	function ( response ) {
                  if (response <= 0 ) {
                     console.log('Response = ' + response);
      					$("#results").html('<p>'+cc_js_translate.query_failed_try_again+'</p>');
                     update_status(cc_js_translate.query_failed);
                  } else { 
                     //console.log('In response' + response);
      					$("#results").html(response).focus();
                     update_status(cc_js_translate.search_results_returned);
                     reduced_filters_without_focus('int-access');
                  }
      	}						
      );

	});
   
   // Courses
   $('#course-submit').click( function(event) {
      event.preventDefault();
      
      console.log('In course-submit function.');
      
      // Harvest the values from the various input fields to use as params
      var location = $('#course-place').val();
      var distance = $('#course-distance').val();
      var days_str = process_checkboxes('days[]');
      var skillslevels_str = process_checkboxes('skillslevels[]');
      var activitytypes_str = process_checkboxes('activitytypes[]');
      var themes_str = process_checkboxes('themes[]');
      var skills_str = process_checkboxes('skills[]');
      var costs_str = process_checkboxes('costs[]');

      // 2018c1 Need reference to cost here.

      console.log('location='+location+' distance='+distance+' days='+days_str+' skillslevels='+
            skillslevels_str+' activitytypes='+activitytypes_str+' themes='+themes_str+' skills='+
            skills_str+' costs='+costs_str);
      $.post(
      	cc_course.ajaxurl,
      	{
      		action: 'ccsg_get_course',
      		location: location,	
      		distance: distance,	
      		days: days_str,	
            
            skillslevels: skillslevels_str ,
            activitytypes: activitytypes_str,
            themes: themes_str,
            skills: skills_str,
            costs: costs_str,
      		cc_security: cc_course.get_course
      	},
      	function ( response ) {
                  if (response <= 0 ) {
                     console.log('Response = ' + response);
      					$("#results").html('<p>'+cc_js_translate.query_failed_try_again+'</p>');
                     update_status(cc_js_translate.query_failed);
                  } else { 
                     //console.log('In response' + response);
      					$("#results").html(response).focus();
                     update_status(cc_js_translate.search_results_returned);
                     //hide_filters_without_focus('courses');  // 2018c1 May need new function call here
                     reduced_filters_without_focus('courses');  // 2018c1 May need new function call here
                  }
      	}						
      );

	});
   
   function process_checkboxes(fieldname){
   // Interpret which checkboxes within a group are checked into a comma delimeted string
      //console.log('fieldname = ' + fieldname);
      
      // Setup temorary array
      var items_arr = new Array();
      
      // Find each checked item in lost and record the value
      $("input[name='"+fieldname+"']:checked").each(function(){
         items_arr.push($(this).val());
      });
      
      // Make the string from the array
      var ret_str = items_arr.join(',');
      
      return ret_str;

   }
   
   // Filter results buttons
   // 2018c1 May need different calls here
   $('#int-access-filter-hide').click( function() {
      hide_filters('int-access');
   });
   $('#courses-filter-hide').click( function() {
      hide_filters('courses');
   });
   $('#int-access-filter-show').click( function() {
      show_filters('int-access');
   });
   $('#courses-filter-show').click( function() {
      show_filters('courses');
   });

   // 2018c1 May need new functions here

   function hide_filters(type) {
      // Show the Edit button
      $('#'+type+'-filter-show').show();
      
      // Need to hide Filter results heading, and the filter results form
      $('#'+type+'-form-header').hide();
      //$('#'+type).hide();
      $('#'+type).slideUp();
      
      // Give the Edit button focus, and then hide me
      $('#'+type+'-filter-show').focus();
      $('#'+type+'-filter-hide').hide();
      update_status(cc_js_translate.filters_hidden);
   }
   
   function show_filters(type) {
      // Show the Edit button
      $('#'+type+'-filter-hide').show();
      
      // Need to hide Filter results heading, and the filter results form
      $('#'+type+'-form-header').show();
      $('#'+type).slideDown();
      
      // Give the Edit button focus, and then hide me
      $('#'+type+'-form-header').focus();
      $('#'+type+'-filter-show').hide();
   }

   function hide_filters_without_focus(type) {
      // Show the Edit button
      $('#'+type+'-filter-show').show();
      
      // Need to hide Filter results heading, and the filter results form
      $('#'+type+'-form-header').hide();
      $('#'+type).hide();
      
      // Give the Edit button focus, and then hide me
      $('#'+type+'-filter-hide').hide();
   }
   
   // new filter results functionality
   // Courses
   $('#courses-filter-full').click( function() {
      full_filters('courses');
   });
   $('#courses-filter-reduced').click( function() {
      reduced_filters('courses');
   });
   // Internet access
   $('#int-access-filter-full').click( function() {
      full_filters('int-access');
   });
   $('#int-access-filter-reduced').click( function() {
      reduced_filters('int-access');
   });
   function full_filters(type) {
      console.log('In full_filters');
      // Show the reduced filters button
      $('#'+type+'-filter-reduced').removeClass('hide').addClass('show');
      
      // Need to hide Filter results heading, and the filter results form
      $('li.extended').show();
      
      // Give the Reduced button focus, and then hide me
      $('#'+type+'-filter-reduced').focus();
      $('#'+type+'-filter-full').removeClass('show').addClass('hide');
   }

   function reduced_filters(type) {
      console.log('In reduced_filters');
      // Show the full filters button
      $('#'+type+'-filter-full').removeClass('hide').addClass('show');
      
      // Need to hide Filter results heading, and the filter results form
      $('li.extended').hide();
      
      // Give the full button focus, and then hide me
      $('#'+type+'-filter-full').focus();
      $('#'+type+'-filter-reduced').removeClass('show').addClass('hide');
   }
   function reduced_filters_without_focus(type) {
      console.log('In reduced_filters_without_focus');
      // Show the full filters button
      $('#'+type+'-filter-full').removeClass('hide').addClass('show');
      
      // Need to hide Filter results heading, and the filter results form
      $('li.extended').hide();
      
      // Then hide me
      $('#'+type+'-filter-reduced').removeClass('show').addClass('hide');
   }

   
   // Mapping stuff ////////////////////////////////////////////////////////////////////////////////////
   
   // Check 
   if (typeof mapDiv === 'undefined') {
   } else {
      showMap(mapDiv, mapData);
   }

   // Look for any review maps - these are always visible
   // They will have one item to show
   $('.map.review').each(function() {
      // Retrieve details from the data fields on the div
      var map_id = $( this ).attr('id');
      var map_title = $( this ).attr('data-title');
      var map_lat = $( this ).attr('data-lat');
      var map_lng = $( this ).attr('data-lng');
      var map_str = $( this ).attr('data-string');
      
      // call function to create map
      initialize_map_simple(map_lat,map_lng, map_id, map_title, map_str);
   });
   
   // Clicking on a show map button - note use of dynamic selector in .on as ths caters
   // for buttons created in subsequent ajax updates to page
   $('#results').on('click','.show-map-here' ,function(e) {
      // Retrieve details from the data fields on the button
      var map_id = $( this ).attr('data-map-id');
      var map_title = $( this ).attr('data-title');
      var map_lat = $( this ).attr('data-lat');
      var map_lng = $( this ).attr('data-lng');
      var map_str = $( this ).attr('data-string');
      
      $('#block-'+map_id).removeClass('initially-hidden');
      $('#header-'+map_id).removeClass('initially-hidden');
      
      setMapDivHeight('gmap-'+map_id);
      //alert("Can you see map?");
      
      // call function to create map
      initialize_map_simple(map_lat, map_lng, 'gmap-'+map_id, map_title, map_str);
      
      // Show hide button and give it focus, then hide show button
      $('#hide-map-button-'+map_id).removeClass('initially-hidden').focus();
      $('#map-button-'+map_id).addClass('initially-hidden');
      
      update_status(cc_js_translate.map_shown);
   });
   
   // Clicking on a hide map button - note use of dynamic selector in .on as this caters
   // for buttons created in subsequent ajax updates to page
   $('#results').on('click','.hide-map-here' ,function(e) {
      // Retrieve details from the data fields on the button
      var map_id = $( this ).attr('data-map-id');
      
      $('#block-'+map_id).addClass('initially-hidden');
      $('#header-'+map_id).addClass('initially-hidden');
      
      // show show button and give it focus, then hide hide button
      $('#map-button-'+map_id).removeClass('initially-hidden').focus();
      $('#hide-map-button-'+map_id).addClass('initially-hidden');
      
      update_status(cc_js_translate.map_hidden);
      
   });


   // Form validation etc  /////////////////////////////////////////////////////////////////////////////
   // Set up appearances for return from server side validation
   $('input[aria-invalid="false"]').parent('label').addClass('valid');
   $('select[aria-invalid="false"]').parent('label').addClass('valid');
   $('textarea[aria-invalid="false"]').parent('label').addClass('valid');
   
   // Check some fields when we move away
   $('.contact input[type="text"], .contact textarea, .contact select').on('blur', function(e) {
      // Perform required validation
      validateTextSelectField($( this ));
   });
   // Check select boxes when they change
   $('.contact select').on('change', function(e) {
      // Perform required validation
      validateTextSelectField($( this ));
   });
   // Check radio buttons when value changes
   $('.contact').find('fieldset').find('input[type="radio"]').on('change', function(e) {
      // Perform required validation - we're checking for required
      // Check if radio button group is required to have one selected
      
      // traverse up to fieldset before passing to function.
      $parFieldset = $( this ).closest('fieldset');
      $ret = validateRadioGrp($( $parFieldset ));
   
   });
   
   
   $('.contact').find('input[type="submit"]').on('click', function(e) {
      $fieldId = '';
      $errInProgress = false;

      // Perform required validation - we're checking for length, email format, 
      // and required
      $( this ).parents('form')
         .find('input[type="text"], textarea, select').each(function() {
            // Call the validation routine on this field
            if(!validateTextSelectField($( this ))) {
               // A validation aerror found
               if (!$errInProgress) {
                  // Set global variable
                  $errInProgress = true;
                  
                  // strore ID of first field with error
                  $fieldId = $( this ).attr('id');
               }
            }
         
      });
      
   
   
      // Check fieldsets that have a data type
      $( this ).parents('form')
         .find('fieldset[data-type]').each(function() {
            // Find data type and branch accordingly
            $dataType = $( this ).attr('data-type');
            
            switch ($dataType) {
               case 'radio' :
                  // Check if radio button group is required to have one selected
                  if (!validateRadioGrp($( this ))) {
                     if (!$errInProgress) {
                        $errInProgress = true;  // Set global variable
                     }
                  }
                  break;
               case 'chkbox' :
                  // Check if checkbox group is required to have one selected
                  if (!validateCheckboxGrp($( this ))) {
                     if (!$errInProgress) {
                        $errInProgress = true;  // Set global variable
                     }
                  }
                  break;
               case 'dob' :
                  if (!validateDateOfBirth($( this ))) {
                     if (!$errInProgress) {
                        $errInProgress = true; // Set global variable
                     }
                  }
                  break;
            }
            
      });
      if ($errInProgress){ 
         e.preventDefault(); 
         alert('Please fix the validation errors before submitting the form.');
         $('#'+$fieldId).focus();
         /*
         */
      }
      
   });


   function validateRadioGrp(obj) {
      // We're being passed the fieldset that contains a radio button group
      
      // First check that radio button group is required
      if ($( obj ).attr('data-v-reqd')) {
         // Retrieve error message and value of any checked radio button
         $errMsg = $( obj ).attr('data-v-reqd');
         $radVal = $( obj ).find('input:radio:checked' ).val();
         //alert($errMsg);
         // if no value then error
         if (!$radVal) {
            doRadioCheckboxGroupFail($( obj), $errMsg );
            return false;
         } 
      } 
      
      // Still here then return true
      doRadioCheckboxGroupSuccess($( obj ));
      return true;
   }

   function validateCheckboxGrp(obj) {
      // We're being passed the fieldset that contains a checkbox group
      
      // First check that radio button group is required
      if ($( obj ).attr('data-v-reqd')) {
         // Retrieve error message and value of any checked radio button
         $errMsg = $( obj ).attr('data-v-reqd');
         $radVal = $( obj ).find('input:checkbox:checked' ).val();
         //alert($errMsg);
         // if no value then error
         if (!$radVal) {
            doRadioCheckboxGroupFail($( obj), $errMsg );
            return false;
         } 
      } 
      
      // Still here then return true
      doRadioCheckboxGroupSuccess($( obj ));
      return true;
   }

   function validateTextSelectField(obj) {
      // Store value of this control
      $myVal = $( obj ).val();
      
      // Check if required 
      if ($( obj ).attr('data-v-reqd')) {
         if ($myVal.length < 1) {
            $errMsg = $( obj ).attr('data-v-reqd');
            // Length less than 1 so fail this input
            doTextSelectFail($( obj ), $errMsg);
            return false; // End out of function
         } 
      } 

      // Check length
      if ($( obj ).attr('data-v-len')) {
         // Get the maxlength from 
         $arrLen = $( obj ).attr('data-v-len').split('~');
         if ($myVal.length > $arrLen[0]) {
            doTextSelectFail($( obj ), $arrLen[1]);
            return false; // End out of function
         }
      }
      // Check integer
      if ($( obj ).attr('data-v-int')) {
         if (!isInt($myVal)) {
            $errMsg = $( obj ).attr('data-v-int');
            // Length less than 1 so fail this input
            doTextSelectFail($( obj ), $errMsg);
            return false; // End out of function
         } 
      }
      if ($( obj ).attr('data-v-email')) {
         if (($myVal.length > 0) && (!isEmail($myVal))) {
            $errMsg = $( obj ).attr('data-v-email');
            doTextSelectFail($( obj ), $errMsg);
            return false; // End out of function
         
         }
      }
      if ($( obj ).attr('data-v-sqldate')) {
         if (($myVal.length > 0) && (!isSqlDate($myVal))) {
            $errMsg = $( obj ).attr('data-v-sqldate');
            doTextSelectFail($( obj ), $errMsg);
            return false; // End out of function
         
         }
      }
      if ($( obj ).attr('data-v-sq')) {
         $arrVal = $( obj ).attr('data-v-sq').split('~');
         if ($myVal != $arrVal[0]) {
            $errMsg = $arrVal[1];
            doTextSelectFail($( obj ), $errMsg);
            return false; // End out of function
         
         }
      }
      // Still here the show success
      doTextSelectSuccess($( obj ));
      return true;
   }
   
   function doTextSelectSuccess(obj) {
      $( obj ).attr('aria-invalid','false')
         .next('.errors').html('')
         .parent('label').addClass('valid');
   }
   
   function doTextSelectFail(obj, message) {
      $( obj ).attr('aria-invalid','true')
         .next('.errors').html(message)
         .parent('label').removeClass('valid');
   }
   
   function doRadioCheckboxGroupFail(obj, message) {
      // Object passed in is fieldset for radio button group
      // We need to highlight the ul within the fieldset
      // and set 
      $( obj ).children('ul').addClass('error');
      $( obj ).children('.fieldseterrors').html(message);
      $( obj ).children('legend').removeClass('valid');
   }
   function doRadioCheckboxGroupSuccess(obj) {
      // Object passed in is fieldset for radio button group
      // We need to highlight the ul within the fieldset
      // and set 
      
      $( obj ).children('ul').removeClass('error');
      $( obj ).children('.fieldseterrors').html('');
      $( obj ).children('legend').addClass('valid');
   }
   function doDobFail(obj, message) {
      // Object passed in is fieldset for radio button group
      // We need to highlight the ul within the fieldset
      // and set 
      $( obj ).children('.fieldseterrors').html(message);
      $( obj ).find('li.dob-d select').attr('aria-invalid','true');
      $( obj ).children('legend').removeClass('valid');
   }
   function doDobSuccess(obj) {
      // Object passed in is fieldset for radio button group
      // We need to highlight the ul within the fieldset
      // and set 
      $( obj ).children('.fieldseterrors').html('');
      $( obj ).find('li.dob-d select').attr('aria-invalid','false');
      $( obj ).children('legend').addClass('valid');
   }
   
   //////// Function to update status div - it's an aria-live region
   function update_status(str){
      $( '#status-txt' ).html('');
      $( '#status-txt' ).html(str);
   }

   //////// Function to set cookie cookie and hide cookie message
   $('#cookie-accept-js').on('click', function(e) {
      Cookies.set('signpostcookieaccept', '1', { expires: 360 });
      $('#banner-cookie').hide('slow');
      e.preventDefault();
      $('#banner-inner').focus();
   });
});

// General helper functions
function isEmail(email) {
   var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
   return regex.test(email);
}
function isInt(value) {
   return !isNaN(value) && parseInt(Number(value)) == value;
}
function isSqlDate(strdate) {
   var regex = /^(2[0-9]{3})-(0[1-9]|1[012])-([123]0|[012][1-9]|31)$/;
   return regex.test(strdate);
}


function isValidDate(text) {
   // Expecting m/d/yyyy
    var date = Date.parse(text);

    if (isNaN(date)) {
        return false;
    }

    var comp = text.split('/');

    if (comp.length !== 3) {
        return false;
    }

    var m = parseInt(comp[0], 10);
    var d = parseInt(comp[1], 10);
    var y = parseInt(comp[2], 10);
    var date = new Date(y, m - 1, d);
    return (date.getFullYear() == y && date.getMonth() + 1 == m && date.getDate() == d);
}



//////////////// Google Map API Scripts ///////////////////////////////////
function initialize_map_simple(lat, lng, mapHolder, strTitle, strStr) {
    //var myLatlng = new google.maps.LatLng(45.124099,-123.113634);
    var myLatlng = new google.maps.LatLng(lat,lng);
    var myOptions = {
        zoom: 15,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }
 
    var map = new google.maps.Map( document.getElementById(mapHolder), myOptions );

    var contentString = '<div id="mkrcontent">'+
        '<p><strong>' + strTitle + '</strong></p></div>';

    var infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    var marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        title: strTitle,
    });
    
    google.maps.event.addListener(marker, 'click', function() {
        infowindow.open(map,marker);
    });
}

/////////////////////// Set MapDiv Height /////////////////////////
function setMapDivHeight(mapDiv) {
   // Get the width of the map div
   mapDivWidth = document.getElementById(mapDiv).clientWidth;
   
   // Work out ideal height
   mapDivIdealHeight = Math.floor(mapDivWidth/1.6);
   
   // Set ideal height to minimum value if too short
   if (mapDivIdealHeight < 200 ) mapDivIdealHeight = 200;
   
   // Now update height of div
   document.getElementById(mapDiv).style.height = mapDivIdealHeight + 'px';
   document.getElementById(mapDiv).style.width = mapDivWidth + 'px';
   
}

function showMap(mapDiv, data ) {
   setMapDivHeight(mapDiv);
   // test length of data - probably 1
   if ( data.length == 1) {
      var detail = data[0];
      
      // create map
      initialize_map_simple(detail["lat"],detail["long"], mapDiv, detail["title"], '');
   } else {
      // Future proof - multiple markers coming through
      for (var i=0; i < data.length; i++) {
      
      }
   }
}


jQuery(document).ready(function($) {

	$('#text_ajax_button').click( function() {
      console.log('In function. Button val = '+$(this).val());
			$.post(
				cc_ajax.ajaxurl,
				{
					action: 'ccsg_test_ajax',
					param1: cc_ajax.param1,	
					param2: cc_ajax.param2,	
					button_val: $(this).val(),	
					cc_security: cc_ajax.get_int_access
				},
				function ( response ) {
               if (response <= 0 ) {
                  console.log('Response = ' + response);
   					$("#results").html('<p>Query failed. Try again.</p>');
               } else { 
                  //console.log('In response' + response);
   					$("#results").html(response.replace(/\n/ig,"<br>"));
               }
				}						
			);

	});
	
});
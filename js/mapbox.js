mapboxgl.accessToken = 'pk.eyJ1IjoiZnJhbmNpc2JhcnRvbiIsImEiOiJjanJhcWhna2MwNXppNDRudngzZWV6bjBsIn0.BOVhsy0Ts-EYnYgErZ7v6Q';
var map = new mapboxgl.Map(
  { 
    container: 'mapbox', // this is the id of the div on the page
	  // style: 'mapbox://styles/francisbarton/cjrar8asr0esy2so6z4tyb9hl',
	  style: 'mapbox://styles/francisbarton/cjsfeqqmc07cq1fs0o0x9jn5k',
	  // style: 'mapbox://styles/mapbox/streets-v11',
	  // style: 'mapbox.streets',
	  zoom: 11.0,
	  center: [-0.126, 50.835]
  }
);
				
// add navigation controls
var nav = new mapboxgl.NavigationControl();
map.addControl(nav, 'top-left');
// add fullscreen control
map.addControl(new mapboxgl.FullscreenControl({map: document.querySelector('body')}));

/*
// add GeoJSON source https://docs.mapbox.com/mapbox-gl-js/api/#geojsonsource
mapbox.addSource( 'geojson_asset_data', {
	type: 'geojson',
	data: 'geo_results'
});

// add markers to map
geojson_asset_data.features.forEach(function(marker) {

    // create a HTML element for each feature
	var el = document.createElement('div');
	el.className = 'marker';

	// make a marker for each feature and add to the map
	new mapboxgl.Marker(el)
    	.setLngLat(marker.geometry.coordinates)
    	.addTo(map);
});
*/
/*
// L.mapbox.accessToken = 'pk.eyJ1IjoiZnJhbmNpc2JhcnRvbiIsImEiOiJjanJhcWhna2MwNXppNDRudngzZWV6bjBsIn0.BOVhsy0Ts-EYnYgErZ7v6Q';

// var map = L.mapbox.map( 'map2', 'mapbox.streets' ) // map2 is the id of the div on the page
  // .setView( [-0.116, 50.835], 11.0 );
  // .featureLayer.setGeoJSON( 'geojson_asset_data' );
  // var layer = L.mapbox.tileLayer('mapbox.streets');
  // var featureLayer = L.mapbox.featureLayer(geojson_asset_data)
  //	.addTo(map);
  // var featureLayer = L.mapbox.featureLayer()
  //	.addTo(map);
  // featureLayer.loadURL('dbh_assets.geojson');
  
*/
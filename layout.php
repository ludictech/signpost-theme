
<?php

if (isset($_GET['cookiedis']) and ($_GET['cookiedis'] == 1)) {
   $cookieTime = 60*60*24*360;
   setcookie("signpostcookieaccept", 1, time()+$cookieTime,'/');
   
   header('Location: '.get_the_permalink());
   // Make sure that code below does not get executed when we redirect.
   exit;

}



if ((getPageName() == 'add-internet-access') or (getPageName() == 'review-internet-access') or (getPageName() == 'add-course') or (getPageName() == 'review-course')){
   if ( is_user_logged_in() ) {
      // Check if I'm logged in and of a suitable user level
      if ((get_current_user_role() != 'administrator') and (get_current_user_role() != 'editor')) header("Location: /");
   } else {
      header("Location: /");
   }
}


if (user_has_necessary_rights() and ((getPageName() == 'internet-access') or (getPageName() == 'courses'))) {
   get_template_part( 'validation','process-buttons' );
}
if (getPageName() == 'contact') {
   get_template_part( 'validation','contact' );
}
if (getPageName() == 'add-internet-access') {
   get_template_part( 'validation','add-internet-access' );
}
if (getPageName() == 'review-internet-access') {
   get_template_part( 'validation','review-internet-access' );
}
if (getPageName() == 'add-course') {
   get_template_part( 'validation','add-course' );
}
if (getPageName() == 'review-course') {
   get_template_part( 'validation','review-course' );
}
?>
<!doctype html>
<!--[if lt IE 7]> <html class="non-js ie6 oldie noanim" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>    <html class="non-js ie7 oldie noanim" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>    <html class="non-js ie8 oldie noanim" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9]>    <html class="non-js ie9 noanim" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 9]><!--> <html class="non-js" <?php language_attributes(); ?>> <!--<![endif]-->

<?php get_header(); ?>

<?php
$thisPageId = get_the_ID();
setMeta();
?>

<body <?php body_class(); ?> id="body">
<a href="#content" class="access">Skip to Content</a>
<div id="top">
<div id="nav-strip">
<div id="nav-strip-inner">
<?php get_template_part( 'navigation' );?>
</div><!-- End of nav-strip-inner -->
</div><!-- End of nav-strip -->
<?php get_template_part( 'banner' );?>
</div><!-- End of top -->
<div id="middle">
<div id="middle-inner">
<?php if (!is_front_page()) get_template_part( 'breadcrumb' );?>

<div id="content" role="main" tabindex="-1">
<div id="content-inner">
<?php 

if (is_home()) {
   get_template_part( 'content','news' ); 
} elseif(is_single()) {
   get_template_part( 'content','single' ); 
} elseif(is_search()) {
   get_template_part( 'content','search' ); 
} elseif(getPageName() == 'home') {
   get_template_part( 'content','home' ); 
} elseif(getPageName() == 'contact') {
   get_template_part( 'content','contact' ); 
} elseif(getPageName() == 'internet-access') {
   get_template_part( 'content','internet-access' ); 
} elseif(getPageName() == 'add-internet-access') {
   get_template_part( 'content','add-internet-access' ); 
} elseif(getPageName() == 'review-internet-access') {
   get_template_part( 'content','review-internet-access' ); 
} elseif(getPageName() == 'courses') {
   get_template_part( 'content','courses' ); 
} elseif(getPageName() == 'add-course') {
   get_template_part( 'content','add-course' ); 
} elseif(getPageName() == 'review-course') {
   get_template_part( 'content','review-course' ); 
} elseif(getPageName() == 'sitemap') {
   get_template_part( 'content','sitemap' ); 
} elseif(is_404()) {
   get_template_part( 'content','404' ); 
} else {
   get_template_part( 'content' ); // Note: is_front_page() will go in here unless page specifically uses Home Page template
}
?>

</div><!-- End of content-inner -->
</div><!-- End of content -->
<div class="clear"></div>

</div><!-- End of middle-inner -->
</div><!-- End of middle -->

<div id="bottom">
<?php get_footer(); ?>
</div><!-- End of bottom -->

<!-- Status container -->
<div id="status-txt" class="srdr" aria-live="polite"></div>
</body>
</html>
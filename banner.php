<div id="banner-inner" role="banner">
<div id="banner-wrap">
<div id="banner1">
<div id="logo">
<?php 

// Check if image added in customizer
if ( get_theme_mod( 'signpost2015_logo' ) ) { ?>
<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
<img src="<?php echo get_theme_mod( 'signpost2015_logo' ); ?>" id="logo_img" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" />
</a>
<?php } else { ?>
<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php echo esc_attr( get_bloginfo( 'name' ) ); ?></a>
<?php } ?>
</div>
</div><!-- End of banner1 -->

<div id="banner2">
<?php 
// Check if button for associated language is required
if ( get_theme_mod( 'signpost2015_associated_language_toggle' ) ) {
   // Try to retrieve corresponding URL for page on other language site
   $lang_link = esc_url(get_post_meta(get_the_ID(), 'spt_other_language_url', true));
      if (empty($lang_link)) {
      $lang_link = esc_url(get_theme_mod( 'signpost2015_associated_language_button_url' ));
   }
?>
<div id="langlink">
<a lang="<?php echo esc_attr(get_theme_mod( 'signpost2015_associated_language_button_text_lang' )); ?>" href="<?php echo $lang_link; ?>" rel="home" class="button">
<?php echo get_theme_mod( 'signpost2015_associated_language_button_text' ); ?>
</a>
</div>
<?php } //endif; ?>

<div id="searchbox">
<!-- Search used to be here -->
</div>
</div><!-- End of banner3 -->
<div class="clear"></div>
</div><!-- End of banner-wrap -->
</div><!-- End of banner-inner -->
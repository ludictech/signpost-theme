<?php
// email constants
define('SYS_EMAIL','lwp@alliance-scotland.org.uk');
//define('SYS_EMAIL','graham.armfield@gmail.com');
define('CONTACT_EMAIL','lwp@alliance-scotland.org.uk');
define('SENDER_EMAIL','lwp@alliance-scotland.org.uk');
define('SENDER_NAME','Links Worker Programme');

// Various URLs within the site
define('CONTACT_THANKS',get_bloginfo ('url').'/contact-thanks');
define('ADD_INTERNET_CONFIRMATION',get_bloginfo ('url').'/review-internet-access');
define('ADD_COURSE_CONFIRMATION',get_bloginfo ('url').'/review-training-course');

?>

<?php
// Reference the form definition array
global $internetAccessForm;

// Retrieve arrays
$arrErrs = getFormErrors();
$clean = getClean();

// Populate $clean array with initial values
foreach((array)$internetAccessForm['fields'] as $field) {
   $clean[$field['name']] = '';
}
// Also initialise the submitted flag
$clean['submitted'] = false;


// Check form submitted
if(isset($_POST['submit'])) {
   // Retrieve $clean array and $arrErrs array
   
   $clean['submitted'] = true;
   $clean['sq'] = substr(sanitize_text_field($_POST['sq']), 0, 2);
   
   //Check for provider - required
   if (empty($_POST['provider'])) {
      // Add error message to string and add input field to error array
      $arrErrs[] = array('provider',__( 'Please supply the internet access provider', 'signpost' ));
   } else {
      // We got one so store it
      $clean['provider'] = substr(sanitize_text_field($_POST['provider']), 0, getFormFieldLength($internetAccessForm, 'provider'));
   }

   //Check for location
   if (empty($_POST['location'])) {
   } else {
      // We got one so store it
      $clean['location'] = substr(sanitize_text_field($_POST['location']), 0, getFormFieldLength($internetAccessForm, 'location'));
   }

   //Check for address - required
   if (empty($_POST['address'])) {
      // Add error message to string and add input field to error array
      $arrErrs[] = array('address',__( 'Please supply the address', 'signpost' ));
   } else {
      // We got one so store it
      $clean['address'] = substr(sanitize_text_field($_POST['address']), 0, getFormFieldLength($internetAccessForm, 'address'));
   }

   //Check for postcode - required
   if (empty($_POST['postcode'])) {
      // Add error message to string and add input field to error array
      $arrErrs[] = array('postcode',__( 'Please include the postcode', 'signpost' ));
   } else {
      // We got one so store it
      $clean['postcode'] = substr(sanitize_text_field($_POST['postcode']), 0, getFormFieldLength($internetAccessForm, 'postcode'));
      
      // format validation
      if (!check_uk_postcode($clean['postcode'])) {
         $arrErrs[] = array('postcode',__( 'Please enter a postcode in valid format', 'signpost' ));
      }
      
   }
   //Check for contact information
   if (empty($_POST['contact'])) {
   } else {
      // We got one so store it
      $clean['contact'] = substr(sanitize_text_field($_POST['contact']), 0, getFormFieldLength($internetAccessForm, 'contact'));
   }

   // Free wifi
   if (checkSelectInOptions($internetAccessForm, 'freewifi', $_POST['freewifi'])) {
      $clean['freewifi'] =  $_POST['freewifi'];
   } else {
      $arrErrs[] = array('freewifi',__( 'Please specify whether there is free wifi', 'signpost' ));
   }
   
   
   //Check for restrictions
   if (empty($_POST['restrictions'])) {
   } else {
      // We got one so store it
      $clean['restrictions'] = substr(sanitize_text_field($_POST['restrictions']), 0, getFormFieldLength($internetAccessForm, 'restrictions'));
   }
   
   //Check for time limits
   if (empty($_POST['timelimits'])) {
   } else {
      // We got one so store it
      $clean['timelimits'] = substr(sanitize_text_field($_POST['timelimits']), 0, getFormFieldLength($internetAccessForm, 'timelimits'));
   }

   //Check for number of pcs
   // orig if (empty($_POST['numpcs'])) {
   if (isset($_POST['numpcs']) && (strlen($_POST['numpcs']) > 0) ) {
      // We got one so store it
      $clean['numpcs'] = substr(sanitize_text_field($_POST['numpcs']), 0, getFormFieldLength($internetAccessForm, 'numpcs'));
   } else {
   }
   
   //Check for number of tablets
   // orig if (empty($_POST['numtablets'])) {
   if (isset($_POST['numtablets']) && (strlen($_POST['numtablets']) > 0) ) {
      // We got one so store it
      $clean['numtablets'] = substr(sanitize_text_field($_POST['numtablets']), 0, getFormFieldLength($internetAccessForm, 'numtablets'));
   } else {
   }
   
   
   //Check for support
   if (checkSelectInOptions($internetAccessForm, 'support', $_POST['support'])) {
      $clean['support'] =  $_POST['support'];
   } else {
      $arrErrs[] = array('support',__( 'Please specify support level available', 'signpost' ));
   }

   //Check for opening days - at least one required
   if (empty($_POST['openingdays'])) {
      // Add error message to string and add input field to error array
      $arrErrs[] = array('openingdays',__( 'Please select at least one opening day', 'signpost' ));
   } else {
      // We got at least one so check they are in the allowed selection
      $clean['openingdays'] = array();
      foreach($_POST['openingdays'] as $day) {
         if (checkCheckboxInOptions($internetAccessForm, 'openingdays', $day)) {
            $clean['openingdays'][] =  '"'.$day.'"';
         }
      }
   }

      //Check for opening hours - required
   if (empty($_POST['openhours'])) {
      // Add error message to string and add input field to error array
      $arrErrs[] = array('openhours',__( 'Please detail the opening hours', 'signpost' ));
   } else {
      // We got one so store it
      $clean['openhours'] = substr(sanitize_text_field($_POST['openhours']), 0, getFormFieldLength($internetAccessForm, 'openhours'));
   }

   //Check for printing
   if (empty($_POST['printing'])) {
   } else {
      // We got one so store it
      $clean['printing'] = substr(sanitize_text_field($_POST['printing']), 0, getFormFieldLength($internetAccessForm, 'printing'));
   }
   
   //Check for number of url
   if (empty($_POST['url'])) {
   } else {
      // We got one so store it
      $clean['url'] = substr(esc_url($_POST['url']), 0, getFormFieldLength($internetAccessForm, 'url'));
   }
   



   
   // Security question
   //Check for security question
   if (empty($_POST['security'])) {
      // Add error message
      $arrErrs[] = array('security',__('Please answer this security question:', 'signpost' ));
   } else {
      // Security value is set to something
      $clean['security'] = substr(sanitize_text_field($_POST['security']), 0, 2);
   

      // We got one so check it
      if (absint($_POST['security'] != $_POST['security'])) {
         // Not a positive integer
         $arrErrs[] = array('security',__('Please provide a correct answer to the security question using a number', 'signpost' ));
      } else {
         // Check the answer is actually correct
         if (strtolower($_POST['security'] != getSecA($clean['sq']))) {
            $arrErrs[] = array('security',__('Please provide a correct answer to the security question.', 'signpost' ));
         } else {
            $clean['security'] = $_POST['security'];
         }
         
      }
   }
   
   
   // Nonce validation
   if (!empty($formDef['nonce-name'])) {
      if ( !wp_verify_nonce($_POST[$formDef['nonce-name']],$_POST[$formDef['nonce-name']]) ) {
         // Add error message
         $arrErrs[] = array('',__('An unidentified error has occured - please try again.', 'signpost' ));
      }
   }

   // If form validates OK then create draft record and relocate to Confirm page
   if (count($arrErrs) == 0) {

      // Start the processing to add the new Profile post
      $postAdd = array();
      
      $postAdd['post_title'] = htmlspecialchars($clean['provider']);
      if ( is_user_logged_in() ) {$postAdd['post_author'] = get_the_author_meta('ID'); }
      $postAdd['post_content'] = '';
      $postAdd['post_type'] = 'spt_internet_access';
      $postAdd['post_status'] = 'draft';

      $pId = wp_insert_post($postAdd);
      $writeSuccess = false;

      // If successful - go ahead and write the custom fields
      if ($pId > 0) {
         // Internet access record successfully added so add meta fields
         $writeSuccess = true;
         
         // Try to geocode address and postcode
         $full_address = $clean['address'].' '.$clean['postcode'];
         $geocode = implode(',',cc_gmap_geocode($full_address));
         
         update_post_meta($pId, 'spt_int_location' , $clean['location']);
         update_post_meta($pId, 'spt_int_address' , $clean['address']);
         update_post_meta($pId, 'spt_int_postcode', $clean['postcode']);
         update_post_meta($pId, 'spt_int_free_wifi', $clean['freewifi']);
         update_post_meta($pId, 'spt_int_contact_info', $clean['contact']);
         update_post_meta($pId, 'spt_int_latlng', $geocode);
         update_post_meta($pId, 'spt_int_restrictions', $clean['restrictions']);
         update_post_meta($pId, 'spt_int_time_limits', $clean['timelimits']);
         update_post_meta($pId, 'spt_int_num_pcs', $clean['numpcs']);
         update_post_meta($pId, 'spt_int_num_tablets', $clean['numtablets']);
         update_post_meta($pId, 'spt_int_support', $clean['support']);
         update_post_meta($pId, 'spt_int_open_days', '['.implode(',',$clean['openingdays']).']');
         update_post_meta($pId, 'spt_int_open_hours', $clean['openhours']);
         update_post_meta($pId, 'spt_int_printing', $clean['printing']);
         update_post_meta($pId, 'spt_int_url', $clean['url']);
         
         
      }

      if ($writeSuccess) {
         // Relocate to confirmation page
         header('Location: '.get_theme_mod( 'signpost2015_review_internet_access_url' ).'?id='.$pId);
         // Make sure that code below does not get executed when we redirect.
         exit;
      }
      
   }
   // Anything else need doing here?
   
// End of is form submitted
} else {
   // Form not submitted so decide which security question to show
   $clean['sq'] = mt_rand(0,(getSecArrayCount()-1));
   

}

// Ensure arrays are upstraight 
setClean($clean);
setFormErrors($arrErrs);
?>

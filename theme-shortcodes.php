<?php

////////////////////////////////////////////////////////////////////////////////
////////////////////// Other Short Code Functions  /////////////////////////////
////////////////////////////////////////////////////////////////////////////////

function spt_asset_list_sc() {
   
   include ( "js/get_json.php" );

// only show data in the session body if its key matches this list
// might be better eventually to convert into a negative list i.e. don't show data whose key matches,
// once list of keys in the json has settled down
   $show_fields = array(
	   // "open_days",
	   "venue_postcode",
	   "asset_type",
	   // "asset_name_cy",
      // "asset_name",
      "asset_desc",
	   "venue_address_cy",
	   "venue_address",
	   //		"location_area",
	   "session_contact_cy",
	   "session_contact",
	   "info_url_cy",
	   "info_url",
      "provider",
      "venue_info",
   );

   function print_info( $item, $show_fields ) {
	   echo "<ul>\n";
		   foreach ( $item as $key => $value ) {
			   if ( in_array( $key, $show_fields ) ) {
				   if ( is_array( $value ) ) {
					   echo "\t<li class=\"$key\">";
					   echo _e( "$key", 'signpost' ), ": ";
					   foreach ( $value as $day ) {
						   echo _e( "$day", 'signpost' ), " ";
					   }
					   echo "</li>\n";
				   } else {
					   echo "\t<li class=\"", $key, "\">", $value, "</li>\n";
				   }
			   }			
		   }
	   echo "</ul>\n\n";
   };


echo "<div id=\"data\" data-collapse>";
  foreach ( $locations as $location ) {
	echo "<h3 class=\"location\">", $location , "</h3>\n<div data-collapse>";
		foreach ( $results as $item ) {
			if ( is_array( $item ) ) {
				$id    = $item['post_id'];
				$name  = $item['asset_name'];
				$type  = $item['asset_type'];
				$place = $item['area'];
				if ( $place === $location ) {
					echo "<h4 class=\"session\" id=\"session-", $id, "\">\n\t";
						if ( $name ) {
							echo _e( "$name", 'signpost' );
						} else
							echo _e( "$type", 'signpost' );
							echo "\n</h4>\n";
							print_info( $item, $show_fields );
				}
			}
		}
	echo "</div>\n";
} 
echo "</div>";
   
}
add_shortcode("spt-asset-list", "spt_asset_list_sc");

function spt_twitter_feed_sc($twitter_id) {
   include ( "check_domain.php" );
   print_r ( "$twitter_id" );
   
   echo "<div>
            <a class=\"twitter-timeline\" href=\"https://twitter.com/",
               $twitter_id,
            "\" style=\"width: auto; max-height: 400;\">";
   echo _e ( "Tweets by " , "signpost" ), $twitter_id,
      "</a>
         <script async src=\"https://platform.twitter.com/widgets.js\" charset=\"utf-8\">
         </script>
	</div>";
};
add_shortcode("spt-twitter-feed", "spt_twitter_feed_sc");

function spt_site_map_sc($atts, $content = null) {
// Function to return the sitemap menu triggered by a shortcode
   
   $ret = '';
   
   $menuArgs = array( 
      'theme_location' => 'sitemap',
      'menu_id' => 'sitemap',
      'container' => false,
      'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
      'echo' => false,
   );

   $ret = wp_nav_menu($menuArgs);

   return $ret;
}
add_shortcode("spt-site-map", "spt_site_map_sc");   

function PageRule_sc($atts, $content = null) {
   return '<hr class="page-rule" />';
}
add_shortcode("page-rule", "PageRule_sc");   

function linksSitemap_sc($atts, $content = null) {
   // Process parameters
   extract(shortcode_atts(array(
       'menu' => 'sitemap',
       ), $atts));

   $menuArgs = array( 
      'theme_location' => $menu,
      'menu_id' => $menu,
      'echo' => false
   
   );
   
   return wp_nav_menu($menuArgs);
}
add_shortcode("links-sitemap", "linksSitemap_sc");   



// Function to place videos on a page
function getVideo_sc($atts, $content = null) {
/* Function to output a YouTube video */
   // Get parameters
   extract(shortcode_atts(array(
      'width' => '400', 
      'height' => '250', 
      'type' => 'yt',
      'vidid' => '',
   ), $atts));
   
   if (empty($vidid)) return '';
   
   if (($type != 'yt') and ($type != 'vim')) return '';

   
   $strHtml = '<div class="video-block">';
   $strHtml .= '<div class="embed-container">';
   if ($type == 'yt') {
      $strHtml .= '<iframe width="'.$width.'" height="'.$height.'" src="//www.youtube.com/embed/'.$vidid.'?rel=0&cc_lang_pref=en&cc_load_policy=1" frameborder="0" allowfullscreen></iframe>';
   }
   if ($type == 'vim') {
      $strHtml .= '
      <iframe src="//player.vimeo.com/video/'.$vidid.'" width="'.$width.'" height="'.$height.'" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
   }
   $strHtml .= '</div></div>';
   return $strHtml;
}
add_shortcode("getvideo", "getVideo_sc");   



function mapBlock_sc($atts, $content = null) {
   // sidebar testimonials block
   global $pageSidebar;
   // Process parameters
   extract(shortcode_atts(array(
      'latlng' => '',
      'label' => 'Health and Social Care <br>Alliance Scotland <br>(The ALLIANCE)'
   ), $atts));

   // no location info included so exit.
   //if (empty($latlong)) return;
   $arrLatLng = explode( ',',$latlng);
   
   $strHtml = '<div class="map-block rs_preserve">';
   $strHtml .= '<div id="map_canvas"></div>';
   $strHtml .= '<script type="text/javascript">';
   $strHtml .= 'var mapDiv = "map_canvas";';
   
   $strHtml .= 'var locData = new Array();';
   $strHtml .= 'locData["type"] = "location";';
   $strHtml .= 'locData["lat"] = "'.$arrLatLng[0].'";';
   $strHtml .= 'locData["long"] = "'.$arrLatLng[1].'";';
   $strHtml .= 'locData["title"] = "'.$label.'";';
   $strHtml .= 'var mapData = new Array(locData);';
   //$strHtml .= 'showMap(mapDiv, mapData);';
   $strHtml .= '</script>';
   
   $strHtml .= '</div>';
   
   return $strHtml;
}
add_shortcode("map-block", "mapBlock_sc");   




?>
<?php
// Reference the form definition array
global $courseForm;

// Retrieve arrays
$arrErrs = getFormErrors();
$clean = getClean();

// Populate $clean array with initial values
foreach((array)$courseForm['fields'] as $field) {
   $clean[$field['name']] = '';
}
// Also initialise the submitted flag
$clean['submitted'] = false;


// Check form submitted
if(isset($_POST['submit'])) {
   // Retrieve $clean array and $arrErrs array
   
   $clean['submitted'] = true;
   $clean['sq'] = substr(sanitize_text_field($_POST['sq']), 0, 2);
   
   //Check for activity summary - required
   if (empty($_POST['activitysummary'])) {
      // Add error message to string and add input field to error array
      $arrErrs[] = array('activitysummary',__( 'Please supply the activity summary', 'signpost' ));
   } else {
      // We got one so store it
      $clean['activitysummary'] = substr(sanitize_text_field($_POST['activitysummary']), 0, getFormFieldLength($courseForm, 'activitysummary'));
   }

   //Check for activity detail - not required
   if (empty($_POST['activitydetail'])) {
   } else {
      // We got one so store it
      $clean['activitydetail'] = substr(sanitize_text_field($_POST['activitydetail']), 0, getFormFieldLength($courseForm, 'activitydetail'));
   }

   // Check for Skills Level
   if (checkSelectInOptions($courseForm, 'skillslevel', $_POST['skillslevel'])) {
      $clean['skillslevel'] =  $_POST['skillslevel'];
   } else {
      $arrErrs[] = array('skillslevel',__( 'Please specify the skills level', 'signpost' ));
   }

   // Check for Activity Type
   if (checkSelectInOptions($courseForm, 'activitytype', $_POST['activitytype'])) {
      $clean['activitytype'] =  $_POST['activitytype'];
   } else {
      $arrErrs[] = array('activitytype',__( 'Please specify the activity type', 'signpost' ));
   }

   // Check for Theme/Title
   if (checkSelectInOptions($courseForm, 'theme', $_POST['theme'])) {
      $clean['theme'] =  $_POST['theme'];
   } else {
      $arrErrs[] = array('theme',__( 'Please specify the theme/title', 'signpost' ));
   }

   // Check for skills
   if (empty($_POST['skills'])) {
      // Add error message to string and add input field to error array
      $arrErrs[] = array('skills',__( 'Please specify at least one skill', 'signpost' ));
   } else {
      // We got at least one so check they are in the allowed selection
      $clean['skills'] = array();
      foreach($_POST['skills'] as $skill) {
         if (checkCheckboxInOptions($courseForm, 'skills', $skill)) {
            $clean['skills'][] =  '"'.$skill.'"';
         }
      }
   }
   

   //Check for provider - required
   if (empty($_POST['provider'])) {
      // Add error message to string and add input field to error array
      $arrErrs[] = array('provider',__( 'Please supply the course provider', 'signpost' ));
   } else {
      // We got one so store it
      $clean['provider'] = substr(sanitize_text_field($_POST['provider']), 0, getFormFieldLength($courseForm, 'provider'));
   }

   //Check for number of url
   if (empty($_POST['url'])) {
   } else {
      // We got one so store it
      $clean['url'] = substr(esc_url($_POST['url']), 0, getFormFieldLength($courseForm, 'url'));
   }

   //Check for booking details
   if (empty($_POST['booking'])) {
   } else {
      // We got one so store it
      $clean['booking'] = substr(sanitize_text_field($_POST['booking']), 0, getFormFieldLength($courseForm, 'booking'));
   }

   //Check for cost details - note !isset instead of empty as a valid value could be 0
   if (!isset($_POST['cost'])) {
   } else {
      // We got one so store it
      $clean['cost'] = substr(sanitize_text_field($_POST['cost']), 0, getFormFieldLength($courseForm, 'cost'));
   }

   //Check for location
   if (empty($_POST['location'])) {
   } else {
      // We got one so store it
      $clean['location'] = substr(sanitize_text_field($_POST['location']), 0, getFormFieldLength($courseForm, 'location'));
   }
   // Free wifi
   if (checkSelectInOptions($courseForm, 'freewifi', $_POST['freewifi'])) {
      $clean['freewifi'] =  $_POST['freewifi'];
   } else {
      $arrErrs[] = array('freewifi',__( 'Please specify whether there is free wifi', 'signpost' ));
   }
   

   //Check for address - required
   if (empty($_POST['address'])) {
      // Add error message to string and add input field to error array
      $arrErrs[] = array('address',__( 'Please supply the address', 'signpost' ));
   } else {
      // We got one so store it
      $clean['address'] = substr(sanitize_text_field($_POST['address']), 0, getFormFieldLength($courseForm, 'address'));
   }

   //Check for postcode - required
   if (empty($_POST['postcode'])) {
      // Add error message to string and add input field to error array
      $arrErrs[] = array('postcode',__( 'Please include the postcode', 'signpost' ));
   } else {
      // We got one so store it
      $clean['postcode'] = substr(sanitize_text_field($_POST['postcode']), 0, getFormFieldLength($courseForm, 'postcode'));
      
      // format validation
      if (!check_uk_postcode($clean['postcode'])) {
         $arrErrs[] = array('postcode',__( 'Please enter a postcode in valid format', 'signpost' ));
      }
      
   }

   //Check for drop in days - not required
   if (empty($_POST['dropindays'])) {
   } else {
      // We got at least one so check they are in the allowed selection
      $clean['dropindays'] = array();
      foreach($_POST['dropindays'] as $day) {
         if (checkCheckboxInOptions($courseForm, 'dropindays', $day)) {
            $clean['dropindays'][] =  '"'.$day.'"';
         }
      }
   }
   
   //Check for exceptions
   if (empty($_POST['exceptions'])) {
   } else {
      // We got one so store it
      $clean['exceptions'] = substr(sanitize_text_field($_POST['exceptions']), 0, getFormFieldLength($courseForm, 'exceptions'));
   }

   // Start date
   $start_date = false;
   if (empty($_POST['startdate'])) {
   } else {
      //Check it's in valid format
      if (preg_match('/(2[0-9]{3})-(0[1-9]|1[012])-([123]0|[012][1-9]|31)/', $_POST['startdate'])) {
         // We got one so store it
         $clean['startdate'] = substr(sanitize_text_field($_POST['startdate']), 0, getFormFieldLength($courseForm, 'startdate'));
         $start_date = true;
      } else {
         // Incorrect format - Add error message to string and add input field to error array
         $clean['startdate'] = substr(sanitize_text_field($_POST['startdate']), 0, getFormFieldLength($courseForm, 'startdate'));
         $arrErrs[] = array('startdate',__( 'Please enter the start date in the correct format - YYYY-MM-DD', 'signpost' ));
      }
   }

   // End date
   $end_date = false;
   if (empty($_POST['enddate'])) {
   } else {
      //Check it's in valid format
      if (preg_match('/(2[0-9]{3})-(0[1-9]|1[012])-([123]0|[012][1-9]|31)/', $_POST['enddate'])) {
         // We got one so store it
         $clean['enddate'] = substr(sanitize_text_field($_POST['enddate']), 0, getFormFieldLength($courseForm, 'enddate'));
         
         // Compare to today -reject if before today
         $today_str = date('Y-m-d');
         
         if ($clean['enddate'] < $today_str) {
            // End date before today - Add error message to string and add input field to error array
            $arrErrs[] = array('enddate',__( 'The course end date cannot be before today', 'signpost' ));
         
         } else {
            $end_date = true;
         }
      } else {
         // Incorrect format - Add error message to string and add input field to error array
         $arrErrs[] = array('enddate',__( 'Please enter the end date in the correct format - YYYY-MM-DD', 'signpost' ));
         $clean['enddate'] = substr(sanitize_text_field($_POST['enddate']), 0, getFormFieldLength($courseForm, 'enddate'));
      }
   }
   
   // Compare dates if both set
   if ($start_date and $end_date) {
      if ($clean['enddate'] < $clean['startdate']) {
         // Incorrect format - Add error message to string and add input field to error array
         $arrErrs[] = array('enddate',__( 'End date is before start date', 'signpost' ));
      } else {
      
         // Dropin day selected must corresponds to start date
         // Return start date day of week
         $start_date_day = yxmxdToFormat($clean['startdate'], 'N');
         //echo '<p>Start date day = '.$start_date_day.'</p>';
         // Set it to corresponding day
         $clean['dropindays'] = array($start_date_day);
      }
   }


   //Check for start time
   if (empty($_POST['starttime'])) {
   } else {
      // We got one so store it
      $clean['starttime'] = substr(sanitize_text_field($_POST['starttime']), 0, getFormFieldLength($courseForm, 'starttime'));
   }
   
   //Check for end time
   if (empty($_POST['endtime'])) {
   } else {
      // We got one so store it
      $clean['endtime'] = substr(sanitize_text_field($_POST['endtime']), 0, getFormFieldLength($courseForm, 'endtime'));
   }
   
   //Check for duration
   if (empty($_POST['duration'])) {
   } else {
      // We got one so store it
      $clean['duration'] = substr(sanitize_text_field($_POST['duration']), 0, getFormFieldLength($courseForm, 'duration'));
   }
   



   
   // Security question
   //Check for security question
   if (empty($_POST['security'])) {
      // Add error message
      $arrErrs[] = array('security',__('Please answer this security question:', 'signpost' ));
   } else {
      // Security value is set to something
      $clean['security'] = substr(sanitize_text_field($_POST['security']), 0, 2);
   

      // We got one so check it
      if (absint($_POST['security'] != $_POST['security'])) {
         // Not a positive integer
         $arrErrs[] = array('security',__('Please provide a correct answer to the security question using a number', 'signpost' ));
      } else {
         // Check the answer is actually correct
         if (strtolower($_POST['security'] != getSecA($clean['sq']))) {
            $arrErrs[] = array('security',__('Please provide a correct answer to the security question.', 'signpost' ));
         } else {
            $clean['security'] = $_POST['security'];
         }
         
      }
   }
   
   
   // Nonce validation
   if (!empty($formDef['nonce-name'])) {
      if ( !wp_verify_nonce($_POST[$formDef['nonce-name']],$_POST[$formDef['nonce-name']]) ) {
         // Add error message
         $arrErrs[] = array('',__('An unidentified error has occured - please try again.', 'signpost' ));
      }
   }

   // If form validates OK then create draft record and relocate to Confirm page
   if (count($arrErrs) == 0) {

      // Start the processing to add the new Profile post
      $postAdd = array();
      
      $postAdd['post_title'] = htmlspecialchars($clean['activitysummary']);
      if ( is_user_logged_in() ) { $postAdd['post_author'] = get_the_author_meta('ID'); }
      $postAdd['post_content'] = htmlspecialchars($clean['activitydetail']);
      $postAdd['post_type'] = 'spt_course';
      $postAdd['post_status'] = 'draft';

      $pId = wp_insert_post($postAdd);
      $writeSuccess = false;

      // If successful - go ahead and write the custom fields
      if ($pId > 0) {
         // Internet access record successfully added so add meta fields
         $writeSuccess = true;
         
         // Try to geocode address and postcode
         $full_address = $clean['address'].' '.$clean['postcode'];
         $geocode = implode(',',cc_gmap_geocode($full_address));
         
         update_post_meta($pId, 'spt_course_location' , $clean['location']);
         update_post_meta($pId, 'spt_course_free_wifi' , $clean['freewifi']);
         update_post_meta($pId, 'spt_course_address' , $clean['address']);
         update_post_meta($pId, 'spt_course_postcode', $clean['postcode']);
         update_post_meta($pId, 'spt_course_latlng', $geocode);

         update_post_meta($pId, 'spt_course_act_type', $clean['activitytype']);
         update_post_meta($pId, 'spt_course_skills', '['.implode(',',$clean['skills']).']');
         update_post_meta($pId, 'spt_course_provider', $clean['provider']);
         update_post_meta($pId, 'spt_course_booking', $clean['booking']);
         update_post_meta($pId, 'spt_course_url', $clean['url']);
         update_post_meta($pId, 'spt_course_cost', $clean['cost']);
         
         if (empty($clean['dropindays'])) {
            $str_days = '';
         } else {
            $str_days = '['.implode(',',$clean['dropindays']).']';
         }
         update_post_meta($pId, 'spt_course_dropin_days', $str_days);
         update_post_meta($pId, 'spt_course_exceptions', $clean['exceptions']);
         update_post_meta($pId, 'spt_course_start_date', $clean['startdate']);
         update_post_meta($pId, 'spt_course_end_date', $clean['enddate']);
         update_post_meta($pId, 'spt_course_start_time', $clean['starttime']);
         update_post_meta($pId, 'spt_course_end_time', $clean['endtime']);
         update_post_meta($pId, 'spt_course_duration', $clean['duration']);


         // Now add taxonomy based values - force values to integers first
         wp_set_object_terms( $pId, (int)$clean['skillslevel'], 'course-skills', false );
         wp_set_object_terms( $pId, (int)$clean['theme'], 'course-theme', false );
         //$clean['theme']
      }

      if ($writeSuccess) {
         // Relocate to confirmation page
         header('Location: '.get_theme_mod( 'signpost2015_review_courses_url' ).'?id='.$pId);
         // Make sure that code below does not get executed when we redirect.
         exit;
      }
   }
   // Anything else need doing here?
   
// End of is form submitted
} else {
   // Form not submitted so decide which security question to show
   $clean['sq'] = mt_rand(0,(getSecArrayCount()-1));
   

}

// Ensure arrays are upstraight 
setClean($clean);
setFormErrors($arrErrs);
?>

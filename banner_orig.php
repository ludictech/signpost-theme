<div id="banner-inner" role="banner">
$privacy_cookie_url = get_theme_mod( 'signpost2015_privacy_cookies_url' );
<?php
if ( (!empty($privacy_cookie_url)) and (check_cookie_cookie() <> '1') ) {
?>
<div id="banner-cookie"><p><?php 
$cookie_msg = esc_attr( get_bloginfo( 'name' ) ).' '.__( 'uses cookies on this website to improve the website and to improve your experience. For more information go to our', 'signpost' );
$cookie_msg .= ' <a href="'.$privacy_cookie_url.'">'.__( 'Privacy and cookies page', 'signpost' ).'</a>.';
echo $cookie_msg; 
?>
<p id="cookie-accept-nojs"><a class="button" href="<?php echo get_the_permalink().'?cookiedis=1'; ?>" ><?php echo __( 'Dismiss', 'signpost' ); ?></a></p>
</p>
<button id="cookie-accept-js">
<?php 
echo __( 'Dismiss', 'signpost' );
?>
</button>
</hp 
}
?>

<div id="logo">
<?php 

// Check if image added in customizer
if ( get_theme_mod( 'signpost2015_logo' ) ) { ?>
<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
<img src="<?php echo get_theme_mod( 'signpost2015_logo' ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
</a>
<?php } else { ?>
<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php echo esc_attr( get_bloginfo( 'name' ) ); ?></a>
<?php } ?>
</div>
</div><!-- End of banner1 -->

<div id="banner2">
<?php 
// Check if button for associated language is required
if ( get_theme_mod( 'signpost2015_associated_language_toggle' ) ) {
   // Try to retrieve corresponding URL for page on other language site
   $lang_link = esc_url(get_post_meta(get_the_ID(), 'spt_other_language_url', true));
   
   if (empty($lang_link)) {
      $lang_link = esc_url(get_theme_mod( 'signpost2015_associated_language_button_url' ));
   }
?>
<div id="langlink">
<a lang="<?php echo esc_attr(get_theme_mod( 'signpost2015_associated_language_button_text_lang' )); ?>" href="<?php echo $lang_link; ?>" rel="home" class="button">
<?php echo get_theme_mod( 'signpost2015_associated_language_button_text' ); ?>
</a>
</div>
<?php } //endif; ?>

<div id="searchbox">
<!-- Search used to be here -->
</div>
</div><!-- End of banner3 -->
<div class="clear"></div>
</div><!-- End of banner-wrap -->
</div><!-- End of banner-inner -->
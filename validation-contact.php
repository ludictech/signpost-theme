<?php
// Reference the form definition array
global $contactForm;

// Retrieve arrays
$arrErrs = getFormErrors();
$clean = getClean();

// Populate $clean array with initial values
foreach((array)$contactForm['fields'] as $field) {
   $clean[$field['name']] = '';
}
// Also initialise the submitted flag
$clean['submitted'] = false;


// Check form submitted
if(isset($_POST['submit'])) {
   // Retrieve $clean array and $arrErrs array
   
   $clean['submitted'] = true;
   $clean['sq'] = substr(sanitize_text_field($_POST['sq']), 0, 2);
   
   //Check for name
   if (empty($_POST['yourname'])) {
      // Add error message to string and add input field to error array
      $arrErrs[] = array('yourname','Please provide your name');
   } else {
      // We got one so store it
      $clean['yourname'] = substr(sanitize_text_field($_POST['yourname']), 0, getFormFieldLength($contactForm, 'yourname'));
   }

      //Check for email
   if (empty($_POST['email'])) {
      // Add error message to string and add input field to error array
      $arrErrs[] = array('email','Please provide your email address');
   } else {
      //store email received - used for display only!!
      $clean['email'] = $_POST['email'];
      if(!filter_has_var(INPUT_POST, "email")) {
        $arrErrs[] = array('email','Please provide your email address');
      } else  {
         // Test email for correct format
        if (!filter_input(INPUT_POST, "email", FILTER_VALIDATE_EMAIL)) {
          // Failed - Add error message to string and add input field to error array
          $arrErrs[] = array('email','Please provide a valid email address');
        } else {
           // Passed validation so ensure what we have left is safe
           $clean['email'] = sanitize_email($clean['email']);
        }
      }
   } 
   //Check for name
   if (empty($_POST['subject'])) {
      // Add error message to string and add input field to error array
      $arrErrs[] = array('subject','Please provide a subject');
   } else {
      // We got one so store it
      $clean['subject'] = substr(sanitize_text_field($_POST['subject']), 0, getFormFieldLength($contactForm, 'subject'));
   }


   //Check for Message
   if (empty($_POST['message'])) {
      // Add error message to string and add input field to error array
      $arrErrs[] = array('message','Please include your message');
   } else {
      // We got one so store it
      $clean['message'] = substr(sanitize_text_field($_POST['message']), 0, getFormFieldLength($contactForm, 'message'));
   }
   
   // Security question
   //Check for security question
   if (empty($_POST['security'])) {
      // Add error message
      $arrErrs[] = array('security','Please answer the security question');
   } else {
      // Security value is set to something
      $clean['security'] = substr(sanitize_text_field($_POST['security']), 0, 2);
   

      // We got one so check it
      if (absint($_POST['security'] != $_POST['security'])) {
         // Not a positive integer
         $arrErrs[] = array('security','Please provide a correct answer to the security question using a number');
      } else {
         // Check the answer is actually correct
         if (strtolower($_POST['security'] != getSecA($clean['sq']))) {
            $arrErrs[] = array('security','Please provide a correct answer to the security question.');
         } else {
            $clean['security'] = $_POST['security'];
         }
         
      }
   }
   
   
   // Nonce validation
   if (!empty($formDef['nonce-name'])) {
      if ( !wp_verify_nonce($_POST[$formDef['nonce-name']],$_POST[$formDef['nonce-name']]) ) {
         // Add error message
         $arrErrs[] = array('','An unidentified error has occured - please try again.');
      }
   }

   // If form validates OK then generate emails and then relocate to Thanks page
   if (count($arrErrs) == 0) {
      //echo $clean['location'];
      // Generate emails

      ///////////////////////////////////////////////////////////////
      // Create the email to administrators CONTACT_EMAIL
      
      $RecipientEmail = get_theme_mod( 'signpost2015_coord_email' ); 
      $RecipientName = get_theme_mod( 'signpost2015_coord_name' );
      $SenderEmail = get_theme_mod( 'signpost2015_coord_email' ); 
      $SenderName = get_theme_mod( 'signpost2015_coord_name' );
      $subject = get_bloginfo('name').' '. __( 'Contact Request', 'signpost' ); 
      $cc = '';
      $bcc = '';
      $priority = '';
      $type = '';
      $attachments = '';

      $mailText='<p>'.get_bloginfo('name').' '.__( 'Website Contact Enquiry Received from', 'signpost' ).':</p>'."\r\n\r\n";
      $mailText.='<p>'.__( 'Name: ', 'signpost' ).$clean['yourname'].'<br>'."\r\n";
      $mailText.=__( 'Email:', 'signpost' ). '<a href="mailto:'.$clean['email'].'">'.$clean['email'].'</a><br/>'."\r\n";
      $mailText.=__( 'Subject:', 'signpost' ).' '.$clean['subject'].'<br/>'."\r\n";
      
      $mailText.=__( 'Message:', 'signpost' ).' '.$clean['message'].'</p>'."\r\n\r\n";
      
      // Split any longer lines
      $mailText = wordwrap($mailText, 70);
      //echo $mailText;
      
      $sent = WPEmail($RecipientEmail, $RecipientName, $SenderEmail, $SenderName, $cc, $bcc, $subject, $mailText, $attachments, $priority, $type);

      /* Send user a copy */
      $mailText='<p>'.get_bloginfo('name').' '.__( 'Website Contact Enquiry', 'signpost' ).':</p>'."\r\n\r\n";
      $mailText.='<p>'.__( 'Hello ', 'signpost' ).$clean['yourname'].'<br>'."\r\n";
      $mailText.=__( 'Thanks for using our Contact Form. Someone will get back to you shortly.', 'signpost' ).'</p>'."\r\n\r\n";
      
      // Split any longer lines
      $mailText = wordwrap($mailText, 70);
      //echo $mailText;
      
      $sent = WPEmail($clean['email'], $clean['yourname'], $SenderEmail, $SenderName, $cc, $bcc, $subject, $mailText, $attachments, $priority, $type);
      
      if ($sent) {
         // Relocate to thanks page
         header('Location: '.get_theme_mod( 'signpost2015_contact_thanks_url' ));
         // Make sure that code below does not get executed when we redirect.
         exit;
      }
      
   }
   // Anything else need doing here?
   
// End of is form submitted
} else {
   // Form not submitted so decide which security question to show
   $clean['sq'] = mt_rand(0,(getSecArrayCount()-1));
   

}

// Ensure arrays are upstraight 
setClean($clean);
setFormErrors($arrErrs);
?>

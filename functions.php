<?php
include("library-functions.php");
include("constants.php");
include("theme-shortcodes.php");
include("form-functions.php");
include("form-definitions.php");
include("customizer.php");
include("ajax-functions.php");

// 
add_action('after_setup_theme', 'my_theme_setup', 4);
function my_theme_setup(){
   load_theme_textdomain('signpost', get_template_directory() . '/languages');
}

// Global configuration options
$modeDebug=false;

function debugMode() {
   global $modeDebug;
   return $modeDebug; 
}

// function add scripts
function signpost_scripts() {
   wp_enqueue_style( 'fontawesome-solid', '//use.fontawesome.com/releases/v5.7.2/css/solid.css' );
   wp_enqueue_style( 'fontawesome', '//use.fontawesome.com/releases/v5.7.2/css/fontawesome.css' );
   wp_enqueue_style( 'jquery-ui', '//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css' );
   wp_enqueue_style( 'leaflet', '//unpkg.com/leaflet@1.5.1/dist/leaflet.css' );
   wp_style_add_data( 'leaflet', 'integrity', 'sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==' );
   wp_style_add_data( 'leaflet', 'crossorigin', '' );

	wp_enqueue_script( 'jquery-ui-js', '//code.jquery.com/ui/1.12.1/jquery-ui.js' );
	wp_enqueue_script( 'data-js', get_template_directory_uri() . '/js/data.js', array( 'jquery' ) );
   wp_enqueue_script( 'collapse-js', get_template_directory_uri() .'/js/jquery.collapse.js', array( 'jquery' ) );
   wp_enqueue_script( 'leaflet-js', '//unpkg.com/leaflet@1.5.1/dist/leaflet.js' );
   wp_script_add_data( 'leaflet-js', 'integrity', 'sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og==' );
   wp_script_add_data( 'leaflet-js', 'crossorigin', '' );
   wp_enqueue_script( 'fontawesome-js', '//kit.fontawesome.com/67a965e3e5.js' );
   wp_script_add_data( 'fontawesome-js', 'crossorigin', 'anonymous' );
   wp_script_add_data( 'fontawesome-js', 'integrity', 'sha384-xjKE4wVAYeuRfHDK01pHg3wlt0pL9gQ5FMnt2O0v6mPcfp0qwoiL6UwBLRTmEKg9' );
   
   // wp_enqueue_script( 'googlemaps-js', '//maps.googleapis.com/maps/api/js?key=AIzaSyAYfe7X0lZBx0a-bkBhpQ2ZXFIokadJOqI&callback=signpostMap&ver=5.0.3 async defer' );
}
add_action( 'wp_enqueue_scripts', 'signpost_scripts' );


function wp_request_localize_my_json_data() {
   require_once( 'includes/get_json.php' );
	
	wp_register_script( 'data-js', $directory . 'js/data.js' );
  
   // Localize script exposing get_json parsed data to javascript (data.js)
	wp_localize_script( 'data-js', 'localJSON', array(
		'data' => $our_data,
		'details' => $details
	) );
  
   // Enqueues main js file
   wp_enqueue_script( 'data-js' );
}
add_action( 'wp_enqueue_scripts', 'wp_request_localize_my_json_data', 10);

/*
function wp_request_localize_my_geojson_data() {
   include_once( "includes/get_geojson.php" );

  // Localize script exposing $results contents to javascript (data.js)
	wp_localize_script( 'mapbox-js', 'localgeo', array(
		'data' => $our_geodata,
		'details' => $details
	) );
  
  // Enqueues main js file
  wp_enqueue_script( 'mapbox-js' );
}
add_action( 'wp_enqueue_scripts', 'wp_request_localize_my_geojson_data', 10);
*/

// Function to remove empty paragraphs from shortcodes etc
function remove_empty_p($content){
   $content = force_balance_tags($content);
   return preg_replace('#<p>\s*+(<br\s*/*>)?\s*</p>#i', '', $content);
}
add_filter('the_content', 'remove_empty_p', 20, 1);

// Replacing wp_title()
function theme_slug_setup() {
   add_theme_support( 'title-tag' );
}
add_action( 'after_setup_theme', 'theme_slug_setup' );

// Shorten Excerpt length
function custom_excerpt_length( $length ) {
	return 30;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


// Custom image sizes
if ( function_exists( 'add_image_size' ) ) { 
}

// Revisions
add_filter( 'wp_revisions_to_keep', 'revisions_stored', 10, 2 );

function revisions_stored( $num, $post ) {
    
    $num = 5;
    return $num;
}


////////////////////////////// Widget areas /////////////////////////////////
$themeName = 'signpost';

function cc_widgets_init() {
   global $themeName;
   
	//Footer Logos
   register_sidebar( array(
		'name' => __( 'Footer Logos', 'signpost' ),
		'id' => 'footer-logos',
		'description' => __( 'Footer Logos', 'signpost' ),
		'before_widget' => '<div id="%1$s" class="footer-logos-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h2 class="footer-widget-title f1">',
		'after_title' => '</h2>',
	) );
   
}

/** Register sidebars by running widgets_init() on the widgets_init hook. */
add_action( 'widgets_init', 'cc_widgets_init' );

// Add support for menus
if ( function_exists( 'register_nav_menus' ) ) { 
   register_nav_menus(array('primary'=>__('Primary Menu', 'signpost' ),));
   register_nav_menus(array('footer'=>__('Footer Menu', 'signpost' ),));
   register_nav_menus(array('sitemap'=>__('Sitemap Menu', 'signpost' ),));
}


// Remove extra margin from wp-caption
class fixImageMargins{
    public $xs = 0; //change this to change the amount of extra spacing

    public function __construct(){
        add_filter('img_caption_shortcode', array(&$this, 'fixme'), 10, 3);
    }
    public function fixme($x=null, $attr, $content){

        extract(shortcode_atts(array(
                'id'    => '',
                'align'    => 'alignnone',
                'width'    => '',
                'caption' => ''
            ), $attr));

        if ( 1 > (int) $width || empty($caption) ) {
            return $content;
        }

        if ( $id ) $id = 'id="' . $id . '" ';

    return '<div ' . $id . 'class="wp-caption ' . $align . '" style="width: ' . ((int) $width + $this->xs) . 'px">'
    . $content . '<p class="wp-caption-text">' . $caption . '</p></div>';
    }
}
$fixImageMargins = new fixImageMargins();


// Set up page meta
$meta = array();

function getMeta() {
   global $meta;
   return $meta; 
}
function setMeta() {
   global $meta;
   global $pageId;
   $meta = get_post_custom(); 
}


// Comments formatting
function custom_comments( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch( $comment->comment_type ) :
		case 'pingback' :
		case 'trackback' : ?>
		<li <?php comment_class(); ?> id="comment<?php comment_ID(); ?>">
			<div class="back-link"><?php comment_author_link(); ?></div>
	<?php
			break;
		default :
	?>
</li><li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
	<div <?php comment_class(); ?> class="comment" itemscope="itemscope" itemtype="http://schema.org/UserComments" itemprop="comment">
	   <div class="comment-avatar">
      <?php if ( $args['avatar_size'] != 0 ) echo get_avatar( $comment, $args['avatar_size'] ); ?>
      </div>

      <p class="comment-author"><?php comment_author_link(); ?></p><!-- .vcard -->
      <p class="comment-time"><?php comment_date(); ?></p>
      
      <div class="clear"></div>
      <?php comment_text(); ?>
   
	</div>
	<?php // End the default styling of comment
		break;
	endswitch;
}


//////////////////////// Taxonomies  /////////////////////////////
// hook into the init action and call create_book_taxonomies when it fires
add_action( 'init', 'signpost_create_course_taxonomies', 0 );

// create taxonomy for courses
function signpost_create_course_taxonomies() {
	// Add new Course Skills taxonomy
	$labels = array(
		'name'              => _x( 'Skills Level', 'taxonomy general name' ),
		'singular_name'     => _x( 'Skills Level', 'taxonomy singular name' ),
		'search_items'      => __( 'Skills Levels' ),
		'all_items'         => __( 'Skills Levels' ),
		'parent_item'       => __( 'Parent Skills Level' ),
		'parent_item_colon' => __( 'Parent Skills Level:' ),
		'edit_item'         => __( 'Edit Skills Level' ),
		'update_item'       => __( 'Update Skills Level' ),
		'add_new_item'      => __( 'Add New Skills Level' ),
		'new_item_name'     => __( 'New Skills Level' ),
		'menu_name'         => __( 'Skills Level' ),
	);

	$args = array(
		'hierarchical'      => false,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'course-skills' ),
	);
   register_taxonomy( 'course-skills', array('spt_course'), $args );
	
   // Now set up Course Theme
   $labels = array(
		'name'              => _x( 'Theme', 'taxonomy general name' ),
		'singular_name'     => _x( 'Theme', 'taxonomy singular name' ),
		'search_items'      => __( 'Themes' ),
		'all_items'         => __( 'Themes' ),
		'parent_item'       => __( 'Parent Theme' ),
		'parent_item_colon' => __( 'Parent Theme:' ),
		'edit_item'         => __( 'Edit Theme' ),
		'update_item'       => __( 'Update Theme' ),
		'add_new_item'      => __( 'Add New Theme' ),
		'new_item_name'     => __( 'New Theme' ),
		'menu_name'         => __( 'Theme' ),
	);

	$args = array(
		'hierarchical'      => false,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'course-theme' ),
	);
   register_taxonomy( 'course-theme', array('spt_course'), $args );
   
   // Now populate the taxonomy arrays for use later
   // Set up args to ensure that empty ones are included
   $args = array('hide_empty' => 0);
   
   // Do course themes first
   $course_themes = get_terms( 'course-theme', $args );
   $temp_arr = array();
   
   foreach ($course_themes as $theme) {
      $temp_arr[] = array((string)$theme->term_id,$theme->name);
   }
   // Update global array
   set_course_themes($temp_arr);
   
   // Do course skills levels
   $course_skills_levels = get_terms( 'course-skills', $args );
   $temp_arr = array();
   
   foreach ($course_skills_levels as $skill) {
      $temp_arr[] = array((string)$skill->term_id,$skill->name);
   }
   // Update global array
   set_course_skills_levels($temp_arr);
   

}


/**
 * Remove capabilities from editors.
 *
 * Call the function when your plugin/theme is activated.
 */
function signpost_set_capabilities() {

    // Get the role object.
    $editor = get_role( 'editor' );

	// A list of capabilities to remove from editors.
    $caps = array(
        'manage_categories',
    );

    foreach ( $caps as $cap ) {
    
        // Remove the capability.
        $editor->remove_cap( $cap );
    }
}
add_action( 'init', 'signpost_set_capabilities' );

// Store page association
$pageName = '';

function getPageName() {
   global $pageName;
   return $pageName; 
}
function setPageName($value) {
   global $pageName;
   $pageName = $value; 
}


function get_items_from_selection_array($str, $my_array) {
// Function to retrive values from an array from a string containg keys
// Typically this will have been created by a checkbox array or a dropdown
// A comma separated string is returned
   
   // Tidy up string
   $str = str_replace(array('[',']','"'), '', $str);
   // create a temporary array
   $arr = explode(',', $str);
   
   // Initialise output array to receive translated indexes
   $aret = array();
   
   // Check items in temp array match with available options in global array and 
   // pull out translated value
   foreach($arr as $item) {
      $check = ccReturnArr($my_array, $item);
      if (!empty($check)) $aret[] = $check;
   }
   $ret = implode(', ', $aret);
   
   return $ret;
}

function output_map_buttons($latlng, $title, $id) {
// Put out the map buttons for Internet Access points or Training Courses
   $arr_lat_lng = explode(',',$latlng);

   $str_html = '<p class="map-buttons">';
   $str_html .= '<button class="show-map-here" id="map-button-'.$id.'" data-map-id="'.$id.'" data-type="map-here" ';
   $str_html .= 'data-title="'.$title.'" data-lat="'.$arr_lat_lng[0].'" data-lng="'.$arr_lat_lng[1].'" data-string="" >';
   $str_html .= '<span class="srdr">'.$title.': </span>'.__( 'Show map here', 'signpost' ).'</button> ';
   $str_html .= '<button class="hide-map-here initially-hidden" id="hide-map-button-'.$id.'" data-map-id="'.$id.'" data-type="map-hide" ';
   $str_html .= 'data-title="'.$title.'" data-string="" >';
   $str_html .= '<span class="srdr">'.$title.': </span>'.__( 'Hide map', 'signpost' ).'</button> ';
   $str_html .= '<a target="_map" class="button show-map-google" data-type="map-gmap" data-latlng="'.$latlng.'" data-id="'.$id.'" href="https://www.google.com/maps/place/'.$latlng.'">';
   $str_html .= '<span class="srdr">'.$title.': </span>'.__( 'Show on Google Maps', 'signpost' ).'<span class="srdr"> '.__( 'opens in a new window', 'signpost' ).': </span></a>';
   $str_html .= '</p>';
   $str_html .= '<h3 class="srdr initially-hidden" id="header-'.$id.'">'.$title.' on a Google map</h3>';
   $str_html .= '<div id="block-'.$id.'" class="map-block initially-hidden" >';
   $str_html .= '<div class="showmap" id="gmap-'.$id.'"  style="height:250px;"></div></div>';
   
   return $str_html;
}
function output_review_map($latlng, $title, $map_id ) {
// Function to write out javascript necessary to show the map on the review page
   $arr_lat_lng = explode(',',$latlng);
   
   $str_html = '<div id="'.$map_id.'-block" class="map-block" >';
   $str_html .= '<div id="'.$map_id.'" class="map review" style="height:250px;" ';
   $str_html .= 'data-title="'.$title.'" data-lat="'.$arr_lat_lng[0].'" data-lng="'.$arr_lat_lng[1].'" data-string="" >';
   $str_html .= '</div></div>';

   return $str_html;
}

// Out put buttons underneath review of internet access and courses
function output_review_form($form_url, $id) {
   $str_html = '<form method="post" action="'.$form_url.'" class="review">';
   $str_html .= '<input type="hidden" id="itemid" name="itemid" value="'.$id.'">';
   $str_html .= '<input type="submit" id="approve" name="approve" data-id="'.$id.'" value="'.__( 'Approve', 'signpost' ).'"> ';
   $str_html .= '<input type="submit" id="editreq" name="editreq" data-id="'.$id.'" value="'.__( 'Needs more work', 'signpost' ).'">';
   $str_html .= '</form>';

   return $str_html;
}

// Output buttons within lists of internet access and courses - if user is logged in and Admin or Editor
function output_item_action_form($form_url, $id) {
   $str_html = '<form method="post" action="'.$form_url.'" class="review">';
   $str_html .= '<input type="hidden" id="itemid" name="itemid" value="'.$id.'">';
   $str_html .= '<input type="submit" id="approve" name="approve" data-id="'.$id.'" value="'.__( 'Approve', 'signpost' ).'"> ';
   $str_html .= '<input type="submit" id="editreq" name="editreq" data-id="'.$id.'" value="'.__( 'Needs more work', 'signpost' ).'">';
   $str_html .= '</form>';

   return $str_html;
}


function get_internet_access_details($int_access_details, $options, $sort ){
/*******************************************************************************
Function to retrieve the list of internet access points if required. Parameters:
$int_access_details - the array to receive the recordset
$options            - array - describes any filtering required
$sort               - array - any extra sorting required if not already done
********************************************************************************/

   // Set up the array for the query
   $args = array(
      'post_type' => 'spt_internet_access',
      'post_status' => 'publish',
      'posts_per_page' => '-1',
      'orderby' => 'title',
      'order' => 'ASC',
   );

   // Add in any more args that might be required based on options
   
   // If none are abort the function with an empty array.
    if (empty($options['days'])) {
      return array();
    }
   
   // If location is specified, then attenpt to geocode it
   if (!empty($options['location'] )) {
      $location_geo = cc_gmap_geocode( $options['location'] );
   } else {
      $location_geo = '';
   }
   
   if ($location_geo) {
      //echo '<p>location_geo = '.$location_geo[0].'</p>';
   } else {
      //echo '<p>location_geo = false</p>';
   }
   
   
   
   // Run query
   $myquery0 = new WP_Query($args); // Run the query
   if ($myquery0->have_posts()) :
   
      while ($myquery0->have_posts()) : $myquery0->the_post();
         $post_meta = get_post_custom();  // Retrieves all custom fields for post
   
         // Process Days and knock out any records that are not required
         if (days_not_covered($options['days'], $post_meta['spt_int_open_days'][0])) {
            continue;
         }
         
         // If $distance and location are set, then retrieve the distance from location for each of the records found 
         if ($location_geo and $post_meta['spt_int_latlng'][0]) {
            $int_access_geo = explode(',',$post_meta['spt_int_latlng'][0]);
            
            // Calculate the distance apart
            $distance_apart = cc_geo_distance($int_access_geo[0], $int_access_geo[1], $location_geo[0], $location_geo[1], 'M');
            //echo '<p>'.get_the_title().' distance_apart = '.$distance_apart.'</p>';
            
            // If we've been passed in a required max distance and the calculated distance apart exceeds this,
            // don't bother processing this record any more
            if (!empty($options['distance'] ) and ($distance_apart > (int)$options['distance'])) {
               // Move onto next record
               continue;
            }
         } else {
            $distance_apart = '';
         }
         
         // Still here? store details
         $int_access = array( // Create array to store required details
            'id' => get_the_ID(),
            'mod_date' => get_the_modified_date( 'Y-m-d' ),
            'provider' => get_the_title(),
            // various meta fields here
            'location' => $post_meta['spt_int_location'][0],
            'address' => $post_meta['spt_int_address'][0],
            'postcode' => $post_meta['spt_int_postcode'][0],
            'freewifi' => $post_meta['spt_int_free_wifi'][0],
            'contact' => $post_meta['spt_int_contact_info'][0],
            'latlng' => $post_meta['spt_int_latlng'][0],
            'distance' => $distance_apart,
            'restrictions' => $post_meta['spt_int_restrictions'][0],
            'time_limits' => $post_meta['spt_int_time_limits'][0],
            'num_pcs' => $post_meta['spt_int_num_pcs'][0],
            'num_tablets' => $post_meta['spt_int_num_tablets'][0],
            'support' => $post_meta['spt_int_support'][0],
            'open_days' => $post_meta['spt_int_open_days'][0],
            'open_hours' => $post_meta['spt_int_open_hours'][0],
            'printing' => $post_meta['spt_int_printing'][0],
            'url' => $post_meta['spt_int_url'][0],
         );
         $int_access_details[] = $int_access; // Give this posts details to the full result set
      endwhile;
   else :    
   
   endif; 
   wp_reset_query(); // Very important - drops the query and restores where you were
   
   // Check if we got any records back and do sorting as required
   if (count($int_access_details) > 0 ) {
      // If we have a location we can sort for distance based on that
      if (!empty($location_geo)) {
         // we have a geo-coded location so we can sort the array on the distance element of the array
         $sorted = array_orderby($int_access_details, 'distance', SORT_ASC);
         $int_access_details = $sorted;
      }
   
   } 
   
   return $int_access_details;

}


function format_internet_access_array($int_access_details, $options, $omit) {
/*******************************************************************************
Function to format the HTML for displaying the internet access list. Parameters:
$int_access_details - the array to process - probably already sorted
$options            - array - any special processing required 
$omit               - array - any fields to omit from the list
********************************************************************************/

   // Initialise output
   $str_html = '';
   $mod_date = '0000-00-00';

      // Put out message showing how many results were found
   $str_html .= '<p class="num-found">'.sprintf(__( 'Your search found %d locations.', 'signpost' ), count($int_access_details)).'</p>';

   // Now construct the HTML to show the panels
   foreach($int_access_details as $int_access) {
      $str_html .= '<section class="data-item">';
      $str_html .= '<h3>'.$int_access['provider'].'</h3>';
      $str_html .= '<p class="location"><strong>'.__( 'Address:', 'signpost' ).'</strong> ';
      // Put out address if it's available
      $str_html .= $int_access['address'].' '.$int_access['postcode'].'</p>';
      
      // If the distance is set then we should display it - rounded to 1 decimal place
      if (!empty($int_access['distance'])) {
         $str_html .= '<p class="distance"><strong>'.__( 'Distance:', 'signpost' ).'</strong> ';
         $str_html .= round($int_access['distance'],1).' miles</p>';
      }
      
      // Check for latlng value and put out mapping buttons and map div
      if (!empty($int_access['latlng'])) {
         $str_html .= output_map_buttons($int_access['latlng'], $int_access['provider'], $int_access['id']);
      }
      // Put out rest of data
      $str_html .= '<dl class="other-detail">';
      
      // Tidy up days
      $sdays = get_items_from_selection_array($int_access['open_days'], get_days_array());
      
      $str_html .= '<dt class="opening">'.__( 'Opening days and times:', 'signpost' ).'</dt> <dd>'.$sdays.'</br>';
      $str_html .= wpautop($int_access['open_hours']).'</dd>';

      // Put out location notes if available
      if (!empty($int_access['location'])) {  
         $str_html .= '<dt>'.__( 'Location notes:', 'signpost' ).'</dt> <dd>'.make_clickable($int_access['location']).'</dd>'; 
      }

      //Contact details
      if (!empty($int_access['contact'])) { 
         $str_html .= '<dt>'.__( 'Contact information:', 'signpost' ).'</dt> <dd>'.make_clickable($int_access['contact']).'</dd>'; 
      }
   
      //Free wifi 
      $freewifi = get_items_from_selection_array($int_access['freewifi'], get_yes_no_array());
      $str_html .= '<dt>'.__( 'Free wifi:', 'signpost' ).'</dt> <dd>'.$freewifi.'</dd>';
   

      if (!empty($int_access['restrictions'])) { 
         $str_html .= '<dt>'.__( 'Restrictions:', 'signpost' ).'</dt> <dd>'.wpautop(make_clickable($int_access['restrictions'])).'</dd>'; 
      }
      
      if (!empty($int_access['time_limits'])) { 
         $str_html .= '<dt>'.__( 'Time limits:', 'signpost' ).'</dt> <dd>'.wpautop(make_clickable($int_access['time_limits'])).'</dd>'; 
      }
      
      if (isset($int_access['num_pcs']) && (strlen($int_access['num_pcs']) > 0) )  { 
         $str_html .= '<dt>'.__( 'Number of internet PCs:', 'signpost' ).'</dt> <dd>'.$int_access['num_pcs'].'</dd>'; 
      }
      
      if (isset($int_access['num_tablets']) && (strlen($int_access['num_tablets']) > 0) )  { 
         $str_html .= '<dt>'.__( 'Number of internet tablets:', 'signpost' ).'</dt> <dd>'.$int_access['num_tablets'].'</dd>'; 
      }
      /*
      if (!empty($int_access['support'])) { 
         $str_html .= '<dt>'.__( 'Support Available:', 'signpost' ).'</dt> <dd>'.wpautop(make_clickable($int_access['support'])).'</dd>'; 
      }
      */
      $support = get_items_from_selection_array($int_access['support'], get_support_array());
      $str_html .= '<dt>'.__( 'Support Available:', 'signpost' ).'</dt> <dd>'.make_clickable($support).'</dd>';
      
      if (!empty($int_access['printing'])) { 
         $str_html .= '<dt>'.__( 'Printing:', 'signpost' ).'</dt> <dd>'.wpautop(make_clickable($int_access['printing'])).'</dd>'; 
      }
      
      if (!empty($int_access['url'])) { 
         $str_html .= '<dt>'.__( 'Internet link:', 'signpost' ).'</dt> <dd><a href="'.$int_access['url'].'">'.$int_access['provider'].'</a></dd>'; 
      }
      
      $str_html .= '</dl><div class="clear"></div>';
      
      // Put out buttons to allow for Delete, Amend and Hide
      if (user_has_necessary_rights()) {
         $str_html .= format_process_item_buttons($int_access['id'],$int_access['provider'] );
      }
      
      $str_html .= '</section>';

      if ($int_access['mod_date'] > $mod_date ) $mod_date = $int_access['mod_date'];
      
   }
   $str_html .= '<p class="mod-date">'.__( 'Date modified:', 'signpost' ).' '.yxmxdToFormat($mod_date).'</p>';

   return $str_html;
} // End of function

function get_course_details($course_details, $options, $sort ){
/*******************************************************************************
Function to retrieve the list of courses if required. Parameters:
$course_details - the array to receive the recordset
$options            - array - describes any filtering required
$sort               - array - any exra sorting required if not already done
********************************************************************************/
   // If none are abort the function with an empty array.
  // 2018c1 Need Cost info in here??
    if (empty($options['days']) or empty($options['skills_levels']) or empty($options['activity_types']) or empty($options['themes']) or empty($options['skills']) or empty($options['costs'])) {
      return array();
    }


   // Set up the array for the query

   $args = array(
      'post_type' => 'spt_course',
      'post_status' => 'publish',
      'posts_per_page' => '-1',
      'orderby' => 'title',
      'order' => 'ASC',
      /* Activity type can be filtered here  */
      'meta_query' => array(
         array(
   			'key'     => 'spt_course_act_type',
   			'value'   => explode(',',$options['activity_types']),
   			'compare' => 'IN',
   		   ),
       ), 
      // taxonomies can be filtered here  
      'tax_query' => array(
         'relation' => 'AND',
         array(
   			'taxonomy' => 'course-skills',
   			'field'    => 'term_id',
   			'terms'    => explode(',',$options['skills_levels']),
   		),
         array(
   			'taxonomy' => 'course-theme',
   			'field'    => 'term_id',
   			'terms'    => explode(',',$options['themes']),
   		),
      ),
    );
   
   /* Can't do skills or costs this way because of the way they stored in the database.
      Need to knock them out when reading record set */
   
   // If location is specified, then attenpt to geocode it
   if (!empty($options['location'] )) {
      $location_geo = cc_gmap_geocode( $options['location'] );
   } else {
      $location_geo = '';
   }
   /*
   if ($location_geo) {
      echo '<p>location_geo = '.$location_geo[0].','.$location_geo[1].'</p>';
   } else {
      echo '<p>location_geo = false</p>';
   }
   */
   // Run query
   $myquery0 = new WP_Query($args); // Run the query
   if ($myquery0->have_posts()) :
   
      while ($myquery0->have_posts()) : $myquery0->the_post();
         $post_meta = get_post_custom();  // Retrieves all custom fields for post
         
         // Check that the skills for this record match the selected one(s)
         //$options['skills']
         if (!compare_req_val_with_db_val($options['skills'], $post_meta['spt_course_skills'][0]) ) {
            continue;
         }

         // Process Days and knock out any records that are not required
         if (days_not_covered($options['days'], $post_meta['spt_course_dropin_days'][0])) {
            continue;
         }
   
         // Process Costs and knock out any records that are not required
         // Check that a record exists first
         if (!isset($post_meta['spt_course_cost'][0])) { // No record stored - treated as paid for
          if ($options['costs'] == 'f') {continue;}
         } else {
         // We have got  a record
         // If I want paid for courses, discard record if it's free
          if (($options['costs'] == 'p') and ($post_meta['spt_course_cost'][0] == '0' )) {continue;}
         // If I want free courses, discard record if it's not free, or no record
          if (($options['costs'] == 'f') and ($post_meta['spt_course_cost'][0] != '0' )) {continue;}
         // If I want both free and paid for then let the record through

         }
   
         // If $distance and location are set, then retrieve the distance from location for each of the records found 
         if ($location_geo and $post_meta['spt_course_latlng'][0]) {
            $course_geo = explode(',',$post_meta['spt_course_latlng'][0]);
            
            // Calculate the distance apart
            $distance_apart = cc_geo_distance($course_geo[0], $course_geo[1], $location_geo[0], $location_geo[1], 'M');
            //echo '<p>'.get_the_title().' distance_apart = '.$distance_apart.'</p>';
            
            // If we've been passed in a required max distance and the calculated distance apart exceeds this,
            // don't bother processing this record any more
            if (!empty($options['distance'] ) and ($distance_apart > (int)$options['distance'])) {
               // Move onto next record
               continue;
            }
         } else {
            $distance_apart = '';
         }

         // Retrieve taxonomy data
         $arr_skills_level = wp_get_object_terms(get_the_ID(), 'course-skills', array('fields' => 'names'));
         $arr_theme_title = wp_get_object_terms(get_the_ID(), 'course-theme', array('fields' => 'names'));
      
        // See if cost is set
        if (!isset($post_meta['spt_course_cost'][0])) {
          $course_cost = '';
        } else {
          $course_cost = $post_meta['spt_course_cost'][0];
        }
        //echo '<p>'.get_the_title().' costt = '.$course_cost.'</p>';

        // Process cost and knock out any records that are not required
         
         // Store details
         $course = array( // Create array to store required details
            'id' => get_the_ID(),
            'mod_date' => get_the_modified_date( 'Y-m-d' ),
            'activity_summary' => get_the_title(),
            'activity_detail' => get_the_content(),
            // get the taxonomy values
            
            'skills_level' => implode(',',$arr_skills_level ),
            'theme_title' => implode(',',$arr_theme_title ),
            // various meta fields here
            'activity_type' => $post_meta['spt_course_act_type'][0],
            'skills' => $post_meta['spt_course_skills'][0],
            'booking' => $post_meta['spt_course_booking'][0],
            'url' => $post_meta['spt_course_url'][0],
            'location' => $post_meta['spt_course_location'][0],
            'free_wifi' => $post_meta['spt_course_free_wifi'][0],
            'address' => $post_meta['spt_course_address'][0],
            'postcode' => $post_meta['spt_course_postcode'][0],
            'latlng' => $post_meta['spt_course_latlng'][0],
            'distance' => $distance_apart,
            'provider' => $post_meta['spt_course_provider'][0],
            'dropin_days' => $post_meta['spt_course_dropin_days'][0],
            'exceptions' => $post_meta['spt_course_exceptions'][0],
            'start_date' => $post_meta['spt_course_start_date'][0],
            'end_date' => $post_meta['spt_course_end_date'][0],
            'start_time' => $post_meta['spt_course_start_time'][0],
            'end_time' => $post_meta['spt_course_end_time'][0],
            'duration' => $post_meta['spt_course_duration'][0],
            'cost' => $course_cost, // 2018c1 Need Cost info in here??
              
         );
         $course_details[] = $course; // Give this posts details to the full result set
      endwhile;
   else :    
   
   endif; 
   wp_reset_query(); // Very important - drops the query and restores where you were
   
   // Check if we got any records back and do sorting as required
   if (count($course_details) > 0 ) {
      if (!empty($location_geo)) {
         // we have a geo-coded location so we can sort the array on the distance element of the array
         $sorted = array_orderby($course_details, 'activity_type',SORT_ASC, 'distance', SORT_ASC);
         $course_details = $sorted;
      } else {
         // No location so just sort based on activity type
         $sorted = array_orderby($course_details, 'activity_type',SORT_ASC,'activity_summary',SORT_ASC);
         $course_details = $sorted;
         
      }
   
   } 
   return $course_details;

}


function format_course_array($course_details, $options, $omit) {
/*******************************************************************************
Function to format the HTML for displaying the course list. Parameters:
$course_details     - the array to process - probably already sorted
$options            - array - any special processing required - eg separate out results for each day
$omit               - array - any fields to omit from the list
********************************************************************************/

   // Initialise output
   $str_html = '';
   $mod_date = '0000-00-00';
   
   $str_html .= '<p class="num-found">'.sprintf(__( 'Your search found %d courses.', 'signpost' ), count($course_details)).'</p>';

   // Store Activity type as we're going to split those up
   $curr_act_type = '';
   // Now construct the HTML to show the panels
   foreach($course_details as $course) {
      // See if we need to put out a new activity type heading
      if ($course['activity_type'] != $curr_act_type) {
         $str_html .= '<h3>'.ccReturnArr(get_course_act_type(),$course['activity_type']).'</h3>';
         $curr_act_type = $course['activity_type'];
      }

      $str_html .= '<section class="data-item">';
      $str_html .= '<h4>'.$course['activity_summary'].'</h4>';
      if (!empty($course['activity_detail'])) { 
         $str_html .= make_clickable(wpautop(make_clickable($course['activity_detail'])));
      }
      if (!empty($course['provider'])) { 
         $str_html .= '<p class="provider"><strong>'.__( 'Provider:', 'signpost' ).'</strong> '.make_clickable($course['provider']).'</p>'; 
      }
      $str_html .= '<p class="location"><strong>'.__( 'Location:', 'signpost' ).'</strong> ';
      // Put out location if it's available
      $str_html .= $course['address'].' '.$course['postcode'].'</p>';

      if (!empty($course['location'])) { 
         $str_html .= '<p class="location"><strong>'.__( 'Location notes:', 'signpost' ).'</strong> '.formatEmailsInText($course['location']).'</p>'; 
      }

      $str_html .= '<p class="location"><strong>'.__( 'Free wifi:', 'signpost' ).'</strong> '.get_items_from_selection_array($course['free_wifi'], get_yes_no_array()).'</p>'; 

      // If the distance is set then we should display it - rounded to 1 decimal place
      if (!empty($course['distance'])) {
         $str_html .= '<p class="distance"><strong>'.__( 'Distance:', 'signpost' ).'</strong> ';
         $str_html .= round($course['distance'],1).' miles</p>';
      }
      
      // Check for latlng value and put out mapping buttons and map div
      if (!empty($course['latlng'])) {
         $str_html .= output_map_buttons($course['latlng'], $course['activity_summary'], $course['id']);
      }
      // Put out rest of data
      $str_html .= '<dl class="other-detail">';
      
      // 2018c1 Need Cost info in here??


      // Tidy up days
      $sdays = get_items_from_selection_array($course['dropin_days'], get_days_array());
   
      // Tidy up cost - interpret values
      if ($course['cost'] == '') {
        $str_cost = 'Unspecified';
      } elseif ($course['cost'] == '0') {
        $str_cost = 'Free';
      } else {
        $str_cost = esc_html($course['cost']);
      }

      $str_html .= '<dt>'.__( 'Cost:', 'signpost' ).'</dt> <dd>'.$str_cost.'</dd>'; 

      $str_html .= '<dt>'.__( 'Skills level:', 'signpost' ).'</dt> <dd>'.$course['skills_level'].'</dd>'; 
      $str_html .= '<dt>'.__( 'Theme/title:', 'signpost' ).'</dt> <dd>'.$course['theme_title'].'</dd>'; 
   
      // Get Skills values
      $sskills = get_items_from_selection_array($course['skills'], get_course_skills());
   
      $str_html .= '<dt>'.__( 'Skills:', 'signpost' ).'</dt> <dd>'.$sskills.'</dd>'; 
      if (!empty($course['booking'])) { 
         $str_html .= '<dt>'.__( 'Booking:', 'signpost' ).'</dt> <dd>'.make_clickable($course['booking']).'</dd>'; 
      }
      if (!empty($course['url'])) { 
         $str_html .= '<dt>'.__( 'Internet link:', 'signpost' ).'</dt> <dd><a href="'.$course['url'].'">'.$course['activity_summary'].'</a></dd>'; 
      }
      if ($course['activity_type'] == 'di') {
         $str_html .= '<dt class="opening">'.__( 'Drop-in days:', 'signpost' ).'</dt> <dd>'.$sdays.'</dd>';
      } else {
         $str_html .= '<dt class="opening">'.__( 'Days:', 'signpost' ).'</dt> <dd>'.$sdays.'</dd>';
      }
   
   
      if (!empty($course['exceptions'])) { 
         $str_html .= '<dt>'.__( 'Exceptions:', 'signpost' ).'</dt> <dd>'.make_clickable($course['exceptions']).'</dd>'; 
      }
      if (!empty($course['start_date']) and !empty($course['end_date']) and ($course['activity_type'] == 'bc') ) {
         $str_html .= '<dt>'.__( 'Start date:', 'signpost' ).'</dt> <dd>'.yxmxdToFormat($course['start_date']).'</dd>'; 
         $str_html .= '<dt>'.__( 'End date:', 'signpost' ).'</dt> <dd>'.yxmxdToFormat($course['end_date']).'</dd>'; 
      } else {
         if (!empty($course['start_date'])) { 
            $str_html .= '<dt>'.__( 'Available from:', 'signpost' ).'</dt> <dd>'.yxmxdToFormat($course['start_date']).'</dd>';          
         }
         if (!empty($course['end_date'])) { 
            $str_html .= '<dt>'.__( 'Available until:', 'signpost' ).'</dt> <dd>'.yxmxdToFormat($course['end_date']).'</dd>'; 
         }
      }
      if (!empty($course['start_time'])) { 
         $str_html .= '<dt>'.__( 'Start time:', 'signpost' ).'</dt> <dd>'.$course['start_time'].'</dd>'; 
      }
      if (!empty($course['end_time'])) { 
         $str_html .= '<dt>'.__( 'End time:', 'signpost' ).'</dt> <dd>'.$course['end_time'].'</dd>'; 
      }
      if (!empty($course['duration'])) { 
         $str_html .= '<dt>'.__( 'Duration:', 'signpost' ).'</dt> <dd>'.$course['duration'].'</dd>'; 
      }
   
      $str_html .= '</dl>';
      $str_html .= '<div class="clear"></div>';
      
      // Put out buttons to allow for Delete, Amend and Hide
      if (user_has_necessary_rights()) {
         $str_html .= format_process_item_buttons($course['id'],$course['activity_summary'] );
      }
      $str_html .= '</section>';
      
      if ($course['mod_date'] > $mod_date ) $mod_date = $course['mod_date'];
   }
   
   $str_html .= '<p class="mod-date">'.__( 'Date modified:', 'signpost' ).' '.yxmxdToFormat($mod_date).'</p>';
   
   return $str_html;

}

function format_process_item_buttons($item_id, $title) {
   $str_html = '<form id="process-'.$item_id.'" class="process" method="post" action="">';
   $str_html .= '<input type="hidden" name="itemid" id="itemid-'.$item_id.'" value="'.$item_id.'">';
   $str_html .= '<input type="submit" name="delete" id="delete-'.$item_id.'" value="'.__( 'Delete', 'signpost' ).' '.$title.'"> ';
   $str_html .= '<input type="submit" name="amend" id="amend-'.$item_id.'" value="'.__( 'Amend', 'signpost' ).' '.$title.'"> ';
   $str_html .= '<input type="submit" name="hide" id="hide-'.$item_id.'" value="'.__( 'Hide', 'signpost' ).' '.$title.'">';
   $str_html .= '</form>';
   
   return $str_html;
}

function user_has_necessary_rights() {
   $ret = false;
   
   if ( is_user_logged_in() ) {
      // Check if I'm logged in and of a suitable user level
      if ((get_current_user_role() == 'administrator') or (get_current_user_role() == 'editor')) $ret = true;
   } 
   
   return $ret;
}

function compare_req_val_with_db_val($req_vals, $db_vals) {
/********************************************************************************************
   Function to check to see whether the item found in the database has the necessary value(s)
   as reflected in the request from the user.
   Function receives two comma delimited strings containing numbers for the days
      $req_vals - the values requested by the user - format 'n,n,n'
      $db_vals - the values in db for the record found - format ["n","n","n"]
      
   M.O. Convert $req_vals into an array and then check each element to see if the character fetaures
   in the $db_vals string
   
   Return true or false depending on whether item is available on requested days
**********************************************************************************************/
   // initialise return
   $ret = false;
   
   // Convert $req_days into an array
   $req_vals_arr = explode(',',$req_vals);
   
   // Check each character in turn
   foreach ($req_vals_arr as $val) {

      if (strpos($db_vals, $val) !== false) {
         // Got a match
         $ret = true;
         break;
      }
   }
   
   return $ret;
}


function days_not_covered($req_days, $item_days) {
/********************************************************************************************
   Function to check to see whether the item found in the database has the necessary days
   as reflected in the request from the user.
   Function receives two comma delimited strings containing numbers for the days
      $req_days - the days requested by the user - format n,n,n
      $item_days - the days attached to the record found - format ["n","n","n"]
      
   M.O. Convert $req_days into an array and then check each element to see if the character fetaures
   in the $item_days string
   
   Return true or false depending on whether item is available on requested days
**********************************************************************************************/
   // initialise return
   $ret = true;
   
   // Convert $req_days into an array
   $req_days_arr = explode(',',$req_days);
   
   // Check each character in turn
   foreach ($req_days_arr as $day) {

      if (strpos($item_days, $day) !== false) {
         // Got a match
         $ret = false;
         break;
      }
   }
   
   return $ret;
}

function chk_dist_sel($str, $check) {
// Used for checking distance values in display pages
   if ($str == $check) {
      return ' selected="selected"';
   } else {
      return '';
   }
}

function chk_val_chk($str, $check) {
// Used for checking values from checkbox groups
// check will be a comma delimited list of items
   $pos = strpos($check, $str);
   
   if ($pos === false) {
      return '';
   } else {
      return ' checked="checked"';
   }
}
function chk_val_chk_arr($str, $arr) {
// Used for checking values from checkbox groups to decide if checkbox is checked
// $arr is the array to check
   $pos = strpos($check, $str);
   
   if ($pos === false) {
      return '';
   } else {
      return ' checked="checked"';
   }
}

function chk_valid_dist($str) {
// Used for checking distance values supplied are correct
   $ret = false;
   
   switch ($str) {
      case '' :
      case '1' :
      case '2' :
      case '5' :
      case '10' :
      case '25' :
      case '50' :
      case '0' :
         $ret = true;
         break;
      default:
         $ret = false;
   }
   
   return $ret;
}

function get_string_from_get($get_val) {
   // Try to retrieve value from GET array
   if (empty($_GET[$get_val])) {
      $ret = '';
   } else {
      $ret = sanitize_text_field( $_GET[$get_val]);
   }
   return $ret;
}


function get_option_from_get($get_val, $array) {
   // Try to retrieve value from GET array, but also check it's a permitted value
   if (isset($_GET[$get_val]) and ccCheckArr($array, $_GET[$get_val])) {
      $ret = $_GET[$get_val];
   } else {
      $ret = '';
   }
   return $ret;
}

function get_options_from_checkbox_group($get_val, $array) {
   // Try to retrieve checkbox group values from GET array, but also check it's a permitted value
   
   // Array to receive item if allowed
   $items = array();
   
   // Roll through items and check they are valid options
   if(!empty($_GET[$get_val])) {
      foreach($_GET[$get_val] as $item) {
         if (ccCheckArr($array, $item)) {
               $items[] = $item; 
         }
      }
   }
   
   // Check if there are any valid values and combine into string
   if (count($items) > 0) {
      $ret = implode(',',$items);
   } else {
      $ret = '';
   }
   
   return $ret;
}

function process_check_box_get_items($get_val, $arr, $submit) {
   // function to process any incoming get values that concern checkbox groups
   // It receives the GET value to check, and the array of possible values
   // Function returns a string which will either be empty if all have been deselected,
   // or it will return a comma delimeted list of the ones selected
   
   // Initialise return
   $ret = '';

   if (isset($_GET[$get_val])) {
      // some values are set so find out which ones
      $ret = get_options_from_checkbox_group($get_val,$arr);
   } else {
      if (isset($_GET[$submit])) {
         // if display page submit is set and no days, then user has unchecked all of them
         $ret = '';
      } else {
         // This is an initial page load - so all selected by default
         // Pull out all possible values
         $ret = possible_array_values_as_string($arr);
      }
   }

   return $ret;
}


function format_search_statement($type, $options, $sort) {
/***********************************************************************************
   Function to format search statement at top of results div
   Receives three paramaters:
      $type - internet access or course
      $options - array containing options for search
      $sort - any specific extra sorting required
*********************************************************************************/
   $str = '';
   
   if ($type == 'i') {
      // Internet access locations
      $str .= __( 'You searched for internet access locations', 'signpost' );
      $str .= format_location_distance($options);
      
      //$str .= __( ', that are available on ', 'signpost' ) .format_days_list($options);
      $str .= __( ', that are available on ', 'signpost' ) .format_item_list($options['days'], get_days_array());
      
      $str .= '. ';
      
      $str .= format_sort_text($options, $sort);
      
   } else {
      // Courses
      $str .= __( 'You searched for courses/sessions', 'signpost' );
      $str .= format_location_distance($options);
      $str .= __( ', with the following selection options:', 'signpost' ).'<br>';
      // $str .= '<strong>'.__( 'Course/session cost', 'signpost' ) .' - </strong>'.format_item_list($options['costs'], get_course_costs()).'<br>';
      $str .= '<strong>'.__( 'Course/session activity types', 'signpost' ) .' - </strong>'.format_item_list($options['activity_types'], get_course_act_type()).'<br>';
      $str .= '<strong>'.__( 'Course/session skills levels', 'signpost' ) .' - </strong>'.format_item_list($options['skills_levels'], get_course_skills_levels()).'<br>';
      $str .= '<strong>'.__( 'Course/session themes', 'signpost' ) .' - </strong>'.format_item_list($options['themes'], get_course_themes()).'<br>';
      $str .= '<strong>'.__( 'Course/session skills', 'signpost' ) .' - </strong>'.format_item_list($options['skills'], get_course_skills()).'<br>';
      $str .= '<strong>'.__( 'Course/session costs', 'signpost' ) .' - </strong>'.format_item_list($options['costs'], get_course_costs()).'<br>';
      $str .= '<strong>'.__( 'Days', 'signpost' ) .' - </strong>'.format_item_list($options['days'], get_days_array()).'<br><br>';
      
      // 2018c1 Need Cost info in here??

      $str .= format_sort_text($options, $sort);
   }
   
   return '<p class="search-statement">'.$str.'</p>';
}

function format_location_distance($options) {
   $str = '';
   if (!empty($options['location'])) {
      if (!empty($options['distance'])) {
         $str .= sprintf(__( ', within %1$s miles of %2$s', 'signpost' ), $options['distance'],$options['location']);
      } else {
         $str .= sprintf(__( ', based on %1$s', 'signpost' ),$options['location']);
      }
   } 
   return $str;
}

function format_days_list($options) {
   // Convert comma separated string into array
   $arr_days = explode(',',$options['days']);
   
   // Process each item in array to swap in corresponding literal
   for ($i = 0; $i < count($arr_days); $i++) {
     $arr_days[$i] = ccReturnArr(get_days_array(), $arr_days[$i]);
   }
   
   // Put back into a string
   $str_days = implode(', ', $arr_days);
   
   return  $str_days;
}

function format_item_list($list, $array) {
   // Convert comma separated string into array
   $arr_items = explode(',',$list);
   
   // Process each item in array to swap in corresponding literal
   for ($i = 0; $i < count($arr_items); $i++) {
     $arr_items[$i] = ccReturnArr($array, $arr_items[$i]);
   }
   
   // Put back into a string
   $str_items = implode(', ', $arr_items);
   
   return  $str_items;
}



function format_sort_text($options, $sort) {
   $str = '';
   
   // If we have a location then we are sorting by distance
   if (!empty($options['location'])) {
      $str .= __( 'The results are sorted by distance', 'signpost' );
      
   }
      // look for any other sort options in the sort array
   if (strlen($str) > 0) $str .= '.';
   
   return $str;
}

function delete_old_courses() {
/* This function performs two actions. 
   1) It moves all courses where the end date has passed to draft
   2) It deletes all courses where the end date is older than 3 months before today. 
*************************************************************************************/
   global $wpdb;
   
   $today_date = date("Y-m-d");
   
   // Now check to see if the function has already been run today
   // Retrieve stored timestamp option - format expected - yyyymmdd
   $stored_time_stamp = get_option('sg_del_courses_timestamp');
   //$stored_time_stamp = false;
   if (!$stored_time_stamp) {
      // No timestamp set - so set one
      update_option('sg_del_courses_timestamp', $today_date);
   } else {
      // A timestamp was retrieved - so check if different from today
      if ($stored_time_stamp == $today_date) {
         // Timestamp = today so leave the function
         return false;
      }
   }

   // Still here? Then go ahead and check for old courses
   
   $ret = true;
   
   // First check for any published courses with an end date before today
   $dateComp = date("Ymd", strtotime('-1 day'));
   // Set up args for query
   $qArgs = array(
      'post_type' => 'spt_course',
      'post_status' => 'publish',
      'posts_per_page' => '-1',
      'meta_query' => array(
      'relation' => 'AND',
         array(
            'key' => 'spt_course_end_date',
            'value' => '0000-00-00',
            'compare' => '>',
         ),
         array(
            'key' => 'spt_course_end_date',
            'value' => $today_date,
            'compare' => '<',
         ),
       ),
   );
   
   // Now run the first query
   $myquery0 = new WP_Query($qArgs);
   
   while ($myquery0->have_posts()) : $myquery0->the_post();
      $post_ref = array();
      $post_ref['ID'] = get_the_ID();
      $post_ref['post_status'] = 'draft';

      $pId = wp_update_post($post_ref);
      
   endwhile;
   wp_reset_query();
   
   
   // Now check for draft courses with end date greater than 3 months ago
   $date_three_months_ago = date("Y-m-d", strtotime('-3 months'));
   
   $qArgs = array(
      'post_type' => 'spt_course',
      'post_status' => 'draft',
      'posts_per_page' => '-1',
      'meta_query' => array(
      'relation' => 'AND',
         array(
            'key' => 'spt_course_end_date',
            'value' => '0000-00-00',
            'compare' => '>',
         ),
         array(
            'key' => 'spt_course_end_date',
            'value' => $date_three_months_ago,
            'compare' => '<',
         ),
       ),
   );

   $myquery0 = new WP_Query($qArgs);
   
   while ($myquery0->have_posts()) : $myquery0->the_post();
      wp_delete_post(get_the_ID(), true);
      
   endwhile;
   wp_reset_query();
   

   
   return $ret;
}
add_action( 'init', 'delete_old_courses' );


// Cookie Functions
function check_cookie_cookie() {
   // Check if cookie set
   if (isset($_COOKIE["signpostcookieaccept"])) {
      // It's set so retrieve choice
      $cookie_accept = trim($_COOKIE["signpostcookieaccept"]);
   } else {
      $cookie_accept = '';
   }
   return $cookie_accept;
   
} // End of function 

// Poss retired functions
/*
function get_skills_from_skills_string($str) {
   global $course_skills_array;
   
   $arr = explode(',', $str);
   $aret = array();
   
   foreach($arr as $item) {
      $aret[] = ccReturnArr($course_skills_array, $item);
   }
   $ret = implode(', ', $aret);
   
   return $ret;
}
*/

/*
function get_days_from_day_string($str) {
   global $days_array;
   
   $arr = explode(',', $str);
   $aret = array();
   
   foreach($arr as $item) {
      $aret[] = ccReturnArr($days_array, $item);
   }
   $ret = implode(', ', $aret);
   //echo '<p>'.$ret.'</p>';
   
   return $ret;
}
*/

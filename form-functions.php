<?php
$course_act_type_array = array();
$course_skills_array = array();
$support_array = array();
$distances_array = array();
$days_array = array();
$yes_no_array = array();


function arrays_setup() { 
   global $course_act_type_array;
   $course_act_type_array = array(
      array('di',__( 'Drop in session', 'signpost' )),
      array('bs',__( 'Booked session', 'signpost' )),
      array('bc',__( 'Booked course', 'signpost' ))
   );
   
   global $course_skills_array;
   $course_skills_array = array(
      array('mi',__( 'Managing information online e.g. searching', 'signpost' )),
      array('co',__( 'Communicating online e.g. email', 'signpost' )),
      array('t',__( 'Transacting online e.g. shopping', 'signpost' )),
      array('ps',__( 'Problem solving online e.g. how to videos', 'signpost' )),
      array('cr',__( 'Creating online e.g. application forms, photos', 'signpost' )),
      array('lit',__( 'Literacy', 'signpost' )),
      array('num',__( 'Numeracy', 'signpost' )),
      array('lan',__( 'English language skills', 'signpost' )),
   );
   
   global $support_array;
   $support_array = array(
      array('1',__( 'None', 'signpost' )),
      array('2',__( 'Staff support if not too busy', 'signpost' )),
      array('3',__( 'Support from staff or volunteer', 'signpost' )),
   );

   global $distances_array;
   $distances_array = array(
      array('1',__( 'Less than 1 miles', 'signpost' )),
      array('2',__( 'Less than 2 miles', 'signpost' )),
      array('5',__( 'Less than 5 miles', 'signpost' )),
      array('10',__( 'Less than 10 miles', 'signpost' )),
      array('25',__( 'Less than 25 miles', 'signpost' )),
      array('50',__( 'Less than 50 miles', 'signpost' )),
      array('0',__( 'Show all', 'signpost' )),
   );
   
   global $days_array;
   $days_array = array(
      array('1',__( 'Monday', 'signpost' )),
      array('2',__( 'Tuesday', 'signpost' )),
      array('3',__( 'Wednesday', 'signpost' )),
      array('4',__( 'Thursday', 'signpost' )),
      array('5',__( 'Friday', 'signpost' )),
      array('6',__( 'Saturday', 'signpost' )),
      array('7',__( 'Sunday', 'signpost' )),
   );
   
   global $yes_no_array;
   $yes_no_array = array(
      array('y',__( 'Yes', 'signpost' )),
      array('n',__( 'No', 'signpost' ))
   );

   global $costs_array;
   $costs_array = array(
      array('f',__( 'Free', 'signpost' )),
      array('p',__( 'Paid for', 'signpost' ))
   );

   
//////////////////////////////////////////////////////////////////   
}

add_action('after_setup_theme', 'arrays_setup', 5);

// Arrays for courses and internet access

function get_course_act_type() {
   global $course_act_type_array;
   return $course_act_type_array; 
}


function get_course_skills() {
   global $course_skills_array;
   return $course_skills_array; 
}

// Get the arrays from the taxonomies
$course_themes_array = array();

function get_course_themes() {
   global $course_themes_array;
   return $course_themes_array; 
}

function set_course_themes($arr) {
   global $course_themes_array;
   $course_themes_array = $arr; 
}

$course_skills_levels_array = array();

function get_course_skills_levels() {
   global $course_skills_levels_array;
   return $course_skills_levels_array; 
}

function set_course_skills_levels($arr) {
   global $course_skills_levels_array;
   $course_skills_levels_array = $arr; 
}

function get_support_array() {
   global $support_array;
   return $support_array; 
}

function get_distances() {
   global $distances_array;
   return $distances_array; 
}

function get_days_array() {
   global $days_array;
   
   return $days_array;
}

function get_yes_no_array() {
   global $yes_no_array;
   
   return $yes_no_array;
}
function get_course_costs() {
   global $costs_array;
   
   return $costs_array;
}

/*
/////////////////// Arrays for date of birth //////////////////
$arrDobD = array();
$arrDobM = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
$arrDobY = array();

function getDayArray() {
   global $arrDobD;
   
   //Populate if not already done so
   if (count($arrDobD) == 0) {
      for ($i = 1; $i <= 31; $i++) {
         $arrDobD[] = $i;
      }
   }
   
   return $arrDobD;
}
function getMonthArray() {
   global $arrDobM;
   
   return $arrDobM;
}

function getYearArray() {
   global $arrDobY;
   
   //Populate if not already done so
   if (count($arrDobY) == 0) {
      $thisYr = (int)date('Y');
      for ($i = $thisYr; $i >= ($thisYr - 100); $i--) {
         $arrDobY[] = $i;
      }
   }
   
   return $arrDobY;
}

*/

///////////////////////////  Form related functions ///////////////////////////////
//Cope with magic quotes

if (get_magic_quotes_gpc()) {
	function co_stripslashes_deep($value)	{
		$value = is_array($value) ?
				array_map('co_stripslashes_deep', $value) :
				stripslashes($value);

		return $value;
	}

	$_POST = array_map('co_stripslashes_deep', $_POST);
	$_GET = array_map('co_stripslashes_deep', $_GET);
	$_COOKIE = array_map('co_stripslashes_deep', $_COOKIE);
	$_REQUEST = array_map('co_stripslashes_deep', $_REQUEST);
}

// Sanitise email addresses
function safeEmail($str) {
   return  filter_var($str, FILTER_SANITIZE_EMAIL);
}

function outScrn($inTxt) {
// Takes a varible name and applies necessary protection
   $inTxt = htmlspecialchars($inTxt, ENT_QUOTES, 'UTF-8');

   return $inTxt;
}
function errInd($fieldName) {
   $path = get_bloginfo('template_url').'/images/';
   $html = '<img src="'.$path.'error-ind.gif" height="16" width="16" alt="'.__( 'Error Indicator', 'signpost' ).'" title="'.__( 'Error Indicator', 'signpost' ).'" id="'.$fieldName.'-err-ind" /> ';

   return $html;
}

function getErrorMsg($arr, $field) {
   foreach($arr as $item) {
      if ($item[0] == $field) return $item[1];
   }
   
   return '';

}



/////////////////// Validation functions /////////
function isValidURL($url) {

   return preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $url);
}

function checkUrlStart($url) {
   // check url starts with http:// - and if not, add on front
   $test1 = 'http://';
   $test2 = 'https://';
   
   $pos1 = strrpos ( $url , $test1, 0 );
   $pos2 = strrpos ( $url , $test2, 0 );
   
   //echo ($pos1 != 0);
   
   if (($pos1 === 0) or ($pos2 === 0 ) ) {
      return $url;
   } else {
      return $test1.$url;
   }
}

// Errors array
$arrErrs = array();
function getFormErrors() {
   global $arrErrs;
   
   return $arrErrs;
}
function setFormErrors($arr) {
   global $arrErrs;
   
   $arrErrs = $arr;
}

// Clean form values
$clean = array();
function getClean() {
   global $clean;
   
   return $clean;
}
function setClean($arr) {
   global $clean;
   
   $clean = $arr;
}

///////////////////////// Shortcodes to place forms on pages //////////////////////////////
/**********************************************************************************************
Contact Forms

This function places the contact form on the page
**********************************************************************************************/
function spt_contact_form_sc($atts, $content = null) {
   // Get parameters
   extract(shortcode_atts(array(
   ), $atts));

   // Pull in stored values
   $arrErrs = getFormErrors();
   $clean = getClean();
   
   //var_dump($clean);
   
   global $contactForm;
   
   // Call the function to print out the form and return
   $strHtml = printFormNew($contactForm, $clean, $arrErrs );
   return $strHtml;


}
add_shortcode("spt-contact-form", "spt_contact_form_sc");   

/**********************************************************************************************
Add Internet Access Form

This function places the add internet access form on the page
**********************************************************************************************/
function spt_add_internet_form_sc($atts, $content = null) {
   // Get parameters
   extract(shortcode_atts(array(
   ), $atts));

   // Pull in stored values
   $arrErrs = getFormErrors();
   $clean = getClean();
   
   //var_dump($clean);
   
   global $internetAccessForm;
   
   // Call the function to print out the form and return
   $strHtml = printFormNew($internetAccessForm, $clean, $arrErrs );
   return $strHtml;


}
add_shortcode("spt-add-internet-form", "spt_add_internet_form_sc");   

/**********************************************************************************************
Add Course Form

This function places the add course form on the page
**********************************************************************************************/
function spt_add_course_form_sc($atts, $content = null) {
   // Get parameters
   extract(shortcode_atts(array(
   ), $atts));

   // Pull in stored values
   $arrErrs = getFormErrors();
   $clean = getClean();
   
   global $courseForm;
   
   // Call the function to print out the form and return
   $strHtml = printFormNew($courseForm, $clean, $arrErrs );
   return $strHtml;


}
add_shortcode("spt-course-form", "spt_add_course_form_sc");   


function getFormFieldLength($formDef, $fieldName) {
   foreach((array)$formDef['fields'] as $field) {
      if ($field['name'] == $fieldName) {
         return $field['maxlen'];
         break;
      }
   }
   return false;
}
function checkSelectInOptions($formDef, $fieldName, $val) {
// Checks that a submitted value from a select control 
// matches one of the available options
   foreach((array)$formDef['fields'] as $field) {
      if (($field['type'] == 'select') and ($field['name'] == $fieldName)) {
         foreach((array)$field['options'] as $option) {
            if ($option[0] == $val) {
               return true;
               break;
            }
         }
      }
   }
   return false;
}
function checkCheckboxInOptions($formDef, $fieldName, $val) {
// Checks that a submitted value from a checkbox group 
// matches one of the available options
   foreach((array)$formDef['fields'] as $field) {
      if (($field['type'] == 'chkboxgroup') and ($field['name'] == $fieldName)) {
         foreach((array)$field['options'] as $option) {
            if ($option[0] == $val) {
               return true;
               break;
            }
         }
      }
   }
   return false;
}

function printFormErrors($formDef,$arrErrs ) {
   $strHtml = '';
   
   if (count($arrErrs) > 0){
      $strHtml .= '<div id="'.$formDef['error-sect-id'].'" class="'.$formDef['error-sect-class'].'">';
      $strHtml .= '<h'.$formDef['error-sect-hdr-level'].'>'.$formDef['error-sect-hdr'].'</h'.$formDef['error-sect-hdr-level'].'>';
      $strHtml .= $formDef['error-sect-intro'];
      $strHtml .= '<ul>';
      foreach ($arrErrs as $err) {
         if (!empty($err[0])) {
            $strHtml .= '<li><a href="#'.$err[0].'">'.$err[1].'</a></li>';
         } else {
            $strHtml .= '<li>'.$err[1].'</li>';
         }
      }
      $strHtml .= '</ul></div>';
   }
   return $strHtml;
}


////////////// New Print form /////////////////////////////
function printFormNew($formDef,$clean, $arrErrs ) {
   // Initialise strings
   $strHidden = '';
   
   // Start container and form
   $strHtml = '<div class="'.$formDef['cont-class'].'" role="form">';
   $strHtml .= '<form action="'.get_the_permalink().'" method="post" name="'.$formDef['form-id'].'" id="'.$formDef['form-id'].'">';
   $strHtml .= '<p>'.__( 'Required information is marked with a', 'signpost' ).' <span class="mand">'.$formDef['mand-ind'].'</span></p>';

   // Start list
   $firstTime = true;
   $strHtml .= '<ul>';
   
   // Print out each of the fields
   foreach($formDef['fields'] as $field) {
      $errMsg = getErrorMsg($arrErrs, $field['name']);
      $errInd = '';
      $ariaInvalidFrag = '';
      
      // Check for any errors on this field
      if (!empty($errMsg)) {
         //$errInd = errInd($field['name']);
         $ariaInvalidFrag = ' aria-invalid="true"';
      } else {
         if (!empty($clean['submitted']) and $clean['submitted']) {
            $ariaInvalidFrag = ' aria-invalid="false"';
         }
      }  
      
      // Retrieve class name for <li>
      $liCss = (empty($field['li-class']))?'':' class="'.$field['li-class'].'"';
      // Is field required?
      $reqd = false;
      $reqStr = '';
      $reqAttr = '';
      // These strings are used in client side validation and will contain 
      // the validation that can be done on the client.
      $validStr = '';
      $validFrag = '';
      
      //echo '<p>Field '.$field['name'].'</p>';
      //if (!empty($field['validation'])) var_dump($field['validation']);

      // Check in validation array - if it exists
      if (!empty($field['validation'])) {
         foreach((array) $field['validation'] as $validate) {
            switch ($validate[0]) {
               case 'reqd' :
                  $reqd = true;
                  $reqStr = '&nbsp;<span class="mand">'.$formDef['mand-ind'].'</span>';
                  $reqAttr = ' aria-required="true"';
                  
                  $validStr .= ' data-v-reqd="'.$validate[1].'"';
                  break;
               case 'len' :
                  $validStr .= ' data-v-len="'.$field['maxlen'].'~'.sprintf($validate[1],$field['maxlen']).'"';
                  break;
               case 'email' :
                  $validStr .= ' data-v-email="'.$validate[1].'"';
                  break;
               case 'sqldate' :
                  $validStr .= ' data-v-sqldate="'.$validate[1].'"';
                  break;
               case 'sq' :
                  $validStr .= ' data-v-sq="'.getSecA($clean['sq']).'~'.$validate[1].'"';
                  break;
           }
         }
      } // end of if (!empty($field['validation']))

      if ($field['type'] != 'chkboxgroup') {
         // Construct start of label
         if (!empty($field['label'])) {
            $labelTxt = '<label for="'.$field['name'].'"><span class="text">'.$errInd.$field['label'].$reqStr.'</span>';
         } else {
            $labelTxt = '';
         }
      } else {
         // Checkbox group labels will be handled further down
      }
      
      switch ($field['type']) {
         case 'text':
            $strHtml .= '<li'.$liCss.'>';
            $strHtml .= $labelTxt;
            $strHtml .= '<input maxlength="'.$field['maxlen'].'" type="text" name="'.$field['name'].'" id="'.$field['name'].'" value="'.outScrn($clean[$field['name']]).'"';
            $strHtml .= $reqAttr.$ariaInvalidFrag.$validStr.'><span class="errors">'.$errMsg.'</span>';
            $strHtml .= '</label></li>';
            break;
         
         case 'textarea':
            $strHtml .= '<li'.$liCss.'>';
            $strHtml .= $labelTxt;
            $strHtml .= '<textarea cols="" rows="3" name="'.$field['name'].'" id="'.$field['name'].'"'.$reqAttr.$ariaInvalidFrag.$validStr.'>';
            $strHtml .= outScrn($clean[$field['name']]);
            $strHtml .= '</textarea>';
            $strHtml .= '<span class="errors">'.$errMsg.'</span>';
            $strHtml .= '</label></li>';
            break;
         
         case 'select':
            $strHtml .= '<li'.$liCss.'>';
            $strHtml .= $labelTxt;
            $strHtml .= '<select name="'.$field['name'].'" id="'.$field['name'].'"'.$reqAttr.$ariaInvalidFrag.$validStr.'>';
            $strHtml .= '<option value="">'.__( 'Please choose...', 'signpost' ).'</option>';
            
            foreach ((array)$field['options'] as $option) {
               $selected = ($clean[$field['name']] == $option[0])?' selected="selected"':'';
               $strHtml .= '<option value="'.$option[0].'"'.$selected.'>'.$option[1].'</option>';
            
            }
            $strHtml .= '</select><span class="errors">'.$errMsg.'</span>';
            $strHtml .= '</label></li>';
            break;
         
          case 'chkboxgroup':
            $strHtml .= '<li'.$liCss.'>';
            $strHtml .= '<fieldset data-type="chkbox"'.$validStr.'>';
            $strHtml .= '<legend id="'.$field['name'].'-legend">'.$field['label'].' '.$reqStr.'</legend>';
            $strHtml .= '<ul class="checkbox">';
            
            foreach ((array)$field['options'] as $option) {
               // See if any of them have been checked
               $checked = '';
               foreach ((array)$clean[$field['name']] as $sel) {
                  if ($sel == $option[0]) {
                     $checked = ' checked="checked"';
                     break;
                  }
               }

               $strHtml .= '<li class="check">';
               $strHtml .= '<input type="checkbox" name="'.$field['name'].'[]" id="'.$field['name'].'-'.$option[0].'" value="'.$option[0].'" aria-labelledby="'.$field['name'].'-legend '.$field['name'].'-'.$option[0].'-label '.$field['name'].'-errors"'.$checked.'>';
               $strHtml .= '<label for="'.$field['name'].'-'.$option[0].'" id="'.$field['name'].'-'.$option[0].'-label">';                
               $strHtml .= $option[1].'</label>';                
               $strHtml .= '</li>';
            }
            $strHtml .= '</ul>';
            $strHtml .= '<div class="fieldseterrors" id="'.$field['name'].'-errors">'.$errMsg.'</div>';
            $strHtml .= '</fieldset></li>';
            break;
         
         case 'sq':
            $strHtml .= '<li'.$liCss.'>';
            $strHtml .= sprintf($labelTxt, getSecQ($clean['sq'])) ;
            $strHtml .= '<input maxlength="'.$field['maxlen'].'" type="text" name="'.$field['name'].'" id="'.$field['name'].'" value=""';
            $strHtml .= $reqAttr.$ariaInvalidFrag.$validStr.' /><span class="errors">'.$errMsg.'</span>';
            $strHtml .= '</label></li>';
            break;
         
         case 'start-fieldset':
            $strHtml .= '<fieldset id="'.$field['name'].'">';
            $strHtml .= '<legend>'.$field['legend'].'</legend>';
            break;

         case 'end-fieldset':
            $strHtml .= '</fieldset>';
            break;

         case 'submit':
            $strHtml .= '<li'.$liCss.'>';
            $strHtml .= '<input type="submit" name="'.$field['name'].'" id="'.$field['name'].'" value="'.$field['value'].'" />';
            $strHtml .= '</li>';
            break;

         case 'hidden':
            $strHidden .= '<input type="hidden" name="'.$field['name'].'" id="'.$field['name'].'" value="'.$field['value'].'" />';
            break;

      }
      
   
   }

   $strHtml .= '</ul>';
  
   // Check if a security question is being used and if so,
   // put out the hidden fields with the index value inside
   if ($formDef['sq-reqd']) {
      $strHidden .= '<input type="hidden" name="'.$formDef['sq-id'].'" id="'.$formDef['sq-id'].'" value="'.$clean['sq'].'" />';

   }
   if (!empty($formDef['nonce-name'])) {
      $strHidden .= wp_nonce_field($formDef['nonce-name'],$formDef['nonce-name'],true, false);
   }
   
   $strHtml .= $strHidden; // Add hidden fields
   $strHtml .= '</form></div>';
   return $strHtml;

}

////////////////////////////////////////////////////


function validateForm($formDef,$clean, $arrErrs ) {
// This is definitely not finished - work in progress only

   $raw = array();
   
   foreach((array)$formDef['fields'] as $field) {
      
      foreach((array)$field['validation'] as $validate) {
      
         // Clean up the values passed in and store
         switch ($field['type']) {
            case 'text':
            case 'textarea':
               $clean[$field['name']] = substr(sanitize_text_field($_POST[$field['name']]), 0, $field['maxlen']);
               break;
            case 'select':
               // Select value must be one of the supplied values
               if (ccCheckArr($field['options'], $_POST[$field['name']])) {
                  $clean[$field['name']] = $_POST[$field['name']];
               } else {
                  $clean[$field['name']] = '';
               }
               
               break;
         }
         switch ($validate[0]) {
            case 'reqd':
               break;
         }



      } // End foreach $formDef['fields']
   } // End foreach $formDef['fields']
   var_dump($clean);
   
   // Update the global version of the $clean array
   setClean($clean);
   return false;
}

function setAria($errMsg, $clean) {
   $str = '';
   // Check for any errors on this field
   if (!empty($errMsg)) {
      $str = ' aria-invalid="true"';
   } else {
      if ($clean['submitted']) {
         $str = ' aria-invalid="false"';
      }
   }  

   return $str;
}

function validateDate($date, $format = 'Y-m-d H:i:s') {
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
}


function check_uk_postcode($string){
    // Start config
    $valid_return_value = true;
    $invalid_return_value = false;
    $exceptions = array('BS981TL', 'BX11LT', 'BX21LB', 'BX32BB', 'BX55AT', 'CF101BH', 'CF991NA', 'DE993GG', 'DH981BT', 'DH991NS', 'E161XL', 'E202AQ', 'E202BB', 'E202ST', 'E203BS', 'E203EL', 'E203ET', 'E203HB', 'E203HY', 'E981SN', 'E981ST', 'E981TT', 'EC2N2DB', 'EC4Y0HQ', 'EH991SP', 'G581SB', 'GIR0AA', 'IV212LR', 'L304GB', 'LS981FD', 'N19GU', 'N811ER', 'NG801EH', 'NG801LH', 'NG801RH', 'NG801TH', 'SE18UJ', 'SN381NW', 'SW1A0AA', 'SW1A0PW', 'SW1A1AA', 'SW1A2AA', 'SW1P3EU', 'SW1W0DT', 'TW89GS', 'W1A1AA', 'W1D4FA', 'W1N4DJ');
    // Add Overseas territories ?
    array_push($exceptions, 'AI-2640', 'ASCN1ZZ', 'STHL1ZZ', 'TDCU1ZZ', 'BBND1ZZ', 'BIQQ1ZZ', 'FIQQ1ZZ', 'GX111AA', 'PCRN1ZZ', 'SIQQ1ZZ', 'TKCA1ZZ');
    // End config


    $string = strtoupper(preg_replace('/\s/', '', $string)); // Remove the spaces and convert to uppercase.
    $exceptions = array_flip($exceptions);
    if(isset($exceptions[$string])){return $valid_return_value;} // Check for valid exception
    $length = strlen($string);
    if($length < 5 || $length > 7){return $invalid_return_value;} // Check for invalid length
    $letters = array_flip(range('A', 'Z')); // An array of letters as keys
    $numbers = array_flip(range(0, 9)); // An array of numbers as keys

    switch($length){
        case 7:
            if(!isset($letters[$string[0]], $letters[$string[1]], $numbers[$string[2]], $numbers[$string[4]], $letters[$string[5]], $letters[$string[6]])){break;}
            if(isset($letters[$string[3]]) || isset($numbers[$string[3]])){
                return $valid_return_value;
            }
        break;
        case 6:
            if(!isset($letters[$string[0]], $numbers[$string[3]], $letters[$string[4]], $letters[$string[5]])){break;}
            if(isset($letters[$string[1]], $numbers[$string[2]]) || isset($numbers[$string[1]], $letters[$string[2]]) || isset($numbers[$string[1]], $numbers[$string[2]])){
                return $valid_return_value;
            }
        break;
        case 5:
            if(isset($letters[$string[0]], $numbers[$string[1]], $numbers[$string[2]], $letters[$string[3]], $letters[$string[4]])){
                return $valid_return_value;
            }
        break;
    }

    return $invalid_return_value;
}


?>
<?php

///////////////////////////  Form Definitions ///////////////////////////////
$contactForm = array();
$internetAccessForm = array();
$courseForm = array();

add_action( 'init', 'signpost_create_form_defs', 3 );

// create taxonomy for courses
function signpost_create_form_defs() {
   global $contactForm;
   global $internetAccessForm;
   global $courseForm;

   global $course_act_type_array;
   global $course_skills_array;
   global $support_array;
   global $distances_array;
   global $days_array;
   global $yes_no_array;
   
   
   // Contact form 
   $contactForm = array(
      'cont-id' => '',
      'cont-class' => 'contactform location-contact',
      'form-id' => 'contactform',
      'submit-to' => '',
      'client-val' => true,
      'mand-ind' => '*',
      'success-img' => '',
      'error-img' => '',
      'error-sect-id' => 'sub-errs',
      'error-sect-class' => '',
      'error-sect-hdr' => __( 'Submission Problems', 'signpost' ),
      'error-sect-hdr-level' => 2,
      'error-sect-intro' => '<p>'.__( 'We were not able to process your enquiry. Please review the following items and check what you entered', 'signpost' ).'</p>',
      'nonce-name' => 'contactnonce',
      'sq-reqd' => true,
      'sq-id' => 'sq',
      'sq-label' => __( 'Please answer this question:', 'signpost' ).' %1$s <span class="clarify">('.__( 'Helps stop spam', 'signpost' ).')</span>',
      'fields' => array(
         array(
            'label' => __( 'Name', 'signpost' ),
            'type' => 'text',
            'name' => 'yourname',
            'li-class' => 'short clear',
            'maxlen' => 200,
            'validation' => array(
                  array('reqd', __( 'Please supply your name', 'signpost' )),
                  array('len', __( 'Your name can only be %1$d characters long', 'signpost' )),
            )
         ),
         array(
            'label' => __( 'Email address', 'signpost' ),
            'type' => 'text',
            'name' => 'email',
            'li-class' => 'short',
            'maxlen' => 100,
            'validation' => array(
                  array('reqd', __( 'Please supply your email address', 'signpost' )),
                  array('email', __( 'Please check the email address format', 'signpost' )),
                  array('len', __( 'Your email address can only be %1$d characters long', 'signpost' )),
            )
         ),
         array(
            'label' => __( 'Subject', 'signpost' ),
            'type' => 'text',
            'name' => 'subject',
            'li-class' => 'clear',
            'maxlen' => 250,
            'validation' => array(
                  array('reqd', __( 'Please supply a subject', 'signpost' )),
                  array('len', __( 'The subject can only be %1$d characters long', 'signpost' )),
            )
         ),
         array(
            'label' => __( 'Your message', 'signpost' ),
            'type' => 'textarea',
            'name' => 'message',
            'li-class' => 'clear',
            'maxlen' => 2000,
            'validation' => array(
                  array('reqd', __( 'Please include your message', 'signpost' )),
                  array('len', __( 'Your message can only be %1$d characters long', 'signpost' )),
            )
         ),
         array(
            'label' => __('Please answer this security question:', 'signpost' ).' %1$s <span class="clarify">('.__('Helps stop spam', 'signpost' ).')</span>',
            'type' => 'sq',
            'name' => 'security',
            'maxlen' => 2,
            'validation' => array(
                  array('reqd', __('Please answer the security question', 'signpost' )),
                  array('len', __('Please answer the security question', 'signpost' )),
                  array('sq', __('Please check your answer for the security question', 'signpost' )),
            )
         ),
         array(
            'name' => 'submit',
            'type' => 'submit',
            'li-class' => 'submit',
            'value' => __('Submit', 'signpost' )
         ),
      ),
      
   );
   
   
   // Add Internet Access form 
   $internetAccessForm = array(
      'cont-id' => '',
      'cont-class' => 'contact add-internet',
      'form-id' => 'add-internet',
      'submit-to' => '',
      'client-val' => true,
      'mand-ind' => '*',
      'success-img' => '',
      'error-img' => '',
      'error-sect-id' => 'sub-errs',
      'error-sect-class' => '',
      'error-sect-hdr' => __( 'Submission Problems', 'signpost' ),
      'error-sect-hdr-level' => 2,
      'error-sect-intro' => '<p>'.__( 'We were not able to process the addition. Please review the following items and check what you entered', 'signpost' ).'</p>',
      'nonce-name' => 'internetaddnonce',
      'sq-reqd' => true,
      'sq-id' => 'sq',
      'sq-label' => __( 'Please answer this question:', 'signpost' ).' %1$s <span class="clarify">('.__( 'Helps stop spam', 'signpost' ).')</span>',
      'fields' => array(
         array(
            'label' => __( 'Provider (e.g. library name, cafe name)', 'signpost' ),
            'type' => 'text',
            'name' => 'provider',
            'li-class' => 'short clear',
            'maxlen' => 200,
            'validation' => array(
                  array('reqd', __( 'Please supply the internet access provider', 'signpost' )),
                  array('len', __( 'Your provider name can only be %1$d characters long', 'signpost' )),
            )
         ),
         array(
            'label' => __( 'Location notes (e.g. refreshments, baby changing, toilets, etc)', 'signpost' ),
            'type' => 'textarea',
            'name' => 'location',
            'li-class' => 'short',
            'maxlen' => 500,
            'validation' => array(
                  array('len', __( 'Location can only be %1$d characters long', 'signpost' )),
            )
         ),
         array(
            'label' => __( 'Address (without postcode)', 'signpost' ),
            'type' => 'textarea',
            'name' => 'address',
            'li-class' => 'short clear',
            'maxlen' => 300,
            'validation' => array(
                  array('reqd', __( 'Please supply the address', 'signpost' )),
                  array('len', __( 'The address can only be %1$d characters long', 'signpost' )),
            )
         ),
         array(
            'label' => __( 'Postcode', 'signpost' ),
            'type' => 'text',
            'name' => 'postcode',
            'li-class' => 'short',
            'maxlen' => 12,
            'validation' => array(
                  array('reqd', __( 'Please include the postcode', 'signpost' )),
                  array('len', __( 'Postcode can only be up to %1$d characters long', 'signpost' )),
            )
         ),
         array(
            'label' => __( 'Contact information', 'signpost' ),
            'type' => 'textarea',
            'name' => 'contact',
            'li-class' => 'short clear',
            'maxlen' => 300,
            'validation' => array(
                  array('len', __( 'Contact information can only be up to %1$d characters long', 'signpost' )),
            )
         ),
         array(
            'label' => __( 'Free wifi', 'signpost' ),
            'type' => 'select',
            'name' => 'freewifi',
            'li-class' => 'short',
            'options' => $yes_no_array,
            'validation' => array(
                  array('reqd', __( 'Please specify whether there is free wifi', 'signpost' )),
            )
         ),
         array(
            'label' => __( 'Restrictions (e.g. age range, women only)', 'signpost' ),
            'type' => 'textarea',
            'name' => 'restrictions',
            'li-class' => 'short clear',
            'maxlen' => 300,
            'validation' => array(
                  array('len', __( 'The restrictions can only be %1$d characters long', 'signpost' )),
            )
         ),
         array(
            'label' => __( 'PC time limits', 'signpost' ),
            'type' => 'textarea',
            'name' => 'timelimits',
            'li-class' => 'short',
            'maxlen' => 300,
            'validation' => array(
                  array('len', __( 'The PC time limits can only be %1$d characters long', 'signpost' )),
            )
         ),
         array(
            'label' => __( 'Number of internet PCs available', 'signpost' ),
            'type' => 'text',
            'name' => 'numpcs',
            'li-class' => 'short clear',
            'maxlen' => 300,
            'validation' => array(
                  array('len', __( 'Number of internet PCs can only be up to %1$d characters long', 'signpost' )),
            )
         ),
         array(
            'label' => __( 'Number of tablets available', 'signpost' ),
            'type' => 'text',
            'name' => 'numtablets',
            'li-class' => 'short',
            'maxlen' => 300,
            'validation' => array(
                  array('len', __( 'Number of tablets can only be up to %1$d characters long', 'signpost' )),
            )
         ),
         array(
            'label' => __( 'Support available', 'signpost' ),
            'type' => 'select',
            'name' => 'support',
            'li-class' => 'short clear',
            'options' => $support_array,
            'validation' => array(
                  array('reqd', __( 'Please specify support level available', 'signpost' )),
            )
         ),
         array(
            'label' => __( 'Usual opening days', 'signpost' ),
            'type' => 'chkboxgroup',
            'name' => 'openingdays',
            'li-class' => 'short clear',
            'options' => $days_array,
            'validation' => array(
               array('reqd', __( 'Please select at least one opening day', 'signpost' )),
            )
         ),
         array(
            'label' => __( 'Opening hours', 'signpost' ),
            'type' => 'textarea',
            'name' => 'openhours',
            'li-class' => 'short',
            'maxlen' => 500,
            'validation' => array(
               array('reqd', __( 'Please detail the opening hours', 'signpost' )),
               array('len', __( 'The opening hours can only be %1$d characters long', 'signpost' )),
            )
         ),
         array(
            'label' => __( 'Printing facilities', 'signpost' ),
            'type' => 'textarea',
            'name' => 'printing',
            'li-class' => 'clear',
            'maxlen' => 500,
            'validation' => array(
                  array('len', __( 'The printing facilities can only be %1$d characters long', 'signpost' )),
            )
         ),
         array(
            'label' => __( 'Web address (e.g. http://www.example.com)', 'signpost' ),
            'type' => 'text',
            'name' => 'url',
            'li-class' => 'clear',
            'maxlen' => 400,
            'validation' => array(
                  array('len', __( 'The web address can only be up to %1$d characters long', 'signpost' )),
            )
         ),
   
   
   
   
   
         array(
            'label' => __('Please answer this security question:', 'signpost' ).' %1$s <span class="clarify">('.__('Helps stop spam', 'signpost' ).')</span>',
            'type' => 'sq',
            'name' => 'security',
            'maxlen' => 2,
            'validation' => array(
                  array('reqd', __('Please answer the security question', 'signpost' )),
                  array('len', __('Please answer the security question', 'signpost' )),
                  array('sq', __('Please check your answer for the security question', 'signpost' )),
            )
         ),
         array(
            'name' => 'submit',
            'type' => 'submit',
            'li-class' => 'submit',
            'value' => __('Submit', 'signpost' )
         ),
      ),
      
   );
   
   
   
   // Add Course form 
   $courseForm = array(
      'cont-id' => '',
      'cont-class' => 'contact course',
      'form-id' => 'course',
      'submit-to' => '',
      'client-val' => true,
      'mand-ind' => '*',
      'success-img' => '',
      'error-img' => '',
      'error-sect-id' => 'sub-errs',
      'error-sect-class' => '',
      'error-sect-hdr' => __( 'Submission Problems', 'signpost' ),
      'error-sect-hdr-level' => 2,
      'error-sect-intro' => '<p>'.__( 'We were not able to process the addition. Please review the following items and check what you entered', 'signpost' ).'</p>',
      'nonce-name' => 'courseaddnonce',
      'sq-reqd' => true,
      'sq-id' => 'sq',
      'sq-label' => __( 'Please answer this question:', 'signpost' ).' %1$s <span class="clarify">('.__( 'Helps stop spam', 'signpost' ).')</span>',
      'fields' => array(
   
         array(
            'label' => __( 'Activity title', 'signpost' ),
            'type' => 'text',
            'name' => 'activitysummary',
            'li-class' => 'clear',
            'maxlen' => 300,
            'validation' => array(
                  array('reqd', __( 'Please supply the activity title', 'signpost' )),
                  array('len', __( 'The activity title can only be %1$d characters long', 'signpost' )),
            )
         ),
         array(
            'label' => __( 'Activity details (A fuller description of the activity)', 'signpost' ),
            'type' => 'textarea',
            'name' => 'activitydetail',
            'li-class' => 'clear',
            'maxlen' => 2000,
            'validation' => array(
                  array('len', __( 'The activity detail can only be %1$d characters long', 'signpost' )),
            )
         ),
         array(
            'label' => __( 'Core skills', 'signpost' ),
            'type' => 'chkboxgroup',
            'name' => 'skills',
            'li-class' => 'short clear',
            'options' => $course_skills_array, 
            'validation' => array(
               array('reqd', 'Please indicate the skills'),
            )
         ),
         array(
            'label' => __( 'Skills level', 'signpost' ),
            'type' => 'select',
            'name' => 'skillslevel',
            'li-class' => 'short',
            'options' => get_course_skills_levels(),
            'validation' => array(
               array('reqd', 'Please indicate the skills level'),
            )
         ),
         array(
            'label' => __( 'Activity type', 'signpost' ),
            'type' => 'select',
            'name' => 'activitytype',
            'li-class' => 'short clear',
            'options' => $course_act_type_array,
            'validation' => array(
               array('reqd', 'Please indicate the activity type'),
            )
         ),
         array(
            'label' => __( 'Theme/Title', 'signpost' ),
            'type' => 'select',
            'name' => 'theme',
            'li-class' => 'short',
            'options' => get_course_themes(), 
            'validation' => array(
               array('reqd', 'Please indicate the theme or title'),
            )
         ),
         array(
            'label' => __( 'Provider', 'signpost' ),
            'type' => 'text',
            'name' => 'provider',
            'li-class' => 'short clear',
            'maxlen' => 200,
            'validation' => array(
                  array('reqd', __( 'Please supply the course provider', 'signpost' )),
                  array('len', __( 'Your provider name can only be %1$d characters long', 'signpost' )),
            )
         ),
         array(
            'label' => __( 'Web address (e.g. http://www.example.com)', 'signpost' ),
            'type' => 'text',
            'name' => 'url',
            'li-class' => 'short',
            'maxlen' => 400,
            'validation' => array(
                  array('len', __( 'The web address can only be up to %1$d characters long', 'signpost' )),
            )
         ),
         array(
            'label' => __( 'Contact information', 'signpost' ),
            'type' => 'textarea',
            'name' => 'booking',
            'li-class' => 'short clear',
            'maxlen' => 2000,
            'validation' => array(
                  array('len', __( 'The contact information can only be %1$d characters long', 'signpost' )),
            )
         ),
         array(
            'label' => __( 'Cost (add 0 for free courses, or detail costs)', 'signpost' ),
            'type' => 'textarea',
            'name' => 'cost',
            'li-class' => 'short',
            'maxlen' => 2000,
            'validation' => array(
                  array('len', __( 'The cost detail can only be %1$d characters long', 'signpost' )),
            )
         ),

         array(
            'label' => __( 'Location notes (e.g. refreshments, baby changing, toilets, etc)', 'signpost' ),
            'type' => 'textarea',
            'name' => 'location',
            'li-class' => 'short clear',
            'maxlen' => 500,
            'validation' => array(
                  array('len', __( 'Location can only be %1$d characters long', 'signpost' )),
            )
         ),
         array(
            'label' => __( 'Free wifi', 'signpost' ),
            'type' => 'select',
            'name' => 'freewifi',
            'li-class' => 'short',
            'options' => $yes_no_array,
            'validation' => array(
                  array('reqd', __( 'Please specify whether there is free wifi', 'signpost' )),
            )
         ),
         array(
            'label' => __( 'Location address (without postcode)', 'signpost' ),
            'type' => 'textarea',
            'name' => 'address',
            'li-class' => 'short clear',
            'maxlen' => 300,
            'validation' => array(
                  array('reqd', __( 'Please supply the address', 'signpost' )),
                  array('len', __( 'The address can only be %1$d characters long', 'signpost' )),
            )
         ),
         array(
            'label' => __( 'Location postcode', 'signpost' ),
            'type' => 'text',
            'name' => 'postcode',
            'li-class' => 'short',
            'maxlen' => 12,
            'validation' => array(
                  array('reqd', __( 'Please include the postcode', 'signpost' )),
                  array('len', __( 'Postcode can only be up to %1$d characters long', 'signpost' )),
            )
         ),
         array(
            'label' => __( 'Activity days', 'signpost' ),
            'type' => 'chkboxgroup',
            'name' => 'dropindays',
            'li-class' => 'short clear',
            'options' => $days_array,
            'validation' => array(
            )
         ),
         array(
            'label' => __( 'Exceptions to regular days', 'signpost' ),
            'type' => 'text',
            'name' => 'exceptions',
            'li-class' => 'short',
            'maxlen' => 400,
            'validation' => array(
                  array('len', __( 'Exceptions to regular days can only be up to %1$d characters long', 'signpost' )),
            )
         ),
   
   
         array(
            'label' => __( 'Start date (format YYYY-MM-DD)', 'signpost' ),
            'type' => 'text',
            'name' => 'startdate',
            'li-class' => 'short clear',
            'maxlen' => 10,
            'validation' => array(
                  array('sqldate', __( 'Please enter start date in correct format', 'signpost' )),
                  array('len', __( 'Start date can only be up to %1$d characters long', 'signpost' )),
            )
         ),
         array(
            'label' => __( 'End date (format YYYY-MM-DD)', 'signpost' ),
            'type' => 'text',
            'name' => 'enddate',
            'li-class' => 'short',
            'maxlen' => 10,
            'validation' => array(
                  array('sqldate', __( 'Please enter start date in correct format', 'signpost' )),
                  array('len', __( 'End date can only be up to %1$d characters long', 'signpost' )),
            )
         ),
         array(
            'label' => __( 'Start time', 'signpost' ),
            'type' => 'text',
            'name' => 'starttime',
            'li-class' => 'short clear',
            'maxlen' => 500,
            'validation' => array(
                  array('len', __( 'Start time can only be up to %1$d characters long', 'signpost' )),
            )
         ),
         array(
            'label' => __( 'End time', 'signpost' ),
            'type' => 'text',
            'name' => 'endtime',
            'li-class' => 'short',
            'maxlen' => 500,
            'validation' => array(
                  array('len', __( 'End time can only be up to %1$d characters long', 'signpost' )),
            )
         ),
   
   
         array(
            'label' => __( 'Duration', 'signpost' ),
            'type' => 'text',
            'name' => 'duration',
            'li-class' => 'clear',
            'maxlen' => 200,
            'validation' => array(
                  array('len', __( 'Duration can only be up to %1$d characters long', 'signpost' )),
            )
         ),
   
   
         array(
            'label' => __('Please answer this security question:', 'signpost' ).' %1$s <span class="clarify">('.__('Helps stop spam', 'signpost' ).')</span>',
            'type' => 'sq',
            'name' => 'security',
            'maxlen' => 2,
            'validation' => array(
                  array('reqd', __('Please answer the security question', 'signpost' )),
                  array('len', __('Please answer the security question', 'signpost' )),
                  array('sq', __('Please check your answer for the security question', 'signpost' )),
            )
         ),
         array(
            'name' => 'submit',
            'type' => 'submit',
            'li-class' => 'submit',
            'value' => __('Submit', 'signpost' )
         ),
      ),
      
   );
   }
?>
